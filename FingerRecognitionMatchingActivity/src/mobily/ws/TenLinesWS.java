package mobily.ws;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.util.Log;

public class TenLinesWS {

	String SOAP_ACTION = "http://www.benchmarkatlarge.com/" + "TenLines";
	String NAMESPACE = "http://www.benchmarkatlarge.com";
	String METHOD_NAME = "TenLines";
	String URL = "http://www.benchmarkatlarge.com/Mobily/WebServices/mobilysync.asmx?WSDL";

	public boolean isTenLines(long personId) {
		boolean isValidatedTenLines=false;
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);  
		request.addProperty("personid", personId);
		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;  
        envelope.setOutputSoapObject(request);  
        HttpTransportSE ht = new HttpTransportSE(URL);
        
        try {  
             ht.call(SOAP_ACTION, envelope);
             if(Integer.parseInt(envelope.getResponse().toString()) == 1)
            	 isValidatedTenLines = true;
        } catch (Exception ex) {  
             Log.d("TenLines", "Exception -> "+ex.getMessage());
             ex.printStackTrace();
        }  
		
        return isValidatedTenLines;
	}

}
