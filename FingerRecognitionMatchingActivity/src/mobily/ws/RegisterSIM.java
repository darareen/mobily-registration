package mobily.ws;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.util.Log;


public class RegisterSIM {
	String SOAP_ACTION = "http://www.benchmarkatlarge.com/" + "SIMRegistration";
	String NAMESPACE = "http://www.benchmarkatlarge.com";
	String METHOD_NAME = "SIMRegistration";
	String URL = "http://www.benchmarkatlarge.com/Mobily/WebServices/mobilysync.asmx?WSDL";
	
	public int registerSIM(String personId, String simNum, String pukNum, byte[] idImage, byte[] sigImage) {
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);  
		request.addProperty("personid", Long.parseLong(personId));
		request.addProperty("SIMID", simNum);
		request.addProperty("PUK", pukNum);
		request.addProperty("IDImage", new org.ksoap2.serialization.SoapPrimitive(SoapEnvelope.ENC, "base64", org.kobjects.base64.Base64.encode(idImage)));
		request.addProperty("SignatureImage", new org.ksoap2.serialization.SoapPrimitive(SoapEnvelope.ENC, "base64", org.kobjects.base64.Base64.encode(sigImage)));
		
		Log.d("TAG", "Request created!!");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(  
                  SoapEnvelope.VER11);
        new MarshalBase64().register(envelope);
        Log.d("TAG", "Envelope!!");
        envelope.dotNet = true;  
        envelope.setOutputSoapObject(request);  
        Log.d("TAG", "Set object!!");
        HttpTransportSE ht = new HttpTransportSE(URL);
        Log.d("TAG", "HT!!");
        String result;
        int resultCode = -1;
        try {  
//        	ht.debug = true;
			ht.call(SOAP_ACTION, envelope);
			Log.d("TAG", "Called!!");
			result = envelope.getResponse().toString();
			try {
				resultCode = Integer.parseInt(result.trim());
			} catch (Exception e) {
				resultCode = -1;
			}
			Log.d("TAG", "Got result!!");
        } catch (Exception ex) {  
//            System.out.println(ht.requestDump);
        	Log.e("TAG", "Exception in SIM activation", ex); 
        }  
		
        Log.d("TAG", "SIM WS RESULT = " + resultCode);
        
		return resultCode;
	}
}
