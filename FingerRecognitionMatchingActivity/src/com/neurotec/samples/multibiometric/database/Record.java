package com.neurotec.samples.multibiometric.database;

import java.nio.ByteBuffer;

public final class Record {

	// ===========================================================
	// Private fields
	// ===========================================================

	private long mId;
	private String mName;
	private ByteBuffer mTemplate;
	private ByteBuffer mImage;

	// ===========================================================
	// Public constructors
	// ===========================================================

	public Record(long id, String name, ByteBuffer template) {
		this(id, name, template, null);
	}

	public Record(long id, String name, ByteBuffer template, ByteBuffer image) {
		if (name == null) {
			throw new NullPointerException("name");
		}
		if (template == null) {
			throw new NullPointerException("template");
		}

		mId = id;
		mName = name;
		mTemplate = template;
		mImage = image;
		mTemplate.rewind();
		if (mImage != null) {
			mImage.rewind();
		}
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	public long getId() {
		return mId;
	}

	public String getName() {
		return mName;
	}

	public void setName(String newName) {
		mName = newName;
	}

	public ByteBuffer getTemplate() {
		mTemplate.rewind();
		return mTemplate;
	}

	public void setTemplate(ByteBuffer template) {
		this.mTemplate = template;
	}

	public ByteBuffer getImage() {
		if (mImage != null) {
			mImage.rewind();
		}
		return mImage;
	}

	public void setImage(ByteBuffer image) {
		this.mImage = image;
	}

	@Override
	public String toString() {
		return mName;
	}
}