package com.neurotec.samples.multibiometric.fingers.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.neurotec.biometrics.NMatcher;
import com.neurotec.biometrics.NMatchingSpeed;
import com.neurotec.samples.preferences.PreferenceTools;

public final class MatchingManager {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static MatchingManager sInstance;

	// ===========================================================
	// Public static methods
	// ===========================================================

	public static MatchingManager getInstance() {
		if (sInstance == null) {
			sInstance = new MatchingManager();
		}
		return sInstance;
	}

	// ===========================================================
	// Private fields
	// ===========================================================

	private NMatcher mMatcher = null;
	private NMatcher mDefaultMatcher = null;

	// ===========================================================
	// Private constructor
	// ===========================================================

	private MatchingManager() {
		mMatcher = new NMatcher();
		mMatcher.setFingersMatchingSpeed(NMatchingSpeed.MEDIUM);

		mDefaultMatcher = new NMatcher();
		mDefaultMatcher.setFingersMatchingSpeed(NMatchingSpeed.MEDIUM);
	}

	// ===========================================================
	// Default methods
	// ===========================================================

	NMatcher getDefaultMatcher() {
		return mDefaultMatcher;
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	public NMatcher getMatcher() {
		return mMatcher;
	}

	public void update(Context context) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		mMatcher.setFingersMatchingSpeed(NMatchingSpeed.get(PreferenceTools.getIntFromString(preferences, MatchingPreferences.FINGERS_MATCHING_SPEED, mDefaultMatcher.getFingersMatchingSpeed().getValue())));
		//TODO use BiometricUtils.getMaximalRotationFromDegrees
		mMatcher.setFingersMaximalRotation(PreferenceTools.getInt(preferences, MatchingPreferences.FINGERS_MAXIMAL_ROTATION, mDefaultMatcher.getFingersMaximalRotation()));
		mMatcher.setFingersMinMatchedCount(PreferenceTools.getInt(preferences, MatchingPreferences.FINGERS_MIN_MATCHED_COUNT, mDefaultMatcher.getFingersMinMatchedCount()));
		mMatcher.setFingersMinMatchedCountThreshold(PreferenceTools.getIntFromString(preferences, MatchingPreferences.FINGERS_MIN_MATCHED_COUNT_THRESHOLD, mDefaultMatcher.getFingersMinMatchedCountThreshold()));
	}

}
