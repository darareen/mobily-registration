package com.neurotec.samples.multibiometric.fingers.preference;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import com.neurotec.biometrics.NMatcher;
import com.neurotec.biometrics.NMatchingSpeed;
import com.neurotec.samples.multibiometric.R;
import com.neurotec.samples.multibiometric.util.BiometricUtils;
import com.neurotec.samples.preferences.BasePreferenceActivity;
import com.neurotec.samples.preferences.PreferenceTools;
import com.neurotec.samples.preferences.SeekBarPreference;

import java.util.HashMap;
import java.util.Map;

public final class MatchingPreferences extends BasePreferenceActivity implements OnSharedPreferenceChangeListener {

	// ===========================================================
	// Public static fields
	// ===========================================================

	public static final String FINGERS_MATCHING_SPEED = "fingers_matching_speed";
	public static final String FINGERS_MAXIMAL_ROTATION = "fingers_maximal_rotation";
	public static final String FINGERS_MIN_MATCHED_COUNT = "fingers_min_matched_count";
	public static final String FINGERS_MIN_MATCHED_COUNT_THRESHOLD = "fingers_min_matched_count_threshold";
	public static final String SET_DEFAULT_MATCHING_PREFERENCES = "set_default_matching_preferences";

	public static final Map<NMatchingSpeed, String> MATCHING_SPEEDS = new HashMap<NMatchingSpeed, String>();

	// ===========================================================
	// Static constructor
	// ===========================================================

	static {
		MATCHING_SPEEDS.put(NMatchingSpeed.LOW, "Low");
		MATCHING_SPEEDS.put(NMatchingSpeed.MEDIUM, "Medium");
	}

	// ===========================================================
	// Private fields
	// ===========================================================

	private ListPreference mMatchingSpeed;
	private SeekBarPreference mFingersMaximalRotation;
	private SeekBarPreference mFingersMinMatchedCount;
	private ListPreference mFingersMinMatchedCountThreshold;

	// ===========================================================
	// Private methods
	// ===========================================================

	private void resetPreferences() {
		loadPreferences(MatchingManager.getInstance().getDefaultMatcher());
	}

	private void setMatchingSpeed(NMatchingSpeed value) {
		String v = String.valueOf(value.getValue());
		getPreferenceScreen().getSharedPreferences().edit().putString(FINGERS_MATCHING_SPEED, v).commit();
		mMatchingSpeed.setValue(v);
		mMatchingSpeed.setSummary(MATCHING_SPEEDS.get(value));
	}

	private void setMaximalRotation(int value) {
		getPreferenceScreen().getSharedPreferences().edit().putInt(FINGERS_MIN_MATCHED_COUNT, value).commit();
		mFingersMaximalRotation.setCurrentValue(value);
	}

	private void setMinMatchedCount(int value) {
		getPreferenceScreen().getSharedPreferences().edit().putInt(FINGERS_MIN_MATCHED_COUNT, value).commit();
		mFingersMinMatchedCount.setCurrentValue(value);
	}

	private void setMinMatchedCountThreshold(int value) {
		String v = String.valueOf(value);
		getPreferenceScreen().getSharedPreferences().edit().putString(FINGERS_MIN_MATCHED_COUNT_THRESHOLD, v).commit();
		mFingersMinMatchedCountThreshold.setValue(v);
		mFingersMinMatchedCountThreshold.setSummary(adjustPercentageCharacter(BiometricUtils.getMatchingThresholdToString(value)));
	}

	private void loadPreferences(NMatcher matcher) {
		setMatchingSpeed(matcher.getFingersMatchingSpeed());
		//TOTO use BiometricUtils.getMaximalRotationToDegrees
		setMaximalRotation(matcher.getFingersMaximalRotation());
		setMinMatchedCount(matcher.getFingersMinMatchedCount());
		setMinMatchedCountThreshold(matcher.getFingersMinMatchedCountThreshold());
	}

	private String adjustPercentageCharacter(String text) {
		if (android.os.Build.VERSION.SDK_INT >= 11) {
			return text.replaceAll("%", "%%");
		} else {
			return text;
		}
	}

	// ===========================================================
	// Protected methods
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.finger_matching_preferences);

		mMatchingSpeed = (ListPreference) getPreferenceScreen().findPreference(FINGERS_MATCHING_SPEED);
		mFingersMaximalRotation = (SeekBarPreference) getPreferenceScreen().findPreference(FINGERS_MAXIMAL_ROTATION);
		mFingersMaximalRotation.setShowMinMax(FingerPreferences.SHOW_SEEKBAR_MIN_MAX_VALUES);
		mFingersMinMatchedCount = (SeekBarPreference) getPreferenceScreen().findPreference(FINGERS_MIN_MATCHED_COUNT);
		mFingersMinMatchedCount.setShowMinMax(FingerPreferences.SHOW_SEEKBAR_MIN_MAX_VALUES);
		mFingersMinMatchedCountThreshold = (ListPreference) getPreferenceScreen().findPreference(FINGERS_MIN_MATCHED_COUNT_THRESHOLD);

		Preference resetDefaults = (Preference) findPreference(SET_DEFAULT_MATCHING_PREFERENCES);
		resetDefaults.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				resetPreferences();
				showToast(R.string.msg_settings_reset);
				return true;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadPreferences(MatchingManager.getInstance().getMatcher());
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		MatchingManager.getInstance().update(this);
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		NMatcher matcher = MatchingManager.getInstance().getDefaultMatcher();
		if (key.equals(FINGERS_MATCHING_SPEED)) {
			setMatchingSpeed(NMatchingSpeed.get(PreferenceTools.getIntFromString(sharedPreferences, FINGERS_MATCHING_SPEED, matcher.getFingersMatchingSpeed().getValue())));
		} else if (key.equals(FINGERS_MIN_MATCHED_COUNT_THRESHOLD)) {
			setMinMatchedCountThreshold(PreferenceTools.getIntFromString(sharedPreferences, FINGERS_MIN_MATCHED_COUNT_THRESHOLD, matcher.getFingersMinMatchedCountThreshold()));
		}
	}
}
