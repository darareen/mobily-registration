package com.neurotec.samples.multibiometric.fingers.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.neurotec.biometrics.NFEReturnedImage;
import com.neurotec.biometrics.NFETemplateSize;
import com.neurotec.biometrics.NFExtractor;
import com.neurotec.biometrics.NFRidgeCountsType;
import com.neurotec.samples.preferences.PreferenceTools;

public final class ExtractorManager {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static ExtractorManager sInstance;

	// ===========================================================
	// Public static methods
	// ===========================================================

	public static ExtractorManager getInstance() {
		if (sInstance == null) {
			sInstance = new ExtractorManager();
		}
		return sInstance;
	}

	// ===========================================================
	// Private fields
	// ===========================================================

	private NFExtractor mExtractor = null;
	private NFExtractor mDefaultExtractor = null;

	// ===========================================================
	// Private constructor
	// ===========================================================

	private ExtractorManager() {
		mExtractor = new NFExtractor();
		mExtractor.setTemplateSize(NFETemplateSize.SMALL);
		mExtractor.setUseQuality(false);
		mExtractor.setParameter(990, false); // SubRecordQuality
		mExtractor.setReturnedImage(NFEReturnedImage.NONE);

		mDefaultExtractor = new NFExtractor();
		mDefaultExtractor.setTemplateSize(NFETemplateSize.SMALL);
		mDefaultExtractor.setUseQuality(false);
		mDefaultExtractor.setParameter(990, false); // SubRecordQuality
		mDefaultExtractor.setReturnedImage(NFEReturnedImage.NONE);
	}

	// ===========================================================
	// Default methods
	// ===========================================================

	NFExtractor getDefaultExtractor() {
		return mDefaultExtractor;
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	public NFExtractor getExtractor() {
		return mExtractor;
	}

	public void update(Context context) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		mExtractor.setTemplateSize(NFETemplateSize.get(PreferenceTools.getIntFromString(preferences, ExtractionPreferences.EXTRACTOR_ENROLLMENT_TEMPLATE_SIZE, mDefaultExtractor.getTemplateSize().getValue())));
		mExtractor.setReturnedImage(NFEReturnedImage.get(PreferenceTools.getIntFromString(preferences, ExtractionPreferences.EXTRACTOR_RETURNED_IMAGE, mDefaultExtractor.getReturnedImage().getValue())));
		mExtractor.setExtractedRidgeCounts(NFRidgeCountsType.get(PreferenceTools.getIntFromString(preferences, ExtractionPreferences.EXTRACTOR_RIDGE_COUNTS, mDefaultExtractor.getExtractedRidgeCounts().getValue())));
		boolean useMinutiaCount = PreferenceTools.getBoolean(preferences, ExtractionPreferences.EXTRACTOR_USE_MINIMAL_MINUTIA_COUNT, ExtractionPreferences.DEFAULT_USE_MINUTIA_COUNT);
		if (useMinutiaCount) {
			mExtractor.setMinMinutiaCount(PreferenceTools.getInt(preferences, ExtractionPreferences.EXTRACTOR_MINIMAL_MINUTIA_COUNT, mDefaultExtractor.getMinMinutiaCount()));
		} else {
			mExtractor.setMinMinutiaCount(0);
		}
		mExtractor.setUseQuality(PreferenceTools.getBoolean(preferences, ExtractionPreferences.EXTRACTOR_USE_QUALITY_THRESHOLD, mDefaultExtractor.isUseQuality()));
		mExtractor.setQualityThreshold(PreferenceTools.getInt(preferences, ExtractionPreferences.EXTRACTOR_QUALITY_THRESHOLD, mDefaultExtractor.getQualityThreshold()));
	}
}
