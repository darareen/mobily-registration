package com.neurotec.samples.multibiometric.fingers.preference;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.neurotec.samples.app.BaseListActivity;
import com.neurotec.samples.licensing.preference.LicensingPreferences;
import com.neurotec.samples.multibiometric.preference.EnrollmentPreferences;

public final class FingerPreferences extends BaseListActivity {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static final String EXTRACTION = "Extraction";
	private static final String ENROLLMENT = "Enrollment";
	private static final String MATCHING = "Matching";
	private static final String LICENSING = "Licensing";

	// ===========================================================
	// Public static fields
	// ===========================================================

	public static final boolean SHOW_SEEKBAR_MIN_MAX_VALUES = true;

	// ===========================================================
	// Protected method
	// ===========================================================

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		String[] values = new String[] {EXTRACTION, ENROLLMENT, MATCHING, LICENSING};
		setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String item = (String) getListAdapter().getItem(position);
		if (item.equals(EXTRACTION)) {
			startActivity(new Intent(this, ExtractionPreferences.class));
		} else if (item.equals(ENROLLMENT)) {
			startActivity(new Intent(this, EnrollmentPreferences.class));
		} else if (item.equals(MATCHING)) {
			startActivity(new Intent(this, MatchingPreferences.class));
		} else if (item.equals(LICENSING)) {
			startActivity(new Intent(this, LicensingPreferences.class));
		}
	}
}
