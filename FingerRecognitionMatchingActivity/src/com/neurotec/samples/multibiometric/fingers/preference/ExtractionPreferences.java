package com.neurotec.samples.multibiometric.fingers.preference;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import com.neurotec.biometrics.NFEReturnedImage;
import com.neurotec.biometrics.NFETemplateSize;
import com.neurotec.biometrics.NFExtractor;
import com.neurotec.biometrics.NFRidgeCountsType;
import com.neurotec.samples.multibiometric.R;
import com.neurotec.samples.preferences.BasePreferenceActivity;
import com.neurotec.samples.preferences.PreferenceTools;
import com.neurotec.samples.preferences.SeekBarPreference;

import java.util.HashMap;
import java.util.Map;

public final class ExtractionPreferences extends BasePreferenceActivity implements OnSharedPreferenceChangeListener {

	// ===========================================================
	// Public static fields
	// ===========================================================

	public static final String EXTRACTOR_ENROLLMENT_TEMPLATE_SIZE = "extractor_enrollment_template_size";
	public static final String EXTRACTOR_RETURNED_IMAGE = "extractor_returned_image";
	public static final String EXTRACTOR_RIDGE_COUNTS = "extractor_ridge_counts";
	public static final String EXTRACTOR_USE_MINIMAL_MINUTIA_COUNT = "extractor_use_minimal_minutia_count";
	public static final String EXTRACTOR_MINIMAL_MINUTIA_COUNT = "extractor_minimal_minutia_count";
	public static final String EXTRACTOR_USE_QUALITY_THRESHOLD = "extractor_use_quality_threshold";
	public static final String EXTRACTOR_QUALITY_THRESHOLD = "extractor_quality_threshold";
	public static final String SET_DEFAULT_EXTRACTION_PREFERENCES = "set_default_extraction_preferences";

	public static final boolean DEFAULT_USE_MINUTIA_COUNT = true;

	public static final Map<NFETemplateSize, String> ENROLLMENT_TEMPLATE_SIZES = new HashMap<NFETemplateSize, String>();
	public static final Map<NFEReturnedImage, String> EXTRACTOR_RETURNED_IMAGES = new HashMap<NFEReturnedImage, String>();
	public static final Map<NFRidgeCountsType, String> RIDGE_COUNTS = new HashMap<NFRidgeCountsType, String>();

	// ===========================================================
	// Static constructor
	// ===========================================================

	static {
		ENROLLMENT_TEMPLATE_SIZES.put(NFETemplateSize.SMALL, "Small");
		ENROLLMENT_TEMPLATE_SIZES.put(NFETemplateSize.LARGE, "Large");

		EXTRACTOR_RETURNED_IMAGES.put(NFEReturnedImage.NONE, "None");
		EXTRACTOR_RETURNED_IMAGES.put(NFEReturnedImage.BINARIZED, "Binarized");
		EXTRACTOR_RETURNED_IMAGES.put(NFEReturnedImage.SKELETONIZED, "Skeletonized");

		RIDGE_COUNTS.put(NFRidgeCountsType.NONE, "None");
		RIDGE_COUNTS.put(NFRidgeCountsType.FOUR_NEIGHBORS, "Four Neighbors");
		RIDGE_COUNTS.put(NFRidgeCountsType.EIGHT_NEIGHBORS, "Eight Neighbors");
		RIDGE_COUNTS.put(NFRidgeCountsType.FOUR_NEIGHBORS_WITH_INDEXES, "Four Neighbors With Indexes");
		RIDGE_COUNTS.put(NFRidgeCountsType.EIGHT_NEIGHBORS_WITH_INDEXES, "Eight Neighbors With Indexes");
	}

	// ===========================================================
	// Private fields
	// ===========================================================

	private ListPreference mExtractorEnrollmentTemplateSize;
	private ListPreference mExtractorReturnedImage;
	private ListPreference mExtractorRidgeCounts;
	private CheckBoxPreference mExtractorUseMinimalMinutiaCount;
	private SeekBarPreference mExtractorMinimalMinutiaCount;
	private CheckBoxPreference mExtractorUseQualityThreshold;
	private SeekBarPreference mExtractorQualityThreshold;

	// ===========================================================
	// Private methods
	// ===========================================================

	private void setEnrollmentTemplateSize(NFETemplateSize value) {
		String v = String.valueOf(value.getValue());
		getPreferenceScreen().getSharedPreferences().edit().putString(EXTRACTOR_ENROLLMENT_TEMPLATE_SIZE, v).commit();
		mExtractorEnrollmentTemplateSize.setValue(v);
		mExtractorEnrollmentTemplateSize.setSummary(ENROLLMENT_TEMPLATE_SIZES.get(value));
	}

	private void setReturnedImage(NFEReturnedImage value) {
		String v = String.valueOf(value.getValue());
		getPreferenceScreen().getSharedPreferences().edit().putString(EXTRACTOR_RETURNED_IMAGE, v).commit();
		mExtractorReturnedImage.setValue(v);
		mExtractorReturnedImage.setSummary(EXTRACTOR_RETURNED_IMAGES.get(value));
	}

	private void setRidgeCounts(NFRidgeCountsType value) {
		String v = String.valueOf(value.getValue());
		getPreferenceScreen().getSharedPreferences().edit().putString(EXTRACTOR_RIDGE_COUNTS, v).commit();
		mExtractorRidgeCounts.setValue(String.valueOf(value.getValue()));
		mExtractorRidgeCounts.setSummary(RIDGE_COUNTS.get(value));
	}

	private void setUseMinimalMinutiaCount(boolean value) {
		getPreferenceScreen().getSharedPreferences().edit().putBoolean(EXTRACTOR_USE_MINIMAL_MINUTIA_COUNT, value).commit();
		mExtractorUseMinimalMinutiaCount.setChecked(value);
	}

	private void setMinimalMinutiaCount(int value) {
		getPreferenceScreen().getSharedPreferences().edit().putInt(EXTRACTOR_MINIMAL_MINUTIA_COUNT, value).commit();
		mExtractorMinimalMinutiaCount.setCurrentValue(value);
	}

	private void setUseQualityThreshold(boolean value) {
		getPreferenceScreen().getSharedPreferences().edit().putBoolean(EXTRACTOR_USE_QUALITY_THRESHOLD, value).commit();
		mExtractorUseQualityThreshold.setChecked(value);
	}

	private void setQualityThreshold(int value) {
		getPreferenceScreen().getSharedPreferences().edit().putInt(EXTRACTOR_QUALITY_THRESHOLD, value).commit();
		mExtractorQualityThreshold.setCurrentValue(value);
	}

	private void resetPreferences() {
		loadPreferences(ExtractorManager.getInstance().getDefaultExtractor());
		setUseMinimalMinutiaCount(DEFAULT_USE_MINUTIA_COUNT);
	}

	private void loadPreferences(NFExtractor extractor) {
		setEnrollmentTemplateSize(extractor.getTemplateSize());
		setReturnedImage(extractor.getReturnedImage());
		setRidgeCounts(extractor.getExtractedRidgeCounts());
		setMinimalMinutiaCount(extractor.getMinMinutiaCount());
		setUseQualityThreshold(extractor.isUseQuality());
		setQualityThreshold(extractor.getQualityThreshold());
	}

	// ===========================================================
	// Protected methods
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.finger_extraction_preferences);

		mExtractorEnrollmentTemplateSize = (ListPreference) getPreferenceScreen().findPreference(EXTRACTOR_ENROLLMENT_TEMPLATE_SIZE);
		mExtractorReturnedImage = (ListPreference) getPreferenceScreen().findPreference(EXTRACTOR_RETURNED_IMAGE);
		mExtractorRidgeCounts = (ListPreference) getPreferenceScreen().findPreference(EXTRACTOR_RIDGE_COUNTS);
		mExtractorUseMinimalMinutiaCount = (CheckBoxPreference) getPreferenceScreen().findPreference(EXTRACTOR_USE_MINIMAL_MINUTIA_COUNT);
		mExtractorMinimalMinutiaCount = (SeekBarPreference) getPreferenceScreen().findPreference(EXTRACTOR_MINIMAL_MINUTIA_COUNT);
		mExtractorMinimalMinutiaCount.setShowMinMax(FingerPreferences.SHOW_SEEKBAR_MIN_MAX_VALUES);
		mExtractorUseQualityThreshold = (CheckBoxPreference) getPreferenceScreen().findPreference(EXTRACTOR_USE_QUALITY_THRESHOLD);
		mExtractorQualityThreshold = (SeekBarPreference) getPreferenceScreen().findPreference(EXTRACTOR_QUALITY_THRESHOLD);
		mExtractorQualityThreshold.setShowMinMax(FingerPreferences.SHOW_SEEKBAR_MIN_MAX_VALUES);

		Preference resetDefaults = (Preference) findPreference(SET_DEFAULT_EXTRACTION_PREFERENCES);
		resetDefaults.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				resetPreferences();
				showToast(R.string.msg_settings_reset);
				return true;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadPreferences(ExtractorManager.getInstance().getExtractor());
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		ExtractorManager.getInstance().update(this);
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		NFExtractor extractor = ExtractorManager.getInstance().getDefaultExtractor();
		if (key.equals(EXTRACTOR_ENROLLMENT_TEMPLATE_SIZE)) {
			setEnrollmentTemplateSize(NFETemplateSize.get(PreferenceTools.getIntFromString(sharedPreferences, EXTRACTOR_ENROLLMENT_TEMPLATE_SIZE, extractor.getTemplateSize().getValue())));
		} else if (key.equals(EXTRACTOR_RETURNED_IMAGE)) {
			setReturnedImage(NFEReturnedImage.get(PreferenceTools.getIntFromString(sharedPreferences, EXTRACTOR_RETURNED_IMAGE, extractor.getReturnedImage().getValue())));
		} else if (key.equals(EXTRACTOR_RIDGE_COUNTS)) {
			setRidgeCounts(NFRidgeCountsType.get(PreferenceTools.getIntFromString(sharedPreferences, EXTRACTOR_RIDGE_COUNTS, extractor.getExtractedRidgeCounts().getValue())));
		}
	}
}
