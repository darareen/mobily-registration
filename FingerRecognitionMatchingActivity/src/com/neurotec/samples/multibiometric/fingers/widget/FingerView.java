package com.neurotec.samples.multibiometric.fingers.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.util.AttributeSet;
import android.view.View;
import com.neurotec.biometrics.*;
import com.neurotec.biometrics.NFRecord.CoreCollection;
import com.neurotec.biometrics.NFRecord.DeltaCollection;
import com.neurotec.biometrics.NFRecord.MinutiaCollection;

public class FingerView extends View {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static final int MINUTIA_DRAW_RADIUS = 7;
	private static final int MINUTIA_DRAW_LINE_LENGTH = 15;
	private static final int CORE_DRAW_LINE_LENGTH = 20;
	private static final int DELTA_DRAW_LINE_LENGTH = 20;

	// ===========================================================
	// Private fields
	// ===========================================================

	private Paint mPaint = null;
	private Bitmap mBitmap = null;
	private NFRecord mRecord = null;
	private int mImageWidth = -1;
	private int mImageHeight = -1;
	private float mImageScale = 1f;

	// ===========================================================
	// Public constructor
	// ===========================================================

	public FingerView(Context context, AttributeSet attrs) {
		super(context, attrs);
		mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeWidth(2);
		mPaint.setFilterBitmap(true);
	}

	// ===========================================================
	// Private methods
	// ===========================================================

	private void updateImageSize() {
		int prevWidth = mImageWidth;
		int prevHeight = mImageHeight;
		if (mBitmap != null) {
			mImageWidth = mBitmap.getWidth();
			mImageHeight = mBitmap.getHeight();
		} else if (mRecord != null) {
			mImageWidth = mRecord.getWidth();
			mImageHeight = mRecord.getHeight();
		} else {
			mImageWidth = -1;
			mImageHeight = -1;
		}
		if (mImageWidth != prevWidth || mImageHeight != prevHeight) {
			requestLayout();
		}
	}

	private void drawMinutiae(Canvas canvas, MinutiaCollection minutiae) {
		if (minutiae == null) {
			return;
		}
		float l = MINUTIA_DRAW_LINE_LENGTH;
		mPaint.setStrokeWidth(2);
		Path p = new Path();
		for (NFMinutia min : minutiae) {
			int x = min.x;
			int y = min.y;
			p.addCircle(x, y, MINUTIA_DRAW_RADIUS, Direction.CW);
			if (min.type == NFMinutiaType.BIFURCATION) {
				double angle = min.getAngle();
				float dx = (float) (l * Math.cos(angle - (Math.PI / 12)));
				float dy = (float) (l * Math.sin(angle - (Math.PI / 12)));
				p.moveTo(x, y);
				p.lineTo(x + dx, y + dy);
				dx = (float) (l * Math.cos(angle + (Math.PI / 12)));
				dy = (float) (l * Math.sin(angle + (Math.PI / 12)));
				p.moveTo(x, y);
				p.lineTo(x + dx, y + dy);
			} else {
				double angle = min.getAngle();
				float dx = (float) (l * Math.cos(angle));
				float dy = (float) (l * Math.sin(angle));
				p.moveTo(x, y);
				p.lineTo(x + dx, y + dy);
			}
		}
		canvas.drawPath(p, mPaint);
	}

	private void drawDeltas(Canvas canvas, DeltaCollection deltas) {
		if (deltas == null) {
			return;
		}
		float s = DELTA_DRAW_LINE_LENGTH / 2;
		int s1 = Math.round(s);
		int z = (int) Math.round((s1 * Math.sqrt(3)) / 2);
		int r = s1 / 2;
		Path p = new Path();
		for (NFDelta delta : deltas) {
			int x = delta.x;
			int y = delta.y;

			p.moveTo(x, y - s1);
			p.lineTo(x - z, y + r);
			p.lineTo(x + z, y + r);
			p.close();

			drawDeltaAngle(p, x, y, delta.angle1, delta.getAngle1());
			drawDeltaAngle(p, x, y, delta.angle2, delta.getAngle2());
			drawDeltaAngle(p, x, y, delta.angle3, delta.getAngle3());
		}
		mPaint.setStrokeWidth(3);
		canvas.drawPath(p, mPaint);
	}

	private void drawDeltaAngle(Path p, int x, int y, int rawAngle, double angle) {
		if ((rawAngle != -1) && (!Double.isNaN(angle))) {
			float dx = (float) (DELTA_DRAW_LINE_LENGTH * Math.cos(angle));
			float dy = (float) (DELTA_DRAW_LINE_LENGTH * Math.sin(angle));
			p.moveTo(x, y);
			p.lineTo(x + dx, y + dy);
		}
	}

	private void drawCores(Canvas canvas, CoreCollection cores) {
		if (cores == null) {
			return;
		}
		float s = CORE_DRAW_LINE_LENGTH / 2;
		Path p = new Path();
		for (NFCore core : cores) {
			int x = core.x;
			int y = core.y;

			p.addRect(x - s, y - s, x + s, y + s, Direction.CW);

			double angle = core.getAngle();
			if ((core.angle != -1) && (!Double.isNaN(angle))) {
				float dx = (float) (CORE_DRAW_LINE_LENGTH * Math.cos(angle));
				float dy = (float) (CORE_DRAW_LINE_LENGTH * Math.sin(angle));
				p.moveTo(x, y);
				p.lineTo(x + dx, y + dy);
			}
		}
		mPaint.setStrokeWidth(3);
		canvas.drawPath(p, mPaint);
	}

	// ===========================================================
	// Protected methods
	// ===========================================================

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		if (mImageWidth == -1 || mImageHeight == -1) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
			return;
		}
		float horzScale = 1f;
		switch (MeasureSpec.getMode(widthMeasureSpec)) {
			case MeasureSpec.EXACTLY:
				horzScale = (float) MeasureSpec.getSize(widthMeasureSpec)
						/ mImageWidth;
				break;
			case MeasureSpec.AT_MOST:
				if (MeasureSpec.getSize(widthMeasureSpec) < mImageWidth) {
					horzScale = (float) MeasureSpec.getSize(widthMeasureSpec)
							/ mImageWidth;
				}
				break;
		}
		float vertScale = 1f;
		switch (MeasureSpec.getMode(heightMeasureSpec)) {
			case MeasureSpec.EXACTLY:
				vertScale = (float) MeasureSpec.getSize(heightMeasureSpec)
						/ mImageHeight;
				break;
			case MeasureSpec.AT_MOST:
				if (MeasureSpec.getSize(heightMeasureSpec) < mImageHeight) {
					vertScale = (float) MeasureSpec.getSize(heightMeasureSpec)
							/ mImageHeight;
				}
				break;
		}

		mImageScale = Math.min(horzScale, vertScale);
		int scaledWidth = (int) (mImageWidth * mImageScale + 0.5f);
		int scaledHeight = (int) (mImageHeight * mImageScale + 0.5f);
		setMeasuredDimension(scaledWidth, scaledHeight);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (mImageWidth == -1 || mImageHeight == -1) {
			return;
		}
		canvas.scale(mImageScale, mImageScale);
		if (mBitmap != null) {
			canvas.drawBitmap(mBitmap, 0, 0, mPaint);
		}
		if (mRecord != null) {
			canvas.scale(mRecord.getHorzResolution() / 500.0f,
					mRecord.getVertResolution() / 500.0f);
			mPaint.setColor(0x66FF6600);
			drawDeltas(canvas, mRecord.getDeltas());
			drawCores(canvas, mRecord.getCores());
			mPaint.setColor(0x99669933);
			drawMinutiae(canvas, mRecord.getMinutiae());
		}
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	public void setBitmap(Bitmap bitmap) {
		mBitmap = bitmap;
		updateImageSize();
		invalidate();
	}

	public Bitmap getBitmap() {
		return mBitmap;
	}

	public void setRecord(NFRecord record) {
		mRecord = record;
		updateImageSize();
		invalidate();
	}

	public NFRecord getRecord() {
		return mRecord;
	}

};