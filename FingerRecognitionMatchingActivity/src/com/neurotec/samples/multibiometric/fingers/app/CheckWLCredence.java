package com.neurotec.samples.multibiometric.fingers.app;
import com.neurotec.samples.multibiometric.R;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import nic.client.WSCheckCWL;
import nic.common.datamodel.WatchListRecord;

/**
 * Created by IntelliJ IDEA.
 * User: root
 * Date: 4/23/13
 * Time: 9:50 PM
 * To change this template use File | Settings | File Templates.
 */


/* Wael
This activity corresponds to the one meant for checking whether a person is on the watch list/blacklist.
 */


public class CheckWLCredence extends Activity
{
    private EditText edittext;
    private Button submit;
    private Spinner spinner1;
    WSCheckCWL ws = new WSCheckCWL();
    private int type;
    private String nin;
    private WatchListRecord[] list;
    private TextView tv;
    private TextView tvmain;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkwl);

        edittext = (EditText)findViewById(R.id.etExpiryDate);

        tv = (TextView)findViewById(R.id.checkwlres);

        spinner1 = (Spinner) findViewById(R.id.spinner1);
	    spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());

        submit = (Button)findViewById(R.id.checkwlbtn);
        submit.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v)
                {
                    Toast.makeText(CheckWLCredence.this, "جاري الاستعلام", Toast.LENGTH_SHORT).show();
                    String typeS = String.valueOf(spinner1.getSelectedItem());
                    type = 0;
                    if (typeS.equals("Citizen"))
                        type = 1;
                    else if (typeS.equals("Resident"))
                        type = 2;
                    else if (typeS.equals("Visitor"))
                        type = 3;

                    nin = edittext.getText().toString();

                    new GetDataTask().execute("call");

                    // For testing:
                    //Toast.makeText(CheckWLCredence.this, nin+" - "+type, Toast.LENGTH_LONG).show();
                }
			});


    }


    public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener
    {
       public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {

          }

          @Override
          public void onNothingSelected(AdapterView<?> arg0) {

          }

    }

    private class GetDataTask extends AsyncTask<String, Void, String>
    {

        protected String doInBackground(String... urls)
		{
            try
            {
                Thread.sleep(3000);
                try
                {
                    /* Wael
                    Performing the webservice call in a background thread. The last three parameters should be hardcoded
                    like this (this might change when we go to production environment).
                     */

                    list = ws.checkCWL(new Long(Long.parseLong(nin)), type, "TBIS", 1000000057L, 1000);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

            }
            catch (InterruptedException e)
            {

            }
            return "done";
        }

        @Override
        protected void onPostExecute(String result)
        {
            /* Wael
            Populating the screen with the returned results.
             */
            String output = populateTable(list);

            //For testing.. remove later,, and uncomment the line above.
        /*    WatchListRecord [] testlist = new WatchListRecord[2];
            testlist[0] = new WatchListRecord();
            testlist[1] = new WatchListRecord();

            testlist[0].setActionMessage("Action message test 1");
            testlist[0].setSourceMessage("Source Message test 1");

            testlist[1].setActionMessage("Action message test 2");
            testlist[1].setSourceMessage("Source Message test 2");

            String output = populateTable(testlist);    */

            tv.setTextColor(Color.BLACK);
            tv.setText(output);
            super.onPostExecute(result);
        }
    }

    private String populateTable(WatchListRecord[] list) {
        String res = "";

        for (int i = 0; i < list.length; i++)
        {
            res = res + "#"+(i+1)+"\n";
            res = res + "Action Message: \t\t " +list[i].getActionMessage()+"\n";
            res = res + "Source Message: \t\t " +list[i].getSourceMessage()+"\n";

            res = res + "SAMIS ID: \t\t " +list[i].getSamisId()+"\n";
            res = res + "Action Code: \t\t " +list[i].getActionCode()+"\n";

            res = res + "Source Code: \t\t " +list[i].getSourceCode()+"\n";
            res = res + "Source ID: \t\t " +list[i].getSourceId()+"\n";

            // Add them all..

            res = res + "----------------"+"\n";

        }

        if (list.length == 0)
            res = "Nothing received.. no data to show";

        return res;
    }

}