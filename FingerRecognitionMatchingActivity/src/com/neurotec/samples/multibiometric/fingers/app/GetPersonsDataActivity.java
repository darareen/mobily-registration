package com.neurotec.samples.multibiometric.fingers.app;

import java.io.File;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import com.neurotec.samples.multibiometric.BiometricApplication;
import com.neurotec.samples.multibiometric.R;

import nic.client.Identification;
import nic.common.datamodel.Person;
import nic.common.datamodel.RegistrationData;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class GetPersonsDataActivity extends Activity {
	private static final String TAG = GetPersonsDataActivity.class.getSimpleName();
	protected static final int dialog_time = 0;
	private EditText edittext;
	private Button submit;
	private Spinner spinner1;
	Identification individualidenty = new Identification();
	private TextView tv;
	private long smaisID;
	private String sessionId = "";
	private int personType;
	private Person person;
	private EditText etExpDate;
	private ImageButton btnExpDate;
	private int Day = -1;
	private int Month = -1;
	private int Year = -1;
	private int cDay;
	private int cMonth;
	private int cYear;
	
	//RegstrationData reference
	RegistrationData registrationData;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.getperson);
		
		//Create a new instance for a new registration flow
		((BiometricApplication)getApplicationContext()).registrationData = new RegistrationData();
		registrationData = ((BiometricApplication)getApplicationContext()).registrationData;
		 
		// configure header
		if (getIntent().getBooleanExtra("EXIT", false)) {
			Log.i(TAG,"inside getintent");
			callExit();
		}
		
		BiometricApplication.clearCacheData();
		
		final LinearLayout llHeaderSection = (LinearLayout) findViewById(R.id.tvTitle_ref);
				((TextView) llHeaderSection.findViewById(R.id.tvTitle)).setText(R.string.home_page_title);
				llHeaderSection.findViewById(R.id.headerButtonRight).setVisibility(View.INVISIBLE);
				llHeaderSection.findViewById(R.id.headerButtonLeft).setVisibility(View.VISIBLE);

		
		tv = (TextView) findViewById(R.id.getpersonmsg);
		edittext = (EditText) findViewById(R.id.etNationalID);

		spinner1 = (Spinner) findViewById(R.id.spinner2);
		spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());

		etExpDate = (EditText) findViewById(R.id.etExpiryDate);
		btnExpDate = (ImageButton) findViewById(R.id.ibExpirydate);
		btnExpDate.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showDialog(dialog_time);
			}
		});

		Button exitButton = (Button) llHeaderSection.findViewById(R.id.headerButtonLeft);
		exitButton.setText("Exit");
		exitButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				callExit();
			}
		});
		
		final Calendar c = Calendar.getInstance();
		Day = c.get(Calendar.DAY_OF_MONTH);
		Month = c.get(Calendar.MONTH);
		Year = c.get(Calendar.YEAR);
		updateDisplay();
		cDay = Day;
		cMonth = Month;
		cYear = Year;
		submit = (Button) findViewById(R.id.getpersonbtn);
		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

					String strPersonType = String.valueOf(spinner1
							.getSelectedItem());
					if (strPersonType != null && strPersonType.equals("Citizen"))
						personType = 1;
					else if (strPersonType != null && strPersonType.equals("Resident"))
						personType = 2;
					else if (strPersonType != null && strPersonType.equals("Visitor"))
						personType = 3;
					else if (strPersonType != null && strPersonType.equals("Illegal"))
						personType = 4;
					else if (strPersonType != null && strPersonType.equals("Deportee"))
						personType = 5;
					else if (strPersonType != null && strPersonType.equals("Pilgrim"))
						personType = 6;
					else
						personType = 0;

					
					smaisID = Long.parseLong(edittext.getText().toString());
					getPersonData(strPersonType, String.valueOf(smaisID), String.valueOf(personType));
			}
					
		});
		
	}
	
	private void getPersonData(String sessionId, String smaisID, String personType) {
		try {
			CallWSTask task = new CallWSTask();
			task.execute(sessionId, smaisID, personType);
		} catch (Exception e) {
			Log.e(TAG, "Exception while fetching person's data", e);
			AlertDialog.Builder builder = new AlertDialog.Builder(GetPersonsDataActivity.this);
			builder.setTitle("Verification Error")
					.setMessage("Unable to verify the ID. Please try again.")
			       .setCancelable(false)
			       .setPositiveButton("OK", null);
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	private class CallWSTask extends AsyncTask<String, Void, Person> {

		Identification individualidenty = new Identification();
		ProgressDialog progDailog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(GetPersonsDataActivity.this);
            progDailog.setMessage("Verifying ID");
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
		}
		
		@Override
		protected Person doInBackground(String... params) {
			return individualidenty.getPersonData(sessionId, smaisID,	personType);
		}
		
		@Override
		protected void onPostExecute(Person result) {
			super.onPostExecute(result);
			person = result;
			if(progDailog.isShowing())
				progDailog.dismiss();
			
			if (person != null) {
				
				registrationData.setPerson(person);
				
				if (person.getMinutiaeData().getFirstMinutiae().getMinutiaeValue() != null || 
						person.getMinutiaeData().getSecondMinutiae().getMinutiaeValue() != null) { 
					Intent gotoCaptureFP = new Intent(GetPersonsDataActivity.this, FingerRecognitionMatchingActivity.class);
					startActivity(gotoCaptureFP);
				} 
				else {
					Intent gotoCaptureFP = new Intent(GetPersonsDataActivity.this, ContinueFingerPrint.class);
					startActivity(gotoCaptureFP);
					}

			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(GetPersonsDataActivity.this);
				builder.setTitle("Invalid ID")
						.setMessage("Please enter a valid ID")
				       .setCancelable(false)
				       .setPositiveButton("OK", null);
				AlertDialog alert = builder.create();
				alert.show();
			}
		}
	}
	
	private void updateDisplay() {
		// TODO Auto-generated method stub

		etExpDate.setText(new StringBuilder().append(Day).append("/")
				.append(Month+1).append("/").append(Year));
	}

	private DatePickerDialog.OnDateSetListener mDateListener = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			// TODO Auto-generated method stub
			Year = year;
			Month = monthOfYear;
			Day = dayOfMonth;
			updateDisplay();
		}
	};

	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case dialog_time:
			return new DatePickerDialog(this, mDateListener, Year, Month, Day);
		}
		return null;
	}

	public class CustomOnItemSelectedListener implements
			AdapterView.OnItemSelectedListener {
		public void onItemSelected(AdapterView<?> parent, View view, int pos,
				long id) {

			String selectedIndType = parent.getItemAtPosition(pos).toString();
			if (!"Citizen".equals(selectedIndType)) {
				etExpDate.setVisibility(View.VISIBLE);
				btnExpDate.setVisibility(View.VISIBLE);
			} else {
				etExpDate.setVisibility(View.INVISIBLE);
				btnExpDate.setVisibility(View.INVISIBLE);
			}
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {

		}

	}

	private boolean validateExpiryDate() {

		Log.i("GetPerson", "validateExpiryDate");
		boolean isDateValid = true;
		Calendar currentDate = Calendar.getInstance();
		currentDate.set(cYear, cMonth, cDay);
		Calendar selectedDate = Calendar.getInstance();
		selectedDate.set(Year, Month, Day);

		if (Day == -1 || Month == -1 || Year == -1) {
			isDateValid = false;
		} else if (currentDate.before(selectedDate)) {
			isDateValid = false;
		}

		return isDateValid;
	}
	
	private void callExit() {
		Log.i(TAG,"inside callExit");
		finish();
	}
}
