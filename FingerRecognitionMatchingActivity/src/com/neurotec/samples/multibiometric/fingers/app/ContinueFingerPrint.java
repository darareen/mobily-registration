package com.neurotec.samples.multibiometric.fingers.app;

import java.io.File;
import java.util.List;

import mobily.ws.TenLinesWS;
import nic.common.datamodel.RegistrationData;

import com.neurotec.samples.multibiometric.BiometricApplication;
import com.neurotec.samples.multibiometric.R;
import com.readystatesoftware.viewbadger.BadgeView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class ContinueFingerPrint extends Activity {

	private static final String TAG = ContinueFingerPrint.class.getSimpleName();
	private Button btnContinue;
	private String gender;
	//RegstrationData reference
  	RegistrationData registrationData;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.continuefingerprint);
		
		registrationData = ((BiometricApplication)getApplicationContext()).registrationData;
		
		if (registrationData.getPerson().getGender() == 1)
			gender = "Male";
		else
			gender = "Female";
    
		final LinearLayout llHeaderSection = (LinearLayout) findViewById(R.id.tvTitle_ref);
		final TextView txtTitle = (TextView) llHeaderSection.findViewById(R.id.tvTitle);
		txtTitle.setText("Person Details");
		
		final Button rightButton = (Button) llHeaderSection.findViewById(R.id.headerButtonRight);
		rightButton.setText("New");
		rightButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(ContinueFingerPrint.this, GetPersonsDataActivity.class);;
				startActivity(intent);
				finish();
			}
		});

		final Button leftButton = (Button) llHeaderSection.findViewById(R.id.headerButtonLeft);
		leftButton.setText("Exit");
		leftButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG,"inside Exit");
				final Intent mainActivity = new Intent(ContinueFingerPrint.this,
						GetPersonsDataActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				mainActivity.putExtra("EXIT", true);
				startActivity(mainActivity);
				finish();
			}
		});
		final TextView txtHeader = (TextView) this.findViewById(R.id.txtHeader);
		txtHeader.setText("Person Details");
		
		final TextView txtID = (TextView) this.findViewById(R.id.txtID);
		txtID.setText(String.valueOf(registrationData.getPerson().getIdNumber()));
		
		final TextView txtFullName = (TextView) this.findViewById(R.id.txtFullName);
		txtFullName.setText(registrationData.getPerson().getFirstName() + " " + registrationData.getPerson().getFamilyName());
		
		final TextView txtGender = (TextView) this.findViewById(R.id.txtGender);
		txtGender.setText(gender);
		
		final TextView txtIDExpiryDate = (TextView) this.findViewById(R.id.txtExpiryDate);
		txtIDExpiryDate.setText(registrationData.getPerson().getIdExpDate());

		final TextView txtEnrollmentStatus = (TextView) this.findViewById(R.id.txtEnrollmentStatus);
		txtEnrollmentStatus.setText("Not Enrolled");
		
		btnContinue = (Button) findViewById(R.id.btncontinue);
		btnContinue.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TenLinesWS tenLinesWS = new TenLinesWS();
            	boolean lessThanTen = tenLinesWS.isTenLines(registrationData.getPerson().getIdNumber());
            	
            	if(lessThanTen) {
            		Intent intent = new Intent(ContinueFingerPrint.this, IdAndSigCaptureActivity.class);
    				startActivity(intent);
            	} else {
            		Toast.makeText(getBaseContext(), "User has 10 lines or more", Toast.LENGTH_SHORT).show();
            	}
			}
		});
	}
}
