package com.neurotec.samples.multibiometric.fingers.app;
import com.neurotec.samples.multibiometric.R;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import nic.client.Identification;
import nic.client.IdentificationProcess;
import nic.client.ReportEnrollment;
import nic.common.datamodel.EnrollmentInfo;
import nic.common.datamodel.Finger;
import nic.common.datamodel.FingerType;
import nic.common.datamodel.Person;

/**
 * Created by IntelliJ IDEA.
 * User: root
 * Date: 5/5/13
 * Time: 10:01 PM
 * To change this template use File | Settings | File Templates.
 */


/* Wael
This activity shows the progress of fingerprints search. It shows what each webservice call is returning.
 */

public class FingerDetails extends Activity
{
    private TextView tv;
    private TextView tv2;
    private Finger[] finger;
    private long CAISE_NUMBER;
    private EnrollmentInfo[] enrollmentInfo;
    private long SAMID_NUMBER;
    private Button btnCaptureSig;

    Identification m = new Identification();
    private Person p = new Person();
    private ImageView img;
    private boolean enroll = true;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fingersres);


//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//		StrictMode.setThreadPolicy(policy);


        tv = (TextView)findViewById(R.id.fingersmsg);
        tv2 = (TextView)findViewById(R.id.fingersdetails);
        btnCaptureSig = (Button)findViewById(R.id.getpersonsig);
        
        img = (ImageView)findViewById(R.id.imageView1fingers);
        img.setVisibility(View.INVISIBLE);


        /* Wael
        Getting the fingerprint images passed from the previous activity
         */

        Bundle extras = getIntent().getExtras();
        finger = new Finger[10];

        finger[6]    = new Finger(FingerType.RIGHT_THUMB_ROLL, extras.getByteArray("rThumb"));

        finger[7]   = new Finger(FingerType.RIGHT_INDEX_ROLL,extras.getByteArray("rIndex"));

        finger[8]    = new Finger(FingerType.RIGHT_MIDDLE_ROLL,extras.getByteArray("rMiddle"));

        finger[9]   = new Finger(FingerType.RIGHT_RING_ROLL   ,extras.getByteArray("rRing"));

        finger[0]   = new Finger(FingerType.RIGHT_PINKY_ROLL  ,extras.getByteArray("rPinkie"));

        finger[5]    = new Finger(FingerType.LEFT_THUMB_ROLL,extras.getByteArray("lThumb"));

        finger[4]    = new Finger(FingerType.LEFT_INDEX_ROLL  ,extras.getByteArray("lIndex"));

        finger[3]   = new Finger(FingerType.LEFT_MIDDLE_ROLL,extras.getByteArray("lMiddle"));

        finger[2]   = new Finger(FingerType.LEFT_RING_ROLL,extras.getByteArray("lRing"));

        finger[1]   = new Finger(FingerType.LEFT_PINKY_ROLL,extras.getByteArray("lPinkie"));


        // Just a normal print statement to check if there were indeed some received data from previous activity.

        tv.setText("جاري البحث الآن بالإصبع:"+"  ...  "
                +finger[finger.length-1].getImage().toString());

//        tv.setText("Searching starts now for fingers: "+finger.length);

        startSearching();
        btnCaptureSig.setVisibility(View.VISIBLE);
        btnCaptureSig.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});

    }

    private void startSearching()
    {
        tv2.setTextColor(Color.BLUE);
        tv2.setText("جاري البحث ...");
        new GetDataTask().execute("call1");
    }

    private class GetDataTask extends AsyncTask<String, Void, String>
    {

        protected String doInBackground(String... urls)
		{
            // Simulates a background job.
            try
            {
                Thread.sleep(3000);

                /* Wael
                First call will return an inquiry ID (called CAISE_Number). This number will be utilized to check
                asynch. whther the search has been complete with results.
                 */
                IdentificationProcess process = new IdentificationProcess();
                CAISE_NUMBER = process.searchFingers(finger, 1000000057L, 1000);
            //    CAISE_NUMBER = 600005451L; //Ali 5451   Wael 5440

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return "done";
        }

        @Override
        protected void onPostExecute(String result)
        {
            tv2.setTextColor(Color.BLUE);
            tv2.setText("رقم البحث في نظام مركز المعلومات:<<<"+CAISE_NUMBER+">>>جاري الاستعلام عن النتيجة");
            new GetEnrollmentTask().execute("call2");
            super.onPostExecute(result);
        }
    }



    private class GetEnrollmentTask extends AsyncTask<String, Void, String>
    {
        protected String doInBackground(String... urls)
		{
            try
            {
                /* Wael
                We call this method to keep checking for a result for the CAISE_Number collected before.
                The result when returned, it has the form of an array of one or more SAMIS_IDs (i.e. iqama numbers
                or Saudi national ID numbers).
                 */
                ReportEnrollment rep = new ReportEnrollment();
                enrollmentInfo = rep.retrieveEnrollmentResult(CAISE_NUMBER);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return "done";
        }

        @Override
        protected void onPostExecute(String result)
        {
            tv2.setTextColor(Color.BLUE);
            String res = "";

            if(enrollmentInfo != null)
            {
                if(enrollmentInfo.length > 0)
                    res = enrollmentInfo[0].getEnrollee() +" "+enrollmentInfo[0].getType();
                tv2.setText("--> تم الحصول على بيانات الشخص بطول: <<<"+enrollmentInfo.length+">>> \n والنتيجة : <<<"+res+">>>"
                        +"\nجاري الحصول على باقي المعلومات");


                /* Wael
                We call the method that will bring the detailed information for each SAMIS ID returned.
                 */
                new GetPersonTask().execute("call3");
                super.onPostExecute(result);
            }
            else enroll = false;

        }
    }

 /*   private class GetEnrollmentReportTask extends AsyncTask<String, Void, String>
    {

        protected String doInBackground(String... urls)
		{
            // Simulates a background job.
            try
            {
                Thread.sleep(2000);
                ReportEnrollment rep = new ReportEnrollment();
                SAMID_NUMBER = rep.queryEnrollmentReport(enrollmentInfo, CAISE_NUMBER);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return "done";
        }

        @Override
        protected void onPostExecute(String result)
        {
            tv2.setTextColor(Color.BLUE);
            tv2.setText("** Acquired needed details.. SAMID_NUMBER: <<<"+SAMID_NUMBER+">>>"+"\nnow listing info:");
            new GetPersonTask().execute("call4");
            super.onPostExecute(result);
        }
    }  */

    private class GetPersonTask extends AsyncTask<String, Void, String>
    {

        protected String doInBackground(String... urls)
		{
            // Simulates a background job.
            try
            {
                Thread.sleep(2000);

                try
                {
                    EnrollmentInfo OneRecord;
                    if (enrollmentInfo != null )
                    {
                        for (int j =0; j < enrollmentInfo.length; j++)
                        {
                            OneRecord = enrollmentInfo[j];

                            Identification ID = new Identification();
                            p = ID.getPersonData("", OneRecord.getEnrollee(), OneRecord.getType());
                        }
                    }
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }

            }
            catch (InterruptedException e)
            {

            }
            return "done";
        }

        @Override
        protected void onPostExecute(String result)
        {
            String output = populateTable(p.getFullName(), p.getBirthGDate(), p.getGender()+"", p.getIdNumber()+"", p.getNationality());

            //for testing.. uncomment the line above and comment this test.
//            String output = populateTable("Ahmad Zaid", "10-9-1980", "Male", "1032095", "Saudi");

            tv2.setTextColor(Color.BLACK);
            tv2.setText(output);
//            Bitmap bmp = BitmapFactory.decodeByteArray(p.getFacialPhoto().getFacialPhoto(), 0, p.getFacialPhoto().getFacialPhoto().length);
//            img.setImageBitmap(bmp);
//            img.setVisibility(View.VISIBLE);
            super.onPostExecute(result);
        }
    }

    private String populateTable(String fullName, String birthGDate, String gender, String idNumber, String nationality)
    {
        String res = "";
        String[] labels = new String[5];
        labels[0] = "الاسم الكامل";
        labels[1] = "تاريخ الميلاد";
        labels[2] = "الجنس";
        labels[3] = "رقم الهوية";
        labels[4] = "الجنسية";

        String[] person = new String[5];
        person[0] = fullName;
        person[1] = birthGDate;
        person[2] = gender;
        person[3] = idNumber;
        person[4] = nationality;

        for (int i = 0; i < labels.length; i++)
        {
            res = res + labels[i]+"\t\t"+person[i]+"\n";
        }

        return res;
    }
}
