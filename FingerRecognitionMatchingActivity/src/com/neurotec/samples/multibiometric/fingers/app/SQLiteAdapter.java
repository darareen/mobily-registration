package com.neurotec.samples.multibiometric.fingers.app;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;


public class  SQLiteAdapter {
	
	static File sdCard = Environment.getExternalStorageDirectory();
//	static String DBLocation =  "/mnt/sdcard/external_sd/post.sqlite";

	static String DBLocation =  sdCard.toString() + "/post.sqlite";

	static final String MYDATABASE_NAME =DBLocation;

	public static final String MYDATABASE_TABLE = "packages";
	
	public static final int MYDATABASE_VERSION = 1;
	
	public static final String KEY_CONTENT = "Content";

	private SQLiteDatabase myDatabase = null;

	//create table MY_DATABASE (ID integer primary key, Content text not null)
 
 public SQLiteAdapter(){
  myDatabase = OpenDatabase();
 }
 
public SQLiteDatabase OpenDatabase() {
 SQLiteDatabase database = null;
 try {
	 Log.i("Database Location", MYDATABASE_NAME);
	 database = SQLiteDatabase.openDatabase(MYDATABASE_NAME, null,
       SQLiteDatabase.OPEN_READWRITE);
 } catch (SQLiteException e) {
   final String message = e.getMessage();
   if (message == null) {
     throw e;
   }
   if (!message.contains("attempt to write a readonly database")) {
     throw e;
   }
 }
 return database;
}

public PackageRecord SelectSingle(String PackageID, String RecipientName, String RecipientAddress, String RecipientPhone, byte[] REcipientSignature) {

	PackageRecord PR = new PackageRecord();

	String[] tableColumns = new String[] {
		    "RecipientName",
		    "RecipientAddress",
		    "RecipientPhone",
		    "RecipientSignature"
		};
String whereClause = "Packageid = '" + PackageID + "'" ;

try{
Cursor cursor = myDatabase.query(MYDATABASE_TABLE, tableColumns, whereClause, null,
		        null, null, null);

PR.NumberOfRecords = cursor.getCount();

if ( cursor.getCount() > 0 ) {
		  cursor.moveToFirst();
		  PR.RecipientName = cursor.getString(0);
	      PR.RecipientAddress = cursor.getString(1);
		  PR.RecipientPhone = cursor.getString(2);
		  PR.RecipientSignature = cursor.getBlob(3);
//Log.v("Retrieved Length", Integer.toString(PR.RecipientSignature.length));
}

else
{
	PR.ErrorMsg = "Customized No Records Found";
}
cursor.close();
myDatabase.close();
return PR;
}

catch (SQLiteException exception) {
    Log.i("SQl Exception while selcting single package record", "Second Line");
    PR.ErrorMsg = exception.getMessage();
    exception.printStackTrace();
    return PR;
} finally {
    myDatabase.close();
}
}


public void UpdateSingle(String PackageID, byte[] RecipientSignature) {

	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
	String currentDateandTime = sdf.format(new Date());
	
    Log.i("Signature lenght",  Integer.toString(RecipientSignature.length));
    
	ContentValues Cvalues = new ContentValues();
	byte[] TheBytes = Base64.encode(RecipientSignature, Base64.DEFAULT ) ;
	Cvalues.put("RecipientSignature", TheBytes);
	Cvalues.put("DeliveredDate", currentDateandTime);
	Cvalues.put("DeliveredStatus", 1);
try {

	myDatabase.update("packages", Cvalues , "Packageid = '" + PackageID + "'",null  );
	
	}

catch (SQLiteException exception) {
    Log.i("SQl Exception while updating single package record", "Second Line");
    exception.printStackTrace();
    return;
} finally {
    myDatabase.close();

}
}



}
 
 