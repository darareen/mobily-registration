package com.neurotec.samples.multibiometric.fingers.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.credenceid.biometrics.Biometrics;
import com.credenceid.biometrics.BiometricsActivity;
import com.neurotec.samples.multibiometric.BiometricApplication;
import com.neurotec.samples.multibiometric.R;

import nic.common.datamodel.RegistrationData;

/**
 * Created by IntelliJ IDEA.
 * User: root
 * Date: 4/9/13
 * Time: 8:16 PM
 * To change this template use File | Settings | File Templates.
 */


public class TestCredence extends BiometricsActivity implements Biometrics.OnConvertToWsqListener
{
	private ImageView iv;
	
	//RegstrationData reference
	RegistrationData registrationData;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finger_credence);
        
        registrationData = ((BiometricApplication)getApplicationContext()).registrationData;

        iv =  (ImageView)this.findViewById(R.id.iv);

        grabFingerprint();
    }


    @Override
    public void onFingerprintGrabbed(ResultCode result, Bitmap bm, byte[] iso, String filepath, String status)
    {
        if (result.equals(Biometrics.ResultCode.OK))
        {
        	iv.setImageBitmap(bm);
        	registrationData.setCapturedFingerPrint(bm);

			Intent gotoView = new Intent(TestCredence.this,
					FingerRecognitionMatchingActivity.class);
			startActivity(gotoView);
        }
    }
}
