package com.neurotec.samples.multibiometric.fingers.app;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import nic.common.datamodel.RegistrationData;

import com.neurotec.samples.multibiometric.BiometricApplication;
import com.neurotec.samples.multibiometric.R;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PrintActivity extends Activity {

	private StringBuilder textToPrint = new StringBuilder("Dar Areen\n--------------\n");
	
	private TextView infomation;
	private TextView status;
	private Button printButton;
	private Button newCustomerButton;
	
	private boolean findBT = false;
	private boolean openBT = false;
	private boolean printToBT = false;
	private boolean closeBT = false;
	
	// android built in classes for bluetooth operations
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
    
    byte[] readBuffer;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;
    
    private static final String TAG = PrintActivity.class.getSimpleName();
    
    //RegstrationData reference
  	RegistrationData registrationData;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_print);
		
		registrationData = ((BiometricApplication)getApplicationContext()).registrationData;
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		infomation = (TextView) findViewById(R.id.customerInfo);
		status = (TextView) findViewById(R.id.printStatus);
		
		final LinearLayout llHeaderSection = (LinearLayout) findViewById(R.id.tvTitle_ref);
		final TextView txtTitle = (TextView) llHeaderSection.findViewById(R.id.tvTitle);
		txtTitle.setText("SIM Details");
		
		final Button rightButton = (Button) llHeaderSection.findViewById(R.id.headerButtonRight);
		rightButton.setText("New");
		rightButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(PrintActivity.this, GetPersonsDataActivity.class);;
				startActivity(intent);
				finish();
			}
		});

		final Button leftButton = (Button) llHeaderSection.findViewById(R.id.headerButtonLeft);
		leftButton.setText("Exit");
		leftButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				final Intent mainActivity = new Intent(PrintActivity.this,
						GetPersonsDataActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				mainActivity.putExtra("EXIT", true);
				startActivity(mainActivity);
				finish();
			}
		});

		
		final TextView txtHeader = (TextView) this.findViewById(R.id.txtHeader);
		txtHeader.setText("SIM Details");
		
		final TextView txtID = (TextView) this.findViewById(R.id.txtID);
		txtID.setText(String.valueOf(registrationData.getPerson().getIdNumber()));
		
		final TextView txtFullName = (TextView) this.findViewById(R.id.txtFullName);
		txtFullName.setText(registrationData.getPerson().getFirstName() + " " + registrationData.getPerson().getFamilyName());
		
		final TextView txtSIM = (TextView) this.findViewById(R.id.txtSIM);
		txtSIM.setText(registrationData.getSimNumber());
		
		final TextView txtPUK = (TextView) this.findViewById(R.id.txtPUK);
		txtPUK.setText(registrationData.getPukNumber());

		final TextView txtStatus = (TextView) this.findViewById(R.id.txtStatus);
		txtStatus.setText("Activated");

		
		printButton = (Button) findViewById(R.id.printButton);
		newCustomerButton = (Button) findViewById(R.id.newCustomerButton);
		
		textToPrint.append("ID: ").append(txtID.getText()).append("\n")
		.append("Name: ").append(txtFullName.getText()).append("\n")
		.append("SIM Num:").append(txtSIM.getText()).append("\n")
		.append("PUK Num:").append(txtPUK.getText()).append("\n")
		.append("Total: 30 SAR").append("\n")
		.append("Activated!\n------------------\n\n\n");
		
		//infomation.setText(textToPrint.toString());
		status.setText("Print or register a new customer");
		
		printButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					print();
				} catch (Exception e) {
					status.setText("Unable to print");
					Log.e(TAG, "Unable to print", e);
				}
			}
		});
		
		newCustomerButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					closeBT();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				Intent intent = new Intent(PrintActivity.this, GetPersonsDataActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
			}
		});
	}
	
	private void print() 
			throws InterruptedException, ExecutionException {
		CallWSTask task = new CallWSTask();
		task.execute();
	}
	
	private class CallWSTask extends AsyncTask<Void, Void, Void> {

		ProgressDialog progDailog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(PrintActivity.this);
            progDailog.setMessage("Printing");
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			try {
				
				if(!findBT) {
					findBT();
					findBT = true;
				}
				if(!openBT) {
					openBT();
					openBT = true;
				}
				
				sendData();
			} catch (Exception e) {
				status.setText("Unable to print");
				Log.e(TAG, "Unable to print", e);
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if(progDailog.isShowing())
				progDailog.dismiss();
		}
	}
	
	/*
     * This will send data to be printed by the Bluetooth printer
     */
    void sendData() throws IOException {
        try {
            
            // the text typed by the user
            String msg = textToPrint.toString().toString();
            
            //This command informs the printer to print the stored logo
//            byte[] printLogo = 
//            	{0X1B, 0X40, 0X1B, 0X61, 0X01, 0X1D, 0X54, 0X1C, 0X70, 0X01, 0X30, 0X1B, 0X40};
//            mmOutputStream.write(printLogo);
            mmOutputStream.write(msg.getBytes());
            
            // tell the user data were sent
            status.setText("Data Sent");
            
        } catch (Exception e) {
        	status.setText("Unable to send data");
			Log.e(TAG, "Unable to send data", e);
        }
    }

    /*
     * This will find the Bluetooth printer device
     */
    void findBT() {
        try {
        	mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                status.setText("No bluetooth adapter available");
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
                    .getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    
                    // MP300 is the name of the bluetooth printer device
                    if (device.getName().equals("P25-003821-02")) {
                        mmDevice = device;
                        break;
                    }
                }
            }
            status.setText("Bluetooth printer found");
		} catch (Exception e) {
			status.setText("Unable to find printer");
			Log.e(TAG, "Unable to find printer", e);
		}
    }

    /*
     * Tries to open a connection to the Bluetooth printer device
     */
    void openBT() throws IOException {
        try {
            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            status.setText("Bluetooth connection established");
        } catch (Exception e) {
        	status.setText("Unable to connect to printer");
			Log.e(TAG, "Unable to connect to printer", e);
        }
    }
    
    /*
     * Close the connection to Bluetooth printer.
     */
    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            status.setText("Bluetooth Closed");
        } catch (Exception e) {
        	status.setText("Unable to close connection");
			Log.e(TAG, "Unable to close connection", e);
        }
    }
    
    @Override
    public void onBackPressed() {
    	//Do nothing
    }
}
