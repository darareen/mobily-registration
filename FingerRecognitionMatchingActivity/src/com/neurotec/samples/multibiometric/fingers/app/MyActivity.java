package com.neurotec.samples.multibiometric.fingers.app;

import com.neurotec.samples.multibiometric.R;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.credenceid.biometrics.Biometrics;
import com.credenceid.biometrics.BiometricsActivity;

import java.io.*;


/* Wael
Ignore this class. it was for testing. We will remove it very soon.
 */


public class MyActivity extends BiometricsActivity implements Biometrics.OnConvertToWsqListener
{

	ImageView image_view;
	TextView text_view;
    private File sdCard;
    private File dir;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
//		image_view = (ImageView) findViewById(R.id.imageView1);
//		text_view = (TextView) findViewById(R.id.textView1);

        sdCard = Environment.getExternalStorageDirectory();
        dir = new File (sdCard.getAbsolutePath() + "/CredenceWSQTest/");

        if(!dir.exists())
            dir.mkdirs();

	}

	public void onButtonClicked(View v) {
		image_view.setImageDrawable(null);
//		grabFingerprint();
        File file = new File(dir, "img"+".PNG");

        Biometrics.OnConvertToWsqListener listener = new Biometrics.OnConvertToWsqListener()
        {

            @Override
            public void onConvertToWsq(ResultCode resultCode, String s)
            {
                if (resultCode.equals(ResultCode.OK))
                    text_view.setText("OK  : "+s);
                else text_view.setText(""+resultCode.name());
            }
        };

        text_view.setText(file.getPath().toString());
        convertToWsq(file.getPath().toString(),0.0f,listener);
        Toast.makeText(MyActivity.this, "DONE!", Toast.LENGTH_SHORT).show();
	}


	@Override
	public void onFingerprintGrabbed(ResultCode result, Bitmap bitmap,
    byte[] iso, String filepath, String status) {
		if (status != null)
			text_view.setText(status);
		if (bitmap != null)
        {
			image_view.setImageBitmap(bitmap);
            writeToSDCard(bitmap);
        }
	}

    private void writeToSDCard(Bitmap bm)
    {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        bm.compress(Bitmap.CompressFormat.PNG, 100, bytes);

        File file = new File(dir, "img"+".PNG");
        try
        {
            FileOutputStream f = new FileOutputStream(file);
            f.write(bytes.toByteArray());
            f.flush();
            f.close();

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
