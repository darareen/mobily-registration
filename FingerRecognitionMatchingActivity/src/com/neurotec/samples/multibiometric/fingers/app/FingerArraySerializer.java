package com.neurotec.samples.multibiometric.fingers.app;
import com.neurotec.samples.multibiometric.R;
import nic.common.datamodel.Finger;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;
import java.util.Vector;


/* Wael
Each fingerprint image+type is stored in a Finger object. Upon sending the fingers to NIC webservice, it expects
the fingers to be passed in an array of Finger objects. This is the array serializer (vector of Fingers).
 */


public class FingerArraySerializer extends Vector<Finger> implements KvmSerializable {
      /**
	 *
	 */

	private static final long serialVersionUID = 1L;
	//n1 stores item namespaces:
    	String n1 = "http://datamodel.common.nic.gov.sa";

        @Override
        public Object getProperty(int arg0) {
                return this.get(arg0);
        }

        @Override
        public int getPropertyCount() {
               return this.elementCount;
        }

        @Override
        public void getPropertyInfo(int arg0, Hashtable arg1, PropertyInfo arg2) {

                arg2.name = "finger";
                arg2.type = Finger.class;
        }

        @Override
        public void setProperty(int arg0, Object arg1) {
                arg1 = (Finger) arg1;
        }

}