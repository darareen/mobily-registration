package com.neurotec.samples.multibiometric.fingers.app;
import com.neurotec.samples.multibiometric.R;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;


/* Wael
This is the main activity. It shows all the options available.
*/


public final class FingerRecognitionActivity extends Activity
{
	private static final String TAG = FingerRecognitionActivity.class.getSimpleName();
	private TextView mStatus;
	private Bitmap mDefaultBitmap;
    private Animation animFadeIn;
    private Animation animFadeOut;


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		try {
//			setContentView(R.layout.finger_main);
//
//			Button loadButton = (Button) findViewById(R.id.load);
//            loadButton.setText("البحث ببصمات الاصابع");
//
//            Button checkwlButton = (Button) findViewById(R.id.checkwl);
//            checkwlButton.setText("البحث في قوائم المراقبة");
//
//
//            Button getPersonButton = (Button) findViewById(R.id.getperson);
//            getPersonButton.setText("البحث برقم الهوية");
//
//
//            animFadeIn = AnimationUtils.loadAnimation(this, R.anim.anim_fade_in);
//            animFadeOut = AnimationUtils.loadAnimation(this, R.anim.anim_fade_out);
//
//			loadButton.setOnClickListener(new View.OnClickListener() {
//				@Override
//				public void onClick(View v) {
//                    Intent intent = new Intent(FingerRecognitionActivity.this, TestCredence.class);
//		            startActivity(intent);
//				}
//			});
//
//            checkwlButton.setOnClickListener(new View.OnClickListener() {
//				@Override
//				public void onClick(View v) {
//                    Intent intent = new Intent(FingerRecognitionActivity.this, CheckWLCredence.class);
//		            startActivity(intent);
//				}
//			});
//
//            getPersonButton.setOnClickListener(new View.OnClickListener() {
//				@Override
//				public void onClick(View v) {
//                    Intent intent = new Intent(FingerRecognitionActivity.this, GetPerson.class);
//		            startActivity(intent);
//				}
//			});
//
//		} catch (Exception e) {
//			Log.e(TAG, "Exception", e);
//			e.printStackTrace();
//		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
//		LicensingManager.getInstance().release(getComponents());
	}
}
