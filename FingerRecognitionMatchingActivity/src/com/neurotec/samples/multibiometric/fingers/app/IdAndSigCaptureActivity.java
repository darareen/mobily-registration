package com.neurotec.samples.multibiometric.fingers.app;
import java.io.ByteArrayOutputStream;
import java.io.File;

import nic.common.datamodel.RegistrationData;

import com.neurotec.samples.multibiometric.BiometricApplication;
import com.neurotec.samples.multibiometric.R;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class IdAndSigCaptureActivity extends Activity {

	private static final String TAG = IdAndSigCaptureActivity.class.getSimpleName();
	private static final int CAMERA_REQUEST = 1888; 
	private static final int SIGNATURE_CAPTURE = 101;
    private ImageView idPhotoView;
    private Button photoButton;
    
    private ImageView signatureView;
    private Button signatureButton;
    
    private boolean idCaptured = false;
    private boolean sigCaptured = false;
    
    private Button continueButton;
    
    private File photoFile;
    
    //RegstrationData reference
  	RegistrationData registrationData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.id_sim_capture);
        
        registrationData = ((BiometricApplication)getApplicationContext()).registrationData;
        
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        idPhotoView = (ImageView)this.findViewById(R.id.id_photo_view);
        photoButton = (Button) this.findViewById(R.id.photo_button);
        // configure header
		final LinearLayout llHeaderSection = (LinearLayout) findViewById(R.id.tvTitle_ref);
		final TextView txtTitle = (TextView) llHeaderSection
				.findViewById(R.id.tvTitle);
		txtTitle.setText("Capture Photo");
		
		final Button rightButton = (Button) llHeaderSection.findViewById(R.id.headerButtonRight);
		rightButton.setText("New");
		rightButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(IdAndSigCaptureActivity.this, GetPersonsDataActivity.class);;
				startActivity(intent);
				finish();
			}
		});

		final Button leftButton = (Button) llHeaderSection.findViewById(R.id.headerButtonLeft);
		leftButton.setText("Exit");
		leftButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Log.i(TAG,"inside Exit");
				final Intent mainActivity = new Intent(IdAndSigCaptureActivity.this,
						GetPersonsDataActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				mainActivity.putExtra("EXIT", true);
				startActivity(mainActivity);
				finish();
			}
		});
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            	photoFile = new File(BiometricApplication.CACHED_PHOTO_FILE);
            	Uri uri = Uri.fromFile(photoFile);
            	Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            	cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(cameraIntent, CAMERA_REQUEST); 
            }
        });
        
        signatureView = (ImageView) this.findViewById(R.id.signature_view);
        signatureButton = (Button) this.findViewById(R.id.signature_button);
        signatureButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getBaseContext(),CaptureSignature.class); 
                startActivityForResult(intent, SIGNATURE_CAPTURE);
			}
		});
        
        continueButton = (Button) this.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// Here we will handle where/how to pass the captured ID and Signature
				
				if(!idCaptured)
					Toast.makeText(getBaseContext(), "ID scan required", Toast.LENGTH_SHORT).show();
				else if(!sigCaptured)
					Toast.makeText(getBaseContext(), "Signature required", Toast.LENGTH_SHORT).show();
				else {
					Intent intent = new Intent(IdAndSigCaptureActivity.this, SimInfoActivity.class);
					startActivity(intent);
				}
			}
		});
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
    	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    	if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {  
    		Bitmap photo = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
            
            //Rotate photo 90 degrees CCW if taken in portrait mode
            if(photo.getWidth() < photo.getHeight()) {
            	Matrix matrix = new Matrix();
            	matrix.setRotate(270f, photo.getWidth()/2, photo.getHeight()/2);
            	photo = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), matrix, true);
            }
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 60, stream);
            registrationData.setIdImgArray(stream.toByteArray()); 
            idPhotoView.setImageBitmap(photo);
            idCaptured = true;
        } else if(requestCode == SIGNATURE_CAPTURE && resultCode == RESULT_OK){
        	byte[] sigArray = (byte[]) data.getExtras().get("sig_image");
        	registrationData.setSigImgArray(sigArray);
        	signatureView.setImageBitmap(BitmapFactory.decodeByteArray(sigArray, 0, sigArray.length));
        	sigCaptured = true;
        }
    }

}
