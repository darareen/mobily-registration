package com.neurotec.samples.multibiometric.fingers.app;



import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.credenceid.biometrics.Biometrics;
import com.credenceid.biometrics.BiometricsActivity;
import com.neurotec.samples.multibiometric.BiometricApplication;
import com.neurotec.samples.multibiometric.R;
import com.readystatesoftware.viewbadger.BadgeView;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import nic.common.datamodel.RegistrationData;

public class CaptureFingerPrint extends BiometricsActivity implements
		View.OnClickListener, Biometrics.OnConvertToWsqListener {
	private Button btn;
	private ImageView iv;

	private TextView tv;
	private int index = 0;
	private boolean doneAll = false;
	List<Integer> resources;

	private boolean clicked = false;

	private File sdCard;
	private File dir;

	private byte[] rIndex;
	private File file;
	private BadgeView badge8;
	private Button btnCaptureSig;
	private Long personID;
	private String gender;
	private String fullname;

	//RegstrationData reference
	RegistrationData registrationData;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.capturefingureprint);
		
		registrationData = ((BiometricApplication)getApplicationContext()).registrationData;
		
		tv = (TextView) findViewById(R.id.tvpersonsdata);

		personID = registrationData.getPerson().getIdNumber();
		if (registrationData.getPerson().getGender() == 1)
			gender = "Male";
		else
			gender = "Female";
		fullname = registrationData.getPerson().getFirstName();
		tv.setText("ID : "+personID+"\nGender : "+gender+"\nFull Name : "+fullname);
		//tv.setText("ID : 1234567\nGender : Male\nFull Name : Kaleem Ullah Brohi");
		iv = (ImageView) findViewById(R.id.imgfig);
		btnCaptureSig = (Button) findViewById(R.id.btncapture);

		resources = new ArrayList<Integer>();
		// resources.add(R.drawable.l1);
		// resources.add(R.drawable.l2);
		// resources.add(R.drawable.l3);
		// resources.add(R.drawable.l4);
		// resources.add(R.drawable.l5);
		// resources.add(R.drawable.r1);
		// resources.add(R.drawable.r2);
		// resources.add(R.drawable.r3);
		// resources.add(R.drawable.r4);
		// resources.add(R.drawable.r5);

		Thread mythread = new Thread(runnable);
		mythread.start();

		sdCard = Environment.getExternalStorageDirectory();
		dir = new File(sdCard.getAbsolutePath() + "/CredenceNIC/");
		if (!dir.exists()) {
			dir.mkdirs();
		}
		Log.d("CaptureFingerPrint", " calling grabFingerprint");

		btnCaptureSig.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String btnText = btnCaptureSig.getText().toString();
				
				if (btnText.equals("Capture")) {
					grabFingerprint();
					
					btnCaptureSig.setText(R.string.btnContinue);
				} else if (btnText.equals("Continue")) {
					
				}

			}
		});

	}

	@Override
	public void onFingerprintGrabbed(ResultCode result, Bitmap bm, byte[] iso,
			String filepath, String status) {
		// Toast.makeText(CaptureFingerPrint.this,
		// "يرجى الانتظار ... التحقق من بصمات الأصابع1",
		// Toast.LENGTH_SHORT).show();

		Log.d("CaptureFingerPrint", " onFingerprintGrabbed event");
		Log.d("CaptureFingerPrint", " result --> " + result);
		Log.d("CaptureFingerPrint", " Bitmap --> " + bm);
		Log.d("CaptureFingerPrint", " Status --> " + status);
		Log.d("CaptureFingerPrint", " filepath --> " + filepath);
		Log.d("CaptureFingerPrint",
				" result code comparing with Bio Result Code --> "
						+ result.equals(Biometrics.ResultCode.OK));
		if (result.equals(Biometrics.ResultCode.OK)) {
			writeToSDCard(bm);

			doneAll = true;
			putInByteArrays();

			if (isNetworkAvailable()) {
				// Intent gotoView = new Intent(CaptureFingerPrint.this,
				// FingerDetails.class);
				// Bundle b = new Bundle();
				// b.putByteArray("rIndex", rIndex);
				// Log.d("CaptureFingerPrint", " rIndex --> "+rIndex);
				// Toast.makeText(CaptureFingerPrint.this,
				// "يرجى الانتظار ... التحقق من بصمات الأصابع",
				// Toast.LENGTH_SHORT).show();
				// gotoView.putExtras(b);
				// startActivity(gotoView);

			}

		}

	}

	@Override
	public void onClick(View view) {

		if (!doneAll) {
			if (badge8.isShown()) {
				badge8.increment(1);
			} else {
				badge8.show();
			}

			clicked = true;

			iv.setImageResource(resources.get(index));
			prepareNextImage();

			grabFingerprint();
		}

	}

	private int prepareNextImage() {
		index = index + 1;
		if (index >= resources.size()) {
			index = 0;
		}
		return index;
	}

	// A PNG and WSQ versions are stored in separate folders.
	private void writeToSDCard(Bitmap bm) {
		Log.d("CaptureFingerPrint", " inside writeToSDCard --> ");
		Bitmap bitmapOrg = bm;

		int width = bitmapOrg.getWidth();
		int height = bitmapOrg.getHeight();
		Log.d("CaptureFingerPrint", "Bitmap\t width --> " + width
				+ "\theight--> " + height);
		Matrix matrix = new Matrix();

		matrix.postRotate(180);

		Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width,
				height, matrix, true);

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();

		resizedBitmap.compress(Bitmap.CompressFormat.PNG, 100, bytes);
		Log.d("CaptureFingerPrint", "index value " + index);
		File file = new File(dir, "img" + index + ".PNG");
		try {
			FileOutputStream f = new FileOutputStream(file);
			f.write(bytes.toByteArray());
			f.flush();
			f.close();
			Log.d("CaptureFingerPrint", "calling writeWSQ ");
			writeWSQ();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			Log.d("CaptureFingerPrint",
					"FileNotFoundException " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			Log.d("CaptureFingerPrint", "IOException " + e.getMessage());
		}
	}

	private void writeWSQ() {
		Log.d("CaptureFingerPrint", "inside writeWSQ ");
		Biometrics.OnConvertToWsqListener listener = new Biometrics.OnConvertToWsqListener() {

			@Override
			public void onConvertToWsq(ResultCode resultCode, String s) {
				File fileI = new File(s);

				File sdCard = Environment.getExternalStorageDirectory();
				File dir2 = new File(sdCard.getAbsolutePath()
						+ "/CredenceNIC/wsq/");
				if (!dir2.exists())
					dir2.mkdirs();
				File fileW = new File(dir2, "imgwsq" + index + ".wsq");

				InputStream inStream = null;
				OutputStream outStream = null;

				try {
					inStream = new FileInputStream(fileI);
					outStream = new FileOutputStream(fileW);

					byte[] buffer = new byte[1024];

					int length;
					while ((length = inStream.read(buffer)) > 0) {
						outStream.write(buffer, 0, length);
					}

					inStream.close();
					outStream.close();

					Toast.makeText(CaptureFingerPrint.this,
							" الانتهاء من : " + index + " wsq",
							Toast.LENGTH_SHORT).show();

					Thread.sleep(5000);

					

				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e1) {

				}

			}
		};

		convertToWsq(
				new File(dir, "img" + index + ".PNG").getPath().toString(),
				0.75f, listener);

	}

	// Iterate all the WSQ images and load them in the corresponding objects.
	private void putInByteArrays() {
		try {
			for (int i = 0; i < 10; i++) {
				File dir3 = new File(sdCard.getAbsolutePath()
						+ "/CredenceNIC/wsq/");

				file = new File(dir3, "imgwsq" + i + ".wsq");

				byte[] image = null;
				image = new byte[(int) (file.length())];
				FileInputStream inputStream = new FileInputStream(file);
				inputStream.read(image);

				if (i == 7)
					rIndex = image;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();

		return activeNetworkInfo != null;
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			ProgressDialog dialog = new ProgressDialog(this);
			dialog.setMessage("Loading.. Please Wait!");
			dialog.setIndeterminate(true);
			dialog.setCancelable(true);
			return dialog;
		}

		else {
			return new AlertDialog.Builder(this)
					.setTitle("Internet Connection")
					.setMessage("Please Check your Internet Connection")
					.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							finish();
						}
					}).create();
		}
	}

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			tv.setText("ضع اصبعك كما في الصورة");

			RotateAnimation rotate = (RotateAnimation) AnimationUtils
					.loadAnimation(tv.getContext(), R.anim.rotateanimation);
			tv.setAnimation(rotate);
		}
	};

	Runnable runnable = new Runnable() {
		public void run() {
			while (clicked == false) {
				synchronized (this) {
					try {

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
			handler.sendEmptyMessage(0);
		}
	};

}
