package com.neurotec.samples.multibiometric.fingers.app;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neurotec.biometrics.NFImpressionType;
import com.neurotec.biometrics.NFPosition;
import com.neurotec.biometrics.NFRecord;
import com.neurotec.images.NGrayscaleImage;
import com.neurotec.images.NImage;
import com.neurotec.images.NPixelFormat;
import com.neurotec.samples.licensing.LicensingManager;
import com.neurotec.samples.licensing.LicensingManager.LicensingStateCallback;
import com.neurotec.samples.licensing.LicensingState;
import com.neurotec.samples.multibiometric.BiometricApplication;
import com.neurotec.samples.multibiometric.BiometricBaseActivity;
import com.neurotec.samples.multibiometric.R;
import com.neurotec.samples.multibiometric.database.Record;
import com.neurotec.samples.multibiometric.fingers.FingerDataProcessor;
import com.neurotec.samples.multibiometric.fingers.FingerDataProcessor.Task;
import com.neurotec.samples.multibiometric.fingers.FingerDataProcessor.TaskResult;
import com.neurotec.samples.multibiometric.fingers.preference.ExtractorManager;
import com.neurotec.samples.multibiometric.fingers.preference.FingerPreferences;
import com.neurotec.samples.multibiometric.fingers.preference.MatchingManager;
import com.neurotec.samples.multibiometric.fingers.widget.FingerView;
import com.neurotec.samples.multibiometric.preference.EnrollmentManager;
import com.neurotec.samples.util.IOUtils;


import com.neurotec.biometrics.NFExtractor.ExtractResult;













import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import mobily.ws.TenLinesWS;
import nic.common.datamodel.RegistrationData;

public final class FingerRecognitionMatchingActivity extends BiometricBaseActivity implements FingerDataProcessor.Callback, LicensingStateCallback {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static final String TAG = FingerRecognitionMatchingActivity.class.getSimpleName();
	private static final int REQUEST_CODE_GET_FILE_OR_SCANNER = 1;

	private static final String BUNDLE_KEY_MESSAGE = "message";
	private static final String BUNDLE_KEY_SCORE = "score";
	private static final String BUNDLE_KEY_STATUS = "status";
	private static final String BUNDLE_KEY_RECORD = "record";
	private static final String BUNDLE_KEY_IMAGE = "image";

	// ===========================================================
	// Private fields
	// ===========================================================

	private NFRecord mRecord;
	private FingerView mFingerView;
	private TextView mStatus;
	private Bitmap mDefaultBitmap;
    private TextView tview;
    private int fingerCount = 0;
    private NFRecord firstFP;
    private NFRecord secondFP;
    private String gender;
    //RegstrationData reference
  	RegistrationData registrationData;

    // ===========================================================
	// Private methods
	// ===========================================================

	private boolean checkWorkerReady() {
		if (!FingerDataProcessor.getInstance().isReady()) {
			showToast(R.string.msg_another_task_in_progress);
			return false;
		}
		if (mRecord == null) {
			showToast(R.string.msg_no_record_loaded);
			return false;
		}
		return true;
	}

//	private boolean checkDbNotEmpty() {
//		if (DBAdapter.getInstance().getFingers().getRecords().size() == 0) {
//			showInfo(getString(R.string.msg_no_records_in_database));
//			return false;
//		} else {
//			return true;
//		}
//	}

//	private boolean checkTemplateId(String name) {
//		if (TextUtils.isEmpty(name)) {
//			return false;
//		}
//		return !DBAdapter.getInstance().getFingers().hasName(name);
//	}

	private ByteBuffer loadUri(Uri uri) {
		ByteBuffer loadedTemplate = null;
		try {
			loadedTemplate = IOUtils.toByteBuffer(this, uri);
			return loadedTemplate;
		} catch (IOException e) {
			Log.e(TAG, "IOException", e);
			String message = getString(R.string.msg_error_opening_file) + " " + e.getMessage();
			showError(message);
			setView(message, null, mDefaultBitmap);
			return null;
		}
	}

	private void clearView() {
		setView("", null, null);
	}

	private void setView(String status, NFRecord record, Bitmap bitmap) {
		mStatus.setText(status);
		mRecord = record;
		if (mFingerView.getRecord() != record) {
			mFingerView.setRecord(record);
		}
		setPersonalPhoto(bitmap);
	}
	
	private void setPersonalPhoto(Bitmap bitmap) {
		try {
				byte[] facialPhoto = registrationData.getPerson().getFacialPhoto().getFacialPhoto();
				if(facialPhoto.length > 10) { //i.e. has a length
					mFingerView.setBitmap(BitmapFactory.decodeByteArray(facialPhoto, 0, facialPhoto.length));
				} else {
					if (mFingerView.getBitmap() != bitmap) {
						mFingerView.setBitmap(bitmap);
					}
				}
		} catch(Exception e){
			if (mFingerView.getBitmap() != bitmap) {
				mFingerView.setBitmap(bitmap);
			}
		}
	}

	private String styleStatusMessage(String message) {
		return (message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase()).replace("_", " ");
	}

	// ===========================================================
	// Protected methods
	// ===========================================================
	// ===========================================================
	// 		Activity events
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		registrationData = ((BiometricApplication)getApplicationContext()).registrationData;
		
		try {
			setContentView(R.layout.finger_main);
			if (savedInstanceState == null) {
				LicensingManager.getInstance().obtain(this, this, getComponents());
			}

            mFingerView = (FingerView) findViewById(R.id.finger_view);
            mDefaultBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.hand);

            if (registrationData.getPerson().getGender() == 1)
    			gender = "Male";
    		else
    			gender = "Female";
        
    		final LinearLayout llHeaderSection = (LinearLayout) findViewById(R.id.tvTitle_ref);
    		final TextView txtTitle = (TextView) llHeaderSection.findViewById(R.id.tvTitle);
    		txtTitle.setText("Person Fingerprint");
    		
    		final Button rightButton = (Button) llHeaderSection.findViewById(R.id.headerButtonRight);
    		rightButton.setText("New");
    		rightButton.setOnClickListener(new OnClickListener() {
    			public void onClick(View v) {
    				Intent intent = new Intent(FingerRecognitionMatchingActivity.this, GetPersonsDataActivity.class);;
    				startActivity(intent);
    				finish();
    			}
    		});

    		final Button leftButton = (Button) llHeaderSection.findViewById(R.id.headerButtonLeft);
    		leftButton.setText("Exit");
    		leftButton.setOnClickListener(new OnClickListener() {
    			public void onClick(View v) {
    				Log.i(TAG,"inside Exit");
    				final Intent mainActivity = new Intent(FingerRecognitionMatchingActivity.this,
    						GetPersonsDataActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    				mainActivity.putExtra("EXIT", true);
    				startActivity(mainActivity);
    				finish();
    			}
    		});
    		final TextView txtHeader = (TextView) this.findViewById(R.id.txtHeader);
    		txtHeader.setText("Person Details");
    		
    		final TextView txtID = (TextView) this.findViewById(R.id.txtID);
    		txtID.setText(String.valueOf(registrationData.getPerson().getIdNumber()));
    		
    		final TextView txtFullName = (TextView) this.findViewById(R.id.txtFullName);
    		txtFullName.setText(registrationData.getPerson().getFirstName() + " " + registrationData.getPerson().getFamilyName());
    		
    		final TextView txtGender = (TextView) this.findViewById(R.id.txtGender);
    		txtGender.setText(gender);
    		
    		final TextView txtIDExpiryDate = (TextView) this.findViewById(R.id.txtExpiryDate);
    		txtIDExpiryDate.setText(registrationData.getPerson().getIdExpDate());

    		final TextView txtEnrollmentStatus = (TextView) this.findViewById(R.id.txtEnrollmentStatus);
    		txtEnrollmentStatus.setText("Enrolled");
    		
            mStatus = (TextView) findViewById(R.id.finger_status);

			Button loadButton = (Button) findViewById(R.id.load);
            loadButton.setText("Capture Fingerprint");

            Button checkwlButton = (Button) findViewById(R.id.checkwl);
            checkwlButton.setText("Compare");


			loadButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
                    Intent intent = new Intent(FingerRecognitionMatchingActivity.this, TestCredence.class);
		            startActivity(intent);
				}
			});

            checkwlButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
//					if (!(checkWorkerReady())) {
//						return;
//					}
					if (!LicensingManager.isFingerMatchingActivated()) {
						showError(getString(R.string.msg_operation_is_not_activated));
						return;
					}

                    FingerDataProcessor processor = FingerDataProcessor.getInstance();
                    processor.setCallback(FingerRecognitionMatchingActivity.this);
                    if (!processor.isReady()) {
                        return;
                    }



                    // First FP
					NImage image = null;
                    ExtractResult aa = null;
					try {
                        image = NImage.fromBitmap(registrationData.getCapturedFingerPrint());
						//image = ImageUtils.getImageFromUri(this, data.getData());
					} catch (Exception e) {
						Log.e(TAG, "IOException", e);
						String message = getString(R.string.msg_error_opening_file) + " " + e.getMessage();
						showError(message);
						setView(message, null, mDefaultBitmap);
					}
					if (image != null) {
						setView(null, null, image.toBitmap());
						//processor.extract(image);

                        NGrayscaleImage grayscaleImage = null;
                        try {
                            if (image.getPixelFormat() != NPixelFormat.GRAYSCALE_8U) {
                                grayscaleImage = image.toGrayscale();
                            } else {
                                grayscaleImage = (NGrayscaleImage) image;
                            }
                            if (grayscaleImage.isResolutionIsAspectRatio() || (grayscaleImage.getHorzResolution() < 250) || (grayscaleImage.getVertResolution() < 250)) {
                                Log.i(TAG, "Resolution is invalid, fixing");
                                grayscaleImage.setResolutionIsAspectRatio(false);
                                grayscaleImage.setHorzResolution(500);
                                grayscaleImage.setVertResolution(500);
                            }

                            aa = ExtractorManager.getInstance().getExtractor().extract(grayscaleImage, NFPosition.UNKNOWN, NFImpressionType.LIVE_SCAN_PLAIN);
                            }
                            finally {
                                if (grayscaleImage != null) {
                                    grayscaleImage.dispose();
                                }
                            }
                         firstFP = aa.getRecord();
					}
					
                    secondFP = new NFRecord(ByteBuffer.wrap(registrationData.getPerson().getMinutiaeData().getFirstMinutiae().getMinutiaeValue()));
                    int ssss = MatchingManager.getInstance().getMatcher().verify(firstFP.save(), secondFP.save());
                    
                    if(ssss <= 0) {
                    	secondFP = new NFRecord(ByteBuffer.wrap(registrationData.getPerson().getMinutiaeData().getSecondMinutiae().getMinutiaeValue()));
                    	ssss = MatchingManager.getInstance().getMatcher().verify(firstFP.save(), secondFP.save());
                    }
                    
                    mStatus.setText("Matching: "+ ssss +"  (0=mismatch, >0 if match");
                    Log.i(TAG, "HERE::::: YAAAAAAY  "+ssss);

//                    FingerDataProcessor.getInstance().verify(firstFP, secondFP);

                    if(ssss > 0) {
//                    	Toast.makeText(getBaseContext(), "Match found", Toast.LENGTH_SHORT).show();
                    	checkTenLines();
                    } else {
                    	AlertDialog.Builder builder = new AlertDialog.Builder(FingerRecognitionMatchingActivity.this);
						builder.setMessage("No fingerprint match found")
						       .setCancelable(false)
						       .setPositiveButton("OK", null);
						AlertDialog alert = builder.create();
						alert.show();
                    }
				}
			});


			FingerDataProcessor.getInstance().setCallback(this);

			if (savedInstanceState != null) {
				Bitmap bitmap = savedInstanceState.getParcelable(BUNDLE_KEY_IMAGE);
				if (bitmap != null) {
					setPersonalPhoto(bitmap);
				}
				byte[] recordData = savedInstanceState.getByteArray(BUNDLE_KEY_RECORD);
				if (recordData != null) {
					mRecord = new NFRecord(ByteBuffer.wrap(recordData));
					if (mRecord != null) {
						mFingerView.setRecord(mRecord);
					}
				}
			} else {
				setPersonalPhoto(mDefaultBitmap);
			}

		} catch (Exception e) {
			Log.e(TAG, "Exception", e);
			showError(e.getMessage());
		}
	}
	
	private void checkTenLines() {
		try {
			CallWSTask task = new CallWSTask();
			task.execute();
		} catch (Exception e) {
			Log.e(TAG, "Exception while calling ten lines WS", e);
			AlertDialog.Builder builder = new AlertDialog.Builder(FingerRecognitionMatchingActivity.this);
			builder.setTitle("Validation Error")
					.setMessage("Unable to verify eligibility. Please try again.")
			       .setCancelable(false)
			       .setPositiveButton("OK", null);
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	private class CallWSTask extends AsyncTask<Void, Void, Boolean> {

		ProgressDialog progDailog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(FingerRecognitionMatchingActivity.this);
            progDailog.setMessage("Verifying eligibility");
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			
			return new TenLinesWS().isTenLines(registrationData.getPerson().getIdNumber());
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if(progDailog.isShowing())
				progDailog.dismiss();
			
			boolean lessThanTen = result;
        	
        	if(lessThanTen) {
        		Intent intent = new Intent(FingerRecognitionMatchingActivity.this, IdAndSigCaptureActivity.class);
				startActivity(intent);
        	} else {
        		AlertDialog.Builder builder = new AlertDialog.Builder(FingerRecognitionMatchingActivity.this);
				builder.setMessage("Client has 10 lines or more")
				       .setCancelable(false)
				       .setPositiveButton("OK", null);
				AlertDialog alert = builder.create();
				alert.show();
        	}
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
//		LicensingManager.getInstance().release(getComponents());
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		switch (id) {
			case R.id.dialog_finger_candidate_list: {
				Builder builder = new Builder(this);
				builder.setTitle(R.string.msg_choose_candidate_for_verification).setAdapter(
					new ArrayAdapter<Record>(this, android.R.layout.simple_list_item_1, new Record[0]),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							AlertDialog alertDlg = (AlertDialog) dialog;
							Record rec = (Record) alertDlg.getListView().getItemAtPosition(which);
							FingerDataProcessor.getInstance().verify(mRecord, rec.getName());
						}

					});
				return builder.create();
			}
			case R.id.dialog_verification: {
				Builder builder = new Builder(this);
				builder.setTitle(getString(R.string.dialog_title_verification))
					.setMessage(getString(R.string.msg_verification_fail))
					.setPositiveButton(getString(R.string.msg_again), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
							showDialog(R.id.dialog_finger_candidate_list);
						}
					}).setNegativeButton(getString(R.string.msg_close), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
				return builder.create();
			}
			default: {
				return super.onCreateDialog(id, args);
			}
		}
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle bundle) {
		switch (id) {
		case R.id.dialog_enroll_id: {
			EditText enrollIdEditText = (EditText) dialog.findViewById(R.id.enroll_id);
			enrollIdEditText.setText("");
			break;
		}
		case R.id.dialog_finger_candidate_list: {
//			RecordCollection recs = DBAdapter.getInstance().getFingers().getRecords();
//			final Record[] items = new Record[recs.size()];
            final Record[] items = new Record[0]; //WAEL ADDED IT!!!!!!
//			for (int i = 0; i < recs.size(); i++) {
//				items[i] = recs.get(i);
//			}
			AlertDialog alertDialog = (AlertDialog) dialog;
			alertDialog.getListView().setAdapter(new ArrayAdapter<Record>(this, android.R.layout.select_dialog_item, items));
			break;
		}
		case R.id.dialog_verification: {
			String verificationMsg = bundle.getString(BUNDLE_KEY_MESSAGE);
			int score = bundle.getInt(BUNDLE_KEY_SCORE);
			if (score > 0) {
				((AlertDialog) dialog).setMessage(verificationMsg + score + ")");
			} else {
				((AlertDialog) dialog).setMessage(verificationMsg);
			}
			break;
		}
		default:
			super.onPrepareDialog(id, dialog, bundle);
		}
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	@Override
	public void onLicensingStateChanged(LicensingState state) {
		switch (state) {
		case OBTAINING:
			showProgress(R.string.msg_obtaining_licenses);
			break;
		case OBTAINED:
			hideProgress();
			showToast(R.string.msg_licenses_obtained);
			break;
		case NOT_OBTAINED:
			hideProgress();
			showToast(R.string.msg_licenses_not_obtained);
			break;
		}

		try {
			ExtractorManager.getInstance().update(this);
			EnrollmentManager.getInstance().update(this);
			MatchingManager.getInstance().update(this);
		} catch (Exception e) {
			Log.e(TAG, "Exception", e);
			showError(e.getMessage());
		}
	}

	@Override
	public String getTableName() {
//		return DBAdapter.FINGERS_TABLE;
        return "";//WAEL ADDED IT!!!
	}

	@Override
	public Class<?> getPreferenceActivity() {
		return FingerPreferences.class;
	}

	@Override
	public List<String> getComponents() {
		return Arrays.asList(
            LicensingManager.LICENSE_FINGER_DETECTION,
			LicensingManager.LICENSE_FINGER_EXTRACTION,
			LicensingManager.LICENSE_FINGER_MATCHING,
			LicensingManager.LICENSE_FINGER_MATCHING_FAST,
			LicensingManager.LICENSE_FINGER_DEVICES_SCANNERS,
			LicensingManager.LICENSE_FINGER_WSQ,
			LicensingManager.LICENSE_FINGER_STANDARDS_FINGER_TEMPLATES,
			LicensingManager.LICENSE_FINGER_STANDARDS_FINGERS
        );

	}

	// ===========================================================
	// 		Callback interface implementation
	// ===========================================================

	@Override
	public void onBackgroundTaskStarted(Task task) {
		switch (task) {
			case EXTRACT: {
				showProgress(R.string.msg_extracting);
			} break;
			case ENROLL: {
				showProgress(R.string.msg_enrolling);
			} break;
			case VERIFY: {
				showProgress(R.string.msg_verifying);
			} break;
            case VERIFYWAEL: {
				showProgress(R.string.msg_verifying);
			} break;
			case IDENTIFY: {
				showProgress(R.string.msg_identifying);
			} break;
			case LOAD_TEMPLATE: {
				showProgress(R.string.msg_finger_ntemplate_loading_progress);
			} break;
			case LOAD_FIRECORD: {
				showProgress(R.string.msg_finger_firecord_loading_progress);
			} break;
			case LOAD_FMRECORD: {
				showProgress(R.string.msg_finger_fmrecord_loading_progress);
			} break;
			default: {
				throw new AssertionError("Unknown task: " + task);
			}
		}
	}

	@Override
	public void onBackgroundTaskFinished(TaskResult result) {
		hideProgress();
		switch (result.getTask()) {
			case EXTRACT:
			case LOAD_FIRECORD: {
				setView(styleStatusMessage(result.getExtractResult().getStatus().toString()), result.getExtractResult().getRecord(), result.getBitmap());
			} break;
			case ENROLL: {
				if (result.isDuplicatePerson()) {
					mStatus.setText(R.string.msg_person_exists);
				} else {
					mStatus.setText(R.string.msg_enrollment_succeeded);
				}
			} break;
			case VERIFY: {
				Bundle bundle = new Bundle();
				int score = result.getVerificationResult();
				bundle.putInt(BUNDLE_KEY_SCORE, score);
				if (score > 0) {
					bundle.putString(BUNDLE_KEY_MESSAGE, getString(R.string.msg_verification_success));
				} else {
					bundle.putString(BUNDLE_KEY_MESSAGE, getString(R.string.msg_verification_fail));
				}
				showDialog(R.id.dialog_verification, bundle);
			} break;
            case VERIFYWAEL: {
				Bundle bundle = new Bundle();
				int score = result.getVerificationResult();
				bundle.putInt(BUNDLE_KEY_SCORE, score);
				if (score > 0) {
					bundle.putString(BUNDLE_KEY_MESSAGE, getString(R.string.msg_verification_success));
				} else {
					bundle.putString(BUNDLE_KEY_MESSAGE, getString(R.string.msg_verification_fail));
				}
				showDialog(R.id.dialog_verification, bundle);
			} break;
			case IDENTIFY: {
				if (result.getIdentificationResults().isEmpty()) {
					showInfo(getString(R.string.msg_no_matches));
				} else {
					StringBuilder b = new StringBuilder();
					for (Pair<String, Integer> r : result.getIdentificationResults()) {
						b.append(getString(R.string.msg_name))
							.append(": ")
							.append(r.first)
							.append("; ")
							.append(getString(R.string.msg_score))
							.append(": ")
							.append(r.second)
							.append('\n');
					}
					showInfo(b.toString());
				}
			} break;
			case LOAD_TEMPLATE:
			case LOAD_FMRECORD: {
				setView(getString(R.string.msg_finger_ntemplate_loading_succeeded), result.getRecord(), null);
			} break;
			default: {
				throw new AssertionError("Unknown task: " + result.getTask());
			}
		}
	}

	@Override
	public void onBackgroundTaskException(Task task, Exception e) {
		hideProgress();
		String message;
		switch (task) {
			case EXTRACT: {
				message = getString(R.string.msg_extraction_error);
			} break;
			case ENROLL: {
				message = getString(R.string.msg_enroll_error);
			} break;
			case VERIFY: {
				message = getString(R.string.msg_verification_error);
			} break;
            case VERIFYWAEL: {
				message = getString(R.string.msg_verification_error);
			} break;
			case IDENTIFY: {
				message = getString(R.string.msg_identification_error);
			} break;
			case LOAD_TEMPLATE: {
				setPersonalPhoto(mDefaultBitmap);
				message = getString(R.string.msg_finger_ntemplate_loading_failed);
			} break;
			case LOAD_FIRECORD: {
				setPersonalPhoto(mDefaultBitmap);
				message = getString(R.string.msg_finger_firecord_loading_failed);
			} break;
			case LOAD_FMRECORD: {
				setPersonalPhoto(mDefaultBitmap);
				message = getString(R.string.msg_finger_fmrecord_loading_failed);
			} break;
			default: {
				throw new AssertionError("Unknown task: " + task);
			}
		}
		showError(message + " " + e.getMessage());
		mStatus.setText(message + " " + e.getMessage());
	}

	// ===========================================================
	// Key events
	// ===========================================================

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			if ((keyCode == KeyEvent.KEYCODE_BACK) || (keyCode == KeyEvent.KEYCODE_HOME)) {
				FingerDataProcessor.getInstance().cancel();
			}
		}
		return super.onKeyDown(keyCode, event);
	}
}
