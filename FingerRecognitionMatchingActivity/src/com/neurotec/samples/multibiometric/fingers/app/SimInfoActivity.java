package com.neurotec.samples.multibiometric.fingers.app;
import java.util.concurrent.ExecutionException;

import com.neurotec.samples.multibiometric.BiometricApplication;
import com.neurotec.samples.multibiometric.R;

import mobily.ws.RegisterSIM;
import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import nic.common.datamodel.RegistrationData;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SimInfoActivity extends Activity {

	static {
	    System.loadLibrary("iconv");
	}
	
	private static final int CAMERA_REQUEST = 1888;

	private EditText simNumText;
	private EditText pukNumText;
	
	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;
	
	private ImageScanner scanner;
	
	private boolean simNumScanned = false;
	private boolean pukNumScanned = false;
	
	private boolean previewing = true;
	
	private Button continueButton;
	
	private final int SUCCESS = 1;
	private final int FAILURE = 0;
	String gender;
	//RegstrationData reference
  	RegistrationData registrationData;
  	
  	private static final String TAG = SimInfoActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sim_info);
		
		registrationData = ((BiometricApplication)getApplicationContext()).registrationData;
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		if (registrationData.getPerson().getGender() == 1)
			gender = "Male";
		else
			gender = "Female";
    
		final LinearLayout llHeaderSection = (LinearLayout) findViewById(R.id.tvTitle_ref);
		final TextView txtTitle = (TextView) llHeaderSection.findViewById(R.id.tvTitle);
		txtTitle.setText("Person Fingerprint");
		
		final Button rightButton = (Button) llHeaderSection.findViewById(R.id.headerButtonRight);
		rightButton.setText("New");
		rightButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(SimInfoActivity.this, GetPersonsDataActivity.class);;
				startActivity(intent);
				finish();
			}
		});

		final Button leftButton = (Button) llHeaderSection.findViewById(R.id.headerButtonLeft);
		leftButton.setText("Exit");
		leftButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				final Intent mainActivity = new Intent(SimInfoActivity.this,
						GetPersonsDataActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				mainActivity.putExtra("EXIT", true);
				startActivity(mainActivity);
				finish();
			}
		});

		
		simNumText = (EditText) findViewById(R.id.simText);
		pukNumText = (EditText) findViewById(R.id.pukText);
		
		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();
		
		scanner = new ImageScanner();
		
		
		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);
		Log.i("holder is :", previewCb.toString());

		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout) findViewById(R.id.barcodePreview);
		preview.addView(mPreview);
		
		continueButton = (Button) this.findViewById(R.id.continueButton);
        continueButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if(simNumText.getText().toString().trim().equals("")) {
					AlertDialog.Builder builder = new AlertDialog.Builder(SimInfoActivity.this);
					builder.setMessage("SIM number required")
					       .setCancelable(false)
					       .setPositiveButton("OK", null);
					AlertDialog alert = builder.create();
					alert.show();
				} else if(pukNumText.getText().toString().trim().equals("")) {
					AlertDialog.Builder builder = new AlertDialog.Builder(SimInfoActivity.this);
					builder.setMessage("PUK number required")
					       .setCancelable(false)
					       .setPositiveButton("OK", null);
					AlertDialog alert = builder.create();
					alert.show();
				}else {					
					registrationData.setSimNumber(simNumText.getText().toString());
					registrationData.setPukNumber(pukNumText.getText().toString());
					
					registerSIM();
				}
			}
		});
		
	}
	
	private void registerSIM() {
		try {
			CallWSTask task = new CallWSTask();
			task.execute();
		} catch (Exception e) {
			Log.e(TAG, "Exception during SIM activation", e);
			AlertDialog.Builder builder = new AlertDialog.Builder(SimInfoActivity.this);
			builder.setTitle("Validation Error")
					.setMessage("Unable to verify eligibility. Please try again.")
			       .setCancelable(false)
			       .setPositiveButton("OK", null);
			AlertDialog alert = builder.create();
			alert.show();
		}
	}
	
	private class CallWSTask extends AsyncTask<Void, Void, Integer> {

		ProgressDialog progDailog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progDailog = new ProgressDialog(SimInfoActivity.this);
            progDailog.setMessage("Activating SIM");
            progDailog.setIndeterminate(true);
            progDailog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDailog.setCancelable(false);
            progDailog.show();
		}
		
		@Override
		protected Integer doInBackground(Void... params) {
			
			return new RegisterSIM().registerSIM(
					String.valueOf(
							registrationData.getPerson().getIdNumber()), 
							registrationData.getSimNumber(), 
							registrationData.getPukNumber(), 
							registrationData.getIdImgArray(), 
							registrationData.getSigImgArray());
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);
			if(progDailog.isShowing())
				progDailog.dismiss();
			int resultCode = result;
			
			Log.d("SIM Activation", "SIM activation result code: " + resultCode);
			
			if(resultCode == SUCCESS) {
				previewing = false;
				mCamera.setPreviewCallback(null);
				mCamera.stopPreview();
				
				//Delete the cached ID photo
				BiometricApplication.clearCacheData();
				
				Toast.makeText(getBaseContext(), "SIM activated", Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent(SimInfoActivity.this, PrintActivity.class);
				startActivity(intent);
			} else if(resultCode == FAILURE){
				AlertDialog.Builder builder = new AlertDialog.Builder(SimInfoActivity.this);
				builder.setMessage("SIM already registered")
				       .setCancelable(false)
				       .setPositiveButton("OK", null);
				AlertDialog alert = builder.create();
				alert.show();
			} else {
				AlertDialog.Builder builder = new AlertDialog.Builder(SimInfoActivity.this);
				builder.setMessage("SIM activation failure")
				       .setCancelable(false)
				       .setPositiveButton("OK", null);
				AlertDialog alert = builder.create();
				alert.show();
			}
		}
	}
	
	public void onPause() {
		super.onPause();
		releaseCamera();
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
		}
		return c;
	}

	private void releaseCamera() {
		if (mCamera != null) {
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}
	
	PreviewCallback previewCb = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {
//				previewing = false;
//				mCamera.setPreviewCallback(null);
//				mCamera.stopPreview();

				
				
				SymbolSet syms = scanner.getResults();
				for (Symbol sym : syms) {
					if(simNumText.hasFocus()) {
						if(!simNumText.getText().toString().equals(sym.getData())) {
							barcodeCaptureNotification();
						}
						simNumText.setText(sym.getData());
						simNumScanned = true;
					} else if(pukNumText.hasFocus()) {
						if(!pukNumText.getText().toString().equals(sym.getData())) {
							barcodeCaptureNotification();
						}
						pukNumText.setText(sym.getData());
						pukNumScanned = true;
					}
				}
			}
		}
	};
	
	private void barcodeCaptureNotification() {
		Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		vibrator.vibrate(100);
		
		final ToneGenerator tg = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);
        tg.startTone(ToneGenerator.TONE_PROP_BEEP);
	}
	
	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};
	
	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

}
