package com.neurotec.samples.multibiometric.fingers;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import com.neurotec.biometrics.NFExtractor.ExtractResult;
import com.neurotec.biometrics.NFImpressionType;
import com.neurotec.biometrics.NFPosition;
import com.neurotec.biometrics.NFRecord;
import com.neurotec.biometrics.NTemplate;
import com.neurotec.biometrics.standards.BDIFStandard;
import com.neurotec.biometrics.standards.FIRecord;
import com.neurotec.biometrics.standards.FMRecord;
import com.neurotec.images.NGrayscaleImage;
import com.neurotec.images.NImage;
import com.neurotec.images.NPixelFormat;
import com.neurotec.samples.multibiometric.devices.FingerScanner.CaptureResult;
import com.neurotec.samples.multibiometric.fingers.preference.ExtractorManager;
import com.neurotec.samples.multibiometric.fingers.preference.MatchingManager;
import com.neurotec.samples.util.AsyncTaskResult;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public final class FingerDataProcessor {

    // ===========================================================
	// Callback interface
	// ===========================================================

	public interface Callback {
		void onBackgroundTaskStarted(Task task);
		void onBackgroundTaskFinished(TaskResult result);
		void onBackgroundTaskException(Task task, Exception e);
	}

	// ===========================================================
	// Task type
	// ===========================================================

	public enum Task {
		EXTRACT,
		ENROLL,
		VERIFY,
        VERIFYWAEL,
        IDENTIFY,
		LOAD_TEMPLATE,
		LOAD_FIRECORD,
		LOAD_FMRECORD;
	}

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static final String TAG = FingerDataProcessor.class.getSimpleName();
	private static FingerDataProcessor sInstance = null;

	// ===========================================================
	// Public static methods
	// ===========================================================

	public static synchronized FingerDataProcessor getInstance() {
		if (sInstance == null) {
			sInstance = new FingerDataProcessor();
		}
		return sInstance;
	}

	// ===========================================================
	// Private fields
	// ===========================================================

	private Callback mCallback;
	private BackgroundTask mBackgroundTask;

	// ===========================================================
	// Private constructor
	// ===========================================================

	private FingerDataProcessor() {
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	public void extract(Bitmap image) {
		if (image != null) {
			extract(NImage.fromBitmap(image));
		} else {
			Log.i(TAG, "Image is null");
		}
	}

	public void extract(NImage image) {
		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.extractFinger(image);
	}

	public void enroll(NFRecord record, String name, boolean checkForDuplicates) {
		enroll(record.save(), name, checkForDuplicates);
	}

	public void enroll(ByteBuffer record, String name, boolean checkForDuplicates) {
		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.enrollFinger(record, name, checkForDuplicates);
	}

	public void verify(NFRecord record, String candidateName) {
		verify(record.save(), candidateName);
	}

	public void verify(ByteBuffer record, String candidateName) {
		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.verifyFinger(record, candidateName);
	}

    public void verify(NFRecord firstFP, NFRecord secondFP) {
        verify(firstFP.save(), secondFP.save());
    }

    public void verify(ByteBuffer record1, ByteBuffer record2) {
		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.verifyFinger(record1, record2);
	}

	public void identify(NFRecord record) {
		identify(record.save());
	}

	public void identify(ByteBuffer record) {
		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.identifyFinger(record);
	}

	public void loadNTemplate(ByteBuffer data) {
		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.loadFingerTemplate(data);
	}

	public void loadFIRecord(ByteBuffer data, BDIFStandard standard) {
		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.loadStandardFIRecord(data, standard);
	}

	public void loadFMRecord(ByteBuffer data, BDIFStandard standard) {
		mBackgroundTask = new BackgroundTask();
		mBackgroundTask.loadStandardFMRecord(data, standard);
	}

	public void setCallback(Callback callback) {
		mCallback = callback;
	}

	public boolean isReady() {
		return ((mBackgroundTask == null) || (mBackgroundTask.getStatus() == AsyncTask.Status.FINISHED));
	}

	public void cancel() {
		if (mBackgroundTask != null) {
			mBackgroundTask.cancel(true);
		}
	}

	// ===========================================================
	// Background task (Inner class)
	// ===========================================================

	final class BackgroundTask extends AsyncTask<Void, CaptureResult, AsyncTaskResult<TaskResult>> {

		// ===========================================================
		// Private fields
		// ===========================================================

		private Task mTask;
		private NImage mImage;
		private String mName;
		private String mCandidateName;
		private ByteBuffer mData;
        private ByteBuffer mData2;
		private BDIFStandard mStandard;
		private boolean mCheckForDuplicates;

		// ===========================================================
		// Private methods
		// ===========================================================

		private void extractFinger(NImage image) {
			mTask = Task.EXTRACT;
			mImage = image;
			execute();
		}

		private void enrollFinger(ByteBuffer record, String name, boolean checkForDuplicates) {
			mTask = Task.ENROLL;
			mData = record;
			mName = name;
			mCheckForDuplicates = checkForDuplicates;
			execute();
		}

		private void verifyFinger(ByteBuffer record, String candidateName) {
			mTask = Task.VERIFY;
			mData = record;
			mCandidateName = candidateName;
			execute();
		}

        private void verifyFinger(ByteBuffer record1, ByteBuffer record2) {
			mTask = Task.VERIFYWAEL;
			mData = record1;
            mData2 = record2;
			execute();
		}


		private void identifyFinger(ByteBuffer record) {
			mTask = Task.IDENTIFY;
			mData = record;
			execute();
		}

		private void loadFingerTemplate(ByteBuffer data) {
			loadItemFromUri(data, Task.LOAD_TEMPLATE);
		}

		private void loadStandardFIRecord(ByteBuffer data, BDIFStandard standard) {
			mStandard = standard;
			loadItemFromUri(data, Task.LOAD_FIRECORD);
		}

		private void loadStandardFMRecord(ByteBuffer data, BDIFStandard standard) {
			mStandard = standard;
			loadItemFromUri(data, Task.LOAD_FMRECORD);
		}

		private void loadItemFromUri(ByteBuffer data, Task task) {
			mTask = task;
			mData = data;
			execute();
		}

		private ExtractResult extract(NImage image) {
			if (image == null) {
                Log.i(TAG, "HERE::::: H1");
				throw new NullPointerException("mImage");
			}
			NGrayscaleImage grayscaleImage = null;
			try {
				if (image.getPixelFormat() != NPixelFormat.GRAYSCALE_8U) {
					Log.i(TAG, "Image format is not grayscale, converting.");
                    Log.i(TAG, "HERE::::: H2");
					grayscaleImage = image.toGrayscale();
                    Log.i(TAG, "HERE::::: H2-2 "+grayscaleImage.toString());
				} else {
					grayscaleImage = (NGrayscaleImage) image;
                    Log.i(TAG, "HERE::::: H3");
				}
				if (grayscaleImage.isResolutionIsAspectRatio() || (grayscaleImage.getHorzResolution() < 250) || (grayscaleImage.getVertResolution() < 250)) {
					Log.i(TAG, "Resolution is invalid, fixing");
                    Log.i(TAG, "HERE::::: H4");
					grayscaleImage.setResolutionIsAspectRatio(false);
					grayscaleImage.setHorzResolution(500);
					grayscaleImage.setVertResolution(500);
				}

                Log.i(TAG, "HERE::::: Check exception here");
				return ExtractorManager.getInstance().getExtractor().extract(grayscaleImage, NFPosition.UNKNOWN, NFImpressionType.LIVE_SCAN_PLAIN);
			} finally {
				if (grayscaleImage != null) {
                    Log.i(TAG, "HERE::::: H5");
					grayscaleImage.dispose();
				}
                Log.i(TAG, "HERE::::: H333333");
			}
		}

		private boolean enroll(ByteBuffer byteBuffer, String name, boolean checkForDuplicates) {
/*			DBAdapter db = DBAdapter.getInstance();
			if (checkForDuplicates) {
				boolean duplicateFound = false;
				NMatcher matcher = MatchingManager.getInstance().getMatcher();
				matcher.identifyStart(byteBuffer);
				for (Record r : db.getFingers().getRecords()) {
					ByteBuffer b = r.getTemplate();
					if (matcher.identifyNext(b) > 0) {
						duplicateFound = true;
						break;
					}
				}
				matcher.identifyEnd();
				if (duplicateFound) {
					return false;
				}
				byteBuffer.flip();
			}
			db.getFingers().insertTemplate(name, byteBuffer);  */
			return true;
		}

		private int verify(ByteBuffer byteBuffer, String candidateName) {
//			ByteBuffer candidate = DBAdapter.getInstance().getFingers().getTemplateByName(candidateName);
//			return MatchingManager.getInstance().getMatcher().verify(byteBuffer, candidate);
            return 0; //WAEL ADDED IT!!!
		}

        private int verify(ByteBuffer byteBuffer1, ByteBuffer byteBuffer2) {
//			ByteBuffer candidate = DBAdapter.getInstance().getFingers().getTemplateByName(candidateName);
//			return MatchingManager.getInstance().getMatcher().verify(byteBuffer, candidate);
            return MatchingManager.getInstance().getMatcher().verify(byteBuffer1, byteBuffer2);
		}

		private List<Pair<String, Integer>> identify(ByteBuffer byteBuffer) {
			List<Pair<String, Integer>> results = new ArrayList<Pair<String, Integer>>();
//			NMatcher matcher = MatchingManager.getInstance().getMatcher();
//			NMatchingDetails details = matcher.identifyStartWithMD(byteBuffer);
//			for (Record record : DBAdapter.getInstance().getFingers().getRecords()) {
//				int score = matcher.identifyNext(record.getTemplate(), details);
//				if (score > 0) {
//					results.add(new Pair<String, Integer>(record.getName(), score));
//				}
//			}
//			matcher.identifyEnd();
			return results;
		}

		private NFRecord loadNTemplate(ByteBuffer data) throws IOException {
			if (data == null) {
				throw new NullPointerException("data");
			}
			NTemplate.check(data);
			NTemplate template = new NTemplate(data);
			return template.getFingers().getRecords().get(0);
		}

		private NImage loadFIRecord(ByteBuffer data) throws IOException {
			if (data == null) {
				throw new NullPointerException("mData");
			}
			if (mStandard == null) {
				throw new NullPointerException("mStandard");
			}
			FIRecord fiRec = new FIRecord(data, mStandard);
			return fiRec.getFingerViews().get(0).toNImage();
		}

		private NFRecord loadFMRecord(ByteBuffer data) throws IOException {
			if (data == null) {
				throw new NullPointerException("mData");
			}
			if (mStandard == null) {
				throw new NullPointerException("mStandard");
			}
			FMRecord fmRecord = new FMRecord(data, mStandard);
			NTemplate nTemplate = fmRecord.toNTemplate();
			return nTemplate.getFingers().getRecords().get(0);
		}

		// ===========================================================
		// Protected methods
		// ===========================================================

		@Override
		protected void onPreExecute() {
			if (mCallback != null) {
				mCallback.onBackgroundTaskStarted(mTask);
			}
            Log.i(TAG, "HERE::::: H33 POST ");
			super.onPreExecute();
		}

		@Override
		protected AsyncTaskResult<TaskResult> doInBackground(Void... args) {
			TaskResult result = new TaskResult();
			result.setTask(mTask);
			if (!isCancelled()) {
				try {
					switch (mTask) {
						case EXTRACT: {
							result.setExtractResult(extract(mImage));
                            Log.i(TAG, "HERE::::: H6");
							result.setBitmap(mImage.toBitmap());
                            Log.i(TAG, "HERE::::: H7 "+mImage.getImageSize());
						} break;
						case ENROLL: {
							result.setDuplicatePerson(!enroll(mData, mName, mCheckForDuplicates));
						} break;
						case VERIFY: {
							result.setVerificationResult(verify(mData, mCandidateName));
						} break;
                        case VERIFYWAEL: {
							result.setVerificationResult(verify(mData, mData2));
						} break;
						case IDENTIFY: {
							result.setIdentificationResults(identify(mData));
						} break;
						case LOAD_TEMPLATE: {
							result.setRecord(loadNTemplate(mData));
						} break;
						case LOAD_FIRECORD: {
							NImage image = loadFIRecord(mData);
							result.setExtractResult(extract(image));
							result.setBitmap(image.toBitmap());
						} break;
						case LOAD_FMRECORD: {
							result.setRecord(loadFMRecord(mData));
						} break;
						default: {
							throw new AssertionError("Unknown task: " + mTask);
						}
					}
				} catch (Exception e) {
					Log.e(TAG, "Exception", e);
					return new AsyncTaskResult<TaskResult>(e);
				}
			}
			return new AsyncTaskResult<TaskResult>(result);
		}

		@Override
		protected void onPostExecute(AsyncTaskResult<TaskResult> result) {
			if (mCallback != null) {
				if (result.getException() == null) {
                    Log.i(TAG, "HERE::::: H7 " +result.getResult().toString());
					mCallback.onBackgroundTaskFinished(result.getResult());
				} else {
                    Log.i(TAG, "HERE::::: H8 " +result.getException().toString());
					mCallback.onBackgroundTaskException(mTask, result.getException());
				}
			}
		}
	}

	public static final class TaskResult {

		private Task mTask;
		private Bitmap mBitmap;
		private ExtractResult mExtractResult;
		private NFRecord mRecord;
		private int mVerificationResult;
		private List<Pair<String, Integer>> mIdentificationResults;
		private boolean mDuplicatePerson;

		// Suppress default constructor for noninstantiability from outside the enclosing class.
		private TaskResult() {
		}

		public Task getTask() {
			return mTask;
		}

		private void setTask(Task task) {
			this.mTask = task;
		}

		public Bitmap getBitmap() {
			return mBitmap;
		}

		private void setBitmap(Bitmap bitmap) {
			this.mBitmap = bitmap;
		}

		public ExtractResult getExtractResult() {
			return mExtractResult;
		}

		private void setExtractResult(ExtractResult extractResult) {
			this.mExtractResult = extractResult;
		}

		public List<Pair<String, Integer>> getIdentificationResults() {
			return mIdentificationResults;
		}

		private void setIdentificationResults(List<Pair<String, Integer>> identificationResults) {
			this.mIdentificationResults = identificationResults;
		}

		public int getVerificationResult() {
			return mVerificationResult;
		}

		private void setVerificationResult(int verificationResult) {
			this.mVerificationResult = verificationResult;
		}

		public boolean isDuplicatePerson() {
			return mDuplicatePerson;
		}

		private void setDuplicatePerson(boolean duplicatePerson) {
			this.mDuplicatePerson = duplicatePerson;
		}

		public NFRecord getRecord() {
			return mRecord;
		}

		public void setRecord(NFRecord record) {
			this.mRecord = record;
		}
	}

}
