package com.neurotec.samples.multibiometric;

import java.io.File;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import com.neurotec.samples.report.CrashReporter;
import com.neurotec.samples.util.EnvironmentUtils;

import nic.common.datamodel.RegistrationData;

import org.acra.ACRA;
import org.acra.ErrorReporter;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(
formKey = "",
mode = ReportingInteractionMode.NOTIFICATION,
includeDropBoxSystemTags = true,
resToastText = R.string.msg_crash_toast_text,
resNotifTickerText = R.string.msg_crash_notif_ticker_text,
resNotifTitle = R.string.msg_crash_notif_title,
resNotifText = R.string.msg_crash_notif_text,
resNotifIcon = android.R.drawable.stat_notify_error,
resDialogText = R.string.msg_crash_dialog_text,
resDialogIcon = android.R.drawable.ic_dialog_info,
resDialogCommentPrompt = R.string.msg_crash_dialog_comment_prompt,
resDialogOkToast = R.string.msg_crash_dialog_ok_toast )

public final class BiometricApplication extends Application {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static final String TAG = BiometricApplication.class.getSimpleName();

	// ===========================================================
	// Public static fields
	// ===========================================================

	public static final String APP_NAME = "multibiometric";
	public static final String SAMPLE_DATA_DIR_PATH = EnvironmentUtils.getDataDirectoryPath(EnvironmentUtils.SAMPLE_DATA_DIR_NAME, APP_NAME);
	public static final String SAMPLE_ISO_DIR_NAME = "iso";
	public static final String SAMPLE_ANSI_DIR_NAME = "ansi";
	public static final String CACHED_PHOTO_FILE = Environment
			.getExternalStorageDirectory().getAbsolutePath()
			+ File.separator + "CredenceNIC"
			+ File.separator + "facial_photo.tmp";

	// ===========================================================
	// Registration Data
	// ===========================================================
	
	public static RegistrationData registrationData = null;
	
	// ===========================================================
	// Public methods
	// ===========================================================

	static {
	    System.loadLibrary("iconv");
	}
	
    @Override
	public void onCreate() {
		super.onCreate();

		try {
			System.setProperty("jna.nounpack", "true");
			System.setProperty("java.io.tmpdir", getCacheDir().getAbsolutePath());
			ACRA.init(this);
			ErrorReporter.getInstance().setReportSender(new CrashReporter());
//			DBAdapter.init(this);
//			DBAdapter.getInstance().open();
//			OrientationSensorManager.init(this);
		} catch (Exception e) {
			Log.e(TAG, "Exception", e);
		}
	}
    
    /**
	 * Deletes any previously cached id scan if any
	 */
	public static void clearCacheData() {
		File photoFile = new File(CACHED_PHOTO_FILE);
		Log.d(TAG, "Photo existence status: " + photoFile.exists());
		Log.d(TAG, "Photo deletion status: " + photoFile.delete());
	}
}
