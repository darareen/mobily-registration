package com.neurotec.samples.multibiometric.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.neurotec.samples.preferences.PreferenceTools;

public final class EnrollmentManager {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static EnrollmentManager sInstance;

	// ===========================================================
	// Public static methods
	// ===========================================================

	public static EnrollmentManager getInstance() {
		if (sInstance == null) {
			sInstance = new EnrollmentManager();
		}
		return sInstance;
	}

	// ===========================================================
	// Private fields
	// ===========================================================

	private boolean mEnrollmentCheckForDuplicates;

	// ===========================================================
	// Private constructor
	// ===========================================================

	private EnrollmentManager() {
		mEnrollmentCheckForDuplicates = EnrollmentPreferences.DEFAULT_CHECK_FOR_DUPLICATES_VALUE;
	}

	// ===========================================================
	// Public methods
	// ===========================================================

	public void update(Context context) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
		mEnrollmentCheckForDuplicates = PreferenceTools.getBoolean(preferences, EnrollmentPreferences.ENROLLMENT_CHECK_FOR_DUPLICATES, EnrollmentPreferences.DEFAULT_CHECK_FOR_DUPLICATES_VALUE);
	}

	public boolean isCheckForDuplicates() {
		return mEnrollmentCheckForDuplicates;
	}

}
