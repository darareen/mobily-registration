package com.neurotec.samples.multibiometric.preference;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import com.neurotec.samples.multibiometric.R;
import com.neurotec.samples.preferences.BasePreferenceActivity;

public final class EnrollmentPreferences extends BasePreferenceActivity {

	// ===========================================================
	// Public static fields
	// ===========================================================

	public static final String ENROLLMENT_CHECK_FOR_DUPLICATES = "enrollment_check_for_duplicates";
	public static final String SET_DEFAULT_ENROLLMENT_PREFERENCES = "set_default_enrollment_preferences";

	public static final boolean DEFAULT_CHECK_FOR_DUPLICATES_VALUE = true;

	// ===========================================================
	// Private fields
	// ===========================================================

	private CheckBoxPreference mCheckForDuplicates;

	// ===========================================================
	// Private methods
	// ===========================================================

	private void resetPreferences() {
		setEnrollmentCheckForDuplicates(DEFAULT_CHECK_FOR_DUPLICATES_VALUE);
	}


	private void setEnrollmentCheckForDuplicates(boolean value) {
		this.mCheckForDuplicates.setChecked(value);
	}

	// ===========================================================
	// Protected methods
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.enrollment_preferences);

		mCheckForDuplicates = (CheckBoxPreference) getPreferenceScreen().findPreference(ENROLLMENT_CHECK_FOR_DUPLICATES);

		Preference resetDefaults = (Preference) findPreference(SET_DEFAULT_ENROLLMENT_PREFERENCES);
		resetDefaults.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				resetPreferences();
				showToast(R.string.msg_settings_reset);
				return true;
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		setEnrollmentCheckForDuplicates(EnrollmentManager.getInstance().isCheckForDuplicates());
	}

	@Override
	protected void onStop() {
		super.onStop();
		EnrollmentManager.getInstance().update(this);
	}
}
