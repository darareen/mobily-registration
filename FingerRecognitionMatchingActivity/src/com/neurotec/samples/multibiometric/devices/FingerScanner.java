package com.neurotec.samples.multibiometric.devices;

import android.graphics.Bitmap;

public abstract class FingerScanner {

	// ===========================================================
	// Private fields
	// ===========================================================

	private String mName;

	// ===========================================================
	// Protected constructor
	// ===========================================================


	protected FingerScanner(String name) {
		this.mName = name;
	}

	// ===========================================================
	// Protected methods
	// ===========================================================

	protected void setName(String value) {
		mName = value;
	}

	// ===========================================================
	// Public abstract methods
	// ===========================================================

	public abstract CaptureResult capture(ICapturePreview preview);
	public abstract void close();

	// ===========================================================
	// Public methods
	// ===========================================================

	public String getName() {
		return mName;
	}

	@Override
	public String toString() {
		return mName;
	}

	// ===========================================================
	// Interface
	// ===========================================================

	public interface ICapturePreview {
		public void preview(CaptureResult result);
	}


	// ===========================================================
	// Inner class
	// ===========================================================

	public class CaptureResult {
		private Bitmap mImage;
		private String mStatusMessage;

		public CaptureResult(Bitmap image, String statusMessage) {
			this.mImage = image;
			this.mStatusMessage = statusMessage;
		}

		public Bitmap getImage() {
			return mImage;
		}

		public String getStatusMessage() {
			return mStatusMessage;
		}
	}
}
