package com.neurotec.samples.multibiometric;

import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import com.neurotec.samples.app.BaseActivity;

import java.util.List;

public abstract class BiometricBaseActivity extends BaseActivity {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static final String TAG = BiometricBaseActivity.class.getSimpleName();

	// ===========================================================
	// Public abstract methods
	// ===========================================================

	protected abstract String getTableName();
	protected abstract Class<?> getPreferenceActivity();
	protected abstract List<String> getComponents();

	// ===========================================================
	// Activity events
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
			requestWindowFeature(Window.FEATURE_NO_TITLE);
		} catch (Exception e) {
			hideProgress();
			showError(e.getMessage());
		}
	}

	// ===========================================================
	// Menu events
	// ===========================================================

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_preferences: {
				// change 11 to Build.VERSION_CODES.HONEYCOMB for compilation for android 3.0 and above
				// android versions till 3.0 doesn't understand Build.VERSION_CODES.HONEYCOMB
				if (android.os.Build.VERSION.SDK_INT >= 11) {
					startActivity(new Intent(this, getPreferenceActivity()));
				} else {
					//Change to appropriate approach (Android 3.0) - > use preferenceFragments to build Preference hierarchy
					startActivity(new Intent(this, getPreferenceActivity()));
				}
				break;
			}
//			case R.id.menu_db_view: {
//				Intent intent = new Intent(this, DBViewActivity.class);
//				intent.putExtra(DBViewActivity.EXTRA_TABLE_NAME, getTableName());
//				startActivity(intent);
//				break;
//			}
//			case R.id.menu_about: {
//				startActivity(new Intent(this, InfoActivity.class));
//				break;
//			}
		}
		return true;
	}

}
