package com.neurotec.samples.licensing.app;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.neurotec.samples.multibiometric.R;
import com.neurotec.samples.app.BaseActivity;
import com.neurotec.samples.licensing.License;
import com.neurotec.samples.licensing.LicensingServiceManager;
import com.neurotec.samples.net.ConnectivityHelper;

import java.util.ArrayList;
import java.util.List;

public final class ActivationActivity extends BaseActivity {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static final String TAG = ActivationActivity.class.getSimpleName();

	// ===========================================================
	// Private fields
	// ===========================================================

	private ListView mListView = null;
	private Button mButtonActivate = null;
	private Button mButtonDeactivate = null;
	private BackgroundTask mTask = null;

	// ===========================================================
	// Private methods
	// ===========================================================

	private List<License> getSelectedLicenses() {
		List<License> licenses = new ArrayList<License>();
		SparseBooleanArray checkedItems = mListView.getCheckedItemPositions();
		if (checkedItems != null) {
			for (int i = 0; i < checkedItems.size(); i++) {
				if (checkedItems.valueAt(i)) {
					licenses.add((License) mListView.getAdapter().getItem(checkedItems.keyAt(i)));
				}
			}
		}
		return licenses;
	}

	@SuppressWarnings("unchecked")
	private void clearView() {
		mListView.clearChoices();
		((ArrayAdapter<License>) mListView.getAdapter()).notifyDataSetChanged();
		updateLicenses();
	}

	private void updateView() {
		boolean activateEnabled = true;
		boolean deactivateEnabled = true;

		List<License> licenses = getSelectedLicenses();

		if (licenses == null || licenses.isEmpty()) {
			activateEnabled = deactivateEnabled = false;
		} else {
			for (License license : licenses) {
				activateEnabled &= !license.isActivated();
				deactivateEnabled &= license.isActivated();
			}
		}

		mButtonActivate.setVisibility(activateEnabled ? View.VISIBLE : View.GONE);
		mButtonDeactivate.setVisibility(deactivateEnabled ? View.VISIBLE : View.GONE);
	}

	private void updateLicenses() {
		try {
			mListView.setAdapter(null);
			List<License> licenses = LicensingServiceManager.getLicenses();
			if (licenses == null || licenses.isEmpty()) {
				showToast(R.string.msg_no_licenses);
			} else {
				mListView.setAdapter(new LicenseListAdapter(this, R.layout.license_list_item, licenses));
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			showToast(e.getMessage());
		}
		updateView();
	}

	// ===========================================================
	// Activity events
	// ===========================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			setContentView(R.layout.activation_view);
			mListView = (ListView) findViewById(R.id.list_view_serial_numbers);
			mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
			mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					updateView();
				}
			});
			mButtonActivate = (Button) findViewById(R.id.button_activate);
			mButtonActivate.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						mTask = new BackgroundTask();
						mTask.activate(getSelectedLicenses());
					} catch (Exception e) {
						Log.e(TAG, e.getMessage(), e);
						showToast(e.getMessage());
					}
				}
			});
			mButtonDeactivate = (Button) findViewById(R.id.button_deactivate);
			mButtonDeactivate.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					try {
						if (ConnectivityHelper.isConnected(ActivationActivity.this)) {
							mTask = new BackgroundTask();
							mTask.deactivate(getSelectedLicenses());
						} else {
							showToast(R.string.msg_no_connection);
						}
					} catch (Exception e) {
						Log.e(TAG, e.getMessage(), e);
						showToast(e.getMessage());
					}
				}
			});
		} catch (Exception e) {
			Log.e(TAG, e.getMessage(), e);
			showError(e.getMessage());
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		updateLicenses();
	}

	@Override
	protected void onDestroy() {
		if (mTask != null) {
			mTask.cancel(true);
			mTask = null;
		}
		super.onDestroy();
	}

	private enum Task {
		ACTIVATE, DEACTIVATE
	}

	private final class BackgroundTask extends AsyncTask<Boolean, String, String> {

		private Task mTask;
		private List<License> mLicenses;

		void activate(List<License> licenses) {
			if (licenses == null) throw new NullPointerException("licenses");
			if (licenses.isEmpty()) throw new IllegalArgumentException("licenses < 0");
			mTask = Task.ACTIVATE;
			mLicenses = licenses;
			showProgress(getString(R.string.msg_activating));
			execute();
		}

		void deactivate(List<License> licenses) {
			if (licenses == null) throw new NullPointerException("licenses");
			if (licenses.isEmpty()) throw new IllegalArgumentException("licenses < 0");
			mTask = Task.DEACTIVATE;
			mLicenses = licenses;
			showProgress(getString(R.string.msg_deactivating));
			execute();
		}

		@Override
		protected String doInBackground(Boolean... params) {
			boolean hasInternet = ConnectivityHelper.isConnected(ActivationActivity.this);
			try {
				if (!isCancelled()) {
					switch (mTask) {
					case ACTIVATE:
						for (License license : mLicenses) {
							license.activate(hasInternet);
						}
						return getString(hasInternet ? R.string.msg_activation_succeeded : R.string.msg_proceed_activation_online);
					case DEACTIVATE:
						for (License license : mLicenses) {
							license.deactivate();
						}
						return getString(R.string.msg_deactivation_succeeded);
					}
				}
			} catch (Exception e) {
				Log.e(TAG, "Exception", e);
				return e.getMessage();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			hideProgress();
			if (result != null) {
				showInfo(result);
			}
			mTask = null;
			clearView();
		}

		@Override
		protected void onProgressUpdate(String... messages) {
			for (String message : messages) {
				showProgress(message);
			}
		}
	}

}


class LicenseListAdapter extends ArrayAdapter<License> {
	View mRow;
	List<License> mLicenses;
	int resLayout;
	Context context;

	public LicenseListAdapter(Context context, int textViewResourceId, List<License> licenses) {
		super(context, textViewResourceId, licenses);
		this.mLicenses = licenses;
		resLayout = textViewResourceId;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		mRow = convertView;
		if (mRow == null) { // inflate our custom layout. resLayout == R.layout.row_team_layout.xml
			LayoutInflater ll = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mRow = ll.inflate(resLayout, parent, false);
		}

		License license = mLicenses.get(position); // Produce a mRow for each Team.

		if (license != null) {
			TextView text = (TextView) mRow.findViewById(R.id.list_item_license_text);
			ImageView image = (ImageView) mRow.findViewById(R.id.list_item_license_image);
			text.setText(license.getName());

			if (license.isActivated()) {
				image.setImageResource(android.R.drawable.ic_input_add);
			} else {
				image.setImageResource(android.R.drawable.ic_menu_help);
			}
		}
		return mRow;
	}
}
