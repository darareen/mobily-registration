package com.neurotec.samples.util;

import android.content.Context;
import android.graphics.*;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import com.neurotec.images.NImage;
import com.neurotec.images.NImageFormat;
import com.neurotec.images.NPixelFormat;

import java.io.*;
import java.nio.ByteBuffer;

public final class ImageUtils {

	// ===========================================================
	// Private static fields
	// ===========================================================

	private static final String TAG = ImageUtils.class.getSimpleName();
	private static final int DEFAULT_WIDTH = 640;
	private static final int DEFAULT_HEIGHT = 480;

	// ===========================================================
	// Private constructor
	// ===========================================================

	private ImageUtils() {
	}

	// ===========================================================
	// Public static methods
	// ===========================================================

	public static Bitmap cropRotateScaleImage(Bitmap image, Rect rect, float rotation, int containerWidth, int containerHeight, Matrix resultMatrix) {
		Matrix matrix1 = new Matrix();
		matrix1.postRotate(rotation, image.getWidth() / 2F, image.getHeight() / 2F);

		Bitmap rotatedBitmap = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix1, true);
		int diffX = (rotatedBitmap.getWidth() - image.getWidth()) / 2;
		int diffY = (rotatedBitmap.getHeight() - image.getHeight()) / 2;
		matrix1.postTranslate(diffX, diffY);

		float center[] = new float[] { rect.centerX(), rect.centerY() };
		matrix1.mapPoints(center);

		Matrix translateRectMatrix = new Matrix();
		translateRectMatrix.setTranslate(center[0] - rect.centerX(), center[1] - rect.centerY());
		RectF mappedRect = new RectF(rect);
		translateRectMatrix.mapRect(mappedRect);

		float scale = 1f;
		if ((containerWidth > 0) && (containerHeight > 0)) {
			scale = Math.min((float) containerWidth / mappedRect.width(), (float) containerHeight / mappedRect.height());
		}

		resultMatrix.set(matrix1);
		resultMatrix.postTranslate(-mappedRect.left, -mappedRect.top);
		resultMatrix.postScale(scale, scale);

		Matrix matrix2 = new Matrix();
		matrix2.postScale(scale, scale);

		mappedRect.left = Math.max(0, mappedRect.left);
		mappedRect.top = Math.max(0, mappedRect.top);
		mappedRect.right = Math.min(rotatedBitmap.getWidth(), mappedRect.right);
		mappedRect.bottom = Math.min(rotatedBitmap.getHeight(), mappedRect.bottom);

		return Bitmap.createBitmap(rotatedBitmap, (int) mappedRect.left, (int) mappedRect.top, (int) mappedRect.width(), (int) mappedRect.height(), matrix2, true);
	}

	public static void saveBitmap(Context context, Bitmap bitmap, Uri fileUri) throws IOException {
		if (context == null) throw new NullPointerException("context");
		if (bitmap == null) throw new NullPointerException("bitmap");
		if (fileUri == null) throw new NullPointerException("fileUri");

		OutputStream os = null;
		try {
			os = context.getContentResolver().openOutputStream(fileUri);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, os);
			os.flush();
		} catch (FileNotFoundException e) {
			throw new IOException("File not found");
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					Log.e(TAG, "Error closing OutputStream", e);
				}
			}
		}
	}

	public static void saveBitmap(Bitmap bitmap, String filename) throws IOException {
		if (bitmap == null) throw new NullPointerException("bitmap");
		if (filename == null) throw new NullPointerException("filename");

		if (!EnvironmentUtils.isSdPresent()) {
			throw new IllegalStateException("SD card is not ready");
		}
		String path = Environment.getExternalStorageDirectory().toString();
		OutputStream os = null;
		try {
			File file = new File(path, filename);
			os = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 85, os);
			os.flush();
		} catch (FileNotFoundException e) {
			throw new IOException("File not found");
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					Log.e(TAG, "Error closing OutputStream", e);
				}
			}
		}
	}

	public static Bitmap getBitmapFromUri(Context context, Uri uri) throws IOException {
		if (context == null) throw new NullPointerException("context");
		if (uri == null) throw new NullPointerException("uri");

		InputStream is = null;
		try {
			is = context.getContentResolver().openInputStream(uri);
			//Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, o);
			int scale = 1;
			int h = (int) Math.ceil(o.outHeight / (float) DEFAULT_HEIGHT);
			int w = (int) Math.ceil(o.outWidth / (float) DEFAULT_WIDTH);

			if (h > 1 || w > 1) {
				if (h > w) {
					scale = h;

				} else {
					scale = w;
				}
			}
			//Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			//TODO: close() might cause IOException exception
			is.close();
			is = context.getContentResolver().openInputStream(uri);
			return BitmapFactory.decodeStream(is, null, o2);
		} catch (FileNotFoundException e) {
			throw new IOException("File not found");
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					Log.e(TAG, "Error closing InputStream", e);
				}
			}
		}
	}

	public static NImage getImageFromUri(Context context, Uri uri) throws IOException {
		if (context == null) throw new NullPointerException("context");
		if (uri == null) throw new NullPointerException("uri");

		InputStream is = null;
		try {
			is = context.getContentResolver().openInputStream(uri);
			return NImage.fromInputStream(is);
		} catch (FileNotFoundException e) {
			throw new IOException("File not found");
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					Log.e(TAG, "Error closing InputStream", e);
				}
			}
		}
	}

	public static NImage fromJPEG(byte[] data) {
		ByteBuffer srcPixels = ByteBuffer.wrap(data);
		return NImage.fromMemory(srcPixels, NImageFormat.getJPEG());
	}

	public static NImage fromNV21(byte[] data, int width, int height) {
		// Take only intensity (Y) data from preview byte buffer.
		ByteBuffer srcPixels = ByteBuffer.wrap(data, 0, width * height);
		return NImage.getWrapper(NPixelFormat.GRAYSCALE_8U, width, height, width, srcPixels);
	}

	public static Bitmap toBitmap(byte[] data, int width, int height) {
		YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, width, height, null);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		yuvimage.compressToJpeg(new Rect(0, 0, width, height), 80, baos);
		byte[] bitmapData = baos.toByteArray();
		return BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
	}

}
