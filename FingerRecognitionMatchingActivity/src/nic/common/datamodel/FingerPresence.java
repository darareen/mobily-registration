/*
 * Author: Nate Roe
 * Source:
 * Revision:
 * Date:
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 * $Log: FingerPresence.java,v $
 * Revision 1.1  2006/06/28 06:49:32  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.8  2005/08/15 11:22:12  nroe
 * Roll back to java 1.4
 *
 * Revision 1.7  2005/08/15 06:49:30  nroe
 * Add various integer interpretations of these enumerated types
 *
 * Revision 1.6  2005/08/13 07:00:32  nroe
 * change package name to correspond with repository changes
 *
 * Revision 1.5  2005/08/09 06:52:11  nroe
 * intial checkin (second time; appropriate keyword expansion setting chosen)
 *
 *
 */
package nic.common.datamodel;

/**
 * Defines whether a finger is present or not.
 * <p>
 * <table>
 *   <tr>
 *     <th>Value</th>
 *     <th>Description</th>
 *   </tr>
 *   <tr>
 *     <td>PRESENT</td>
 *     <td>This finger is present</td>
 *   </tr>
 *   <tr>
 *     <td>AMPUTATED</td>
 *     <td>This finger is amputated</td>
 *   </tr>
 *   <tr>
 *     <td>NO_PRINT</td>
 *     <td>This finger is not amputated but no print can be taken</td>
 *   </tr>
 *   <tr>
 *     <td>UNDEFINED</td>
 *     <td>The presence of this finger has not (yet) been defined</td>
 *   </tr>
 * </table>
 *
 * @author nroe
 */
public class FingerPresence {
    public static final int PRESENT = 0;
    public static final int AMPUTATED = 1;
    public static final int NO_PRINT = 2;
    public static final int UNDEFINED = 3;
}
