package nic.common.datamodel;

public class HijriYear {
	
	private int hijriYear;
	private String indicator;
	private long lowerGregDate;
	private long upperGregDate;

	public int getHijriYear() {
		return hijriYear;
	}
	public void setHijriYear(int hijriYear) {
		this.hijriYear = hijriYear;
	}
	public String getIndicator() {
		return indicator;
	}
	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}
	public long getLowerGregDate() {
		return lowerGregDate;
	}
	public void setLowerGregDate(long lowerGregDate) {
		this.lowerGregDate = lowerGregDate;
	}
	public long getUpperGregDate() {
		return upperGregDate;
	}
	public void setUpperGregDate(long upperGregDate) {
		this.upperGregDate = upperGregDate;
	}

}
