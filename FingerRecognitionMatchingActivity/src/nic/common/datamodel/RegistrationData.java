package nic.common.datamodel;

import android.graphics.Bitmap;

public class RegistrationData {

	private Person person = null;
	private String simNumber = null;
	private String pukNumber = null;
	private byte[] idImgArray = null;
	private byte[] sigImgArray = null;
	private Bitmap capturedFingerPrint = null;
	
	public RegistrationData() {
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public String getSimNumber() {
		return simNumber;
	}

	public void setSimNumber(String simNumber) {
		this.simNumber = simNumber;
	}

	public String getPukNumber() {
		return pukNumber;
	}

	public void setPukNumber(String pukNumber) {
		this.pukNumber = pukNumber;
	}

	public byte[] getIdImgArray() {
		return idImgArray;
	}

	public void setIdImgArray(byte[] idImgArray) {
		this.idImgArray = idImgArray;
	}

	public byte[] getSigImgArray() {
		return sigImgArray;
	}

	public void setSigImgArray(byte[] sigImgArray) {
		this.sigImgArray = sigImgArray;
	}

	public Bitmap getCapturedFingerPrint() {
		return capturedFingerPrint;
	}

	public void setCapturedFingerPrint(Bitmap capturedFingerPrint) {
		this.capturedFingerPrint = capturedFingerPrint;
	}
}
