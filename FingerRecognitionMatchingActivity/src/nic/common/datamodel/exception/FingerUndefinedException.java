/*
 * Author: nroe
 * Source:
 * Revision:
 * Date:
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 * $Log: FingerUndefinedException.java,v $
 * Revision 1.6  2006/06/28 07:52:40  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.1  2006/06/28 06:49:34  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.4  2005/08/13 07:21:26  nroe
 * fix inheritance to refer to DataException
 *
 * Revision 1.3  2005/08/13 07:03:05  nroe
 * change package to correspond to repository changes
 *
 * Revision 1.2  2005/08/09 06:48:41  nroe
 * Change enumerations to the new enum type, modify other classes to work with these new enumerations
 *
 * Revision 1.1  2005/08/07 12:32:57  aabdrabbou
 * Take out dbbeans in a separate project
 *
 *
 */
package nic.common.datamodel.exception;


/**
 *
 * @author nroe
 */
public class FingerUndefinedException extends DataException {

    /**
     * @param msg
     */
    public FingerUndefinedException(String msg) {
        super(msg);
    }

}
