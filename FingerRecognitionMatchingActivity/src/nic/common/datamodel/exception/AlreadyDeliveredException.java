package nic.common.datamodel.exception;

public class AlreadyDeliveredException extends Exception {
	public AlreadyDeliveredException(String mesg){
		super(mesg);
	}
	
	public AlreadyDeliveredException(Throwable parent){
		super(parent);
	}
}
