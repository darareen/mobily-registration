package nic.common.datamodel.exception;

public class NoCardPrintRecordException extends Exception {
	public NoCardPrintRecordException(String mesg){
		super(mesg);
	}
	
	public NoCardPrintRecordException(Throwable parent){
		super(parent);
	}
}
