package nic.common.datamodel;

public class Visitor extends Person {
	private long visaNumber = -1;
	private int visaIssueDate = -1;
	private int visaExpiryDate = -1;
	private int daysVisaExtended = -1;
	private long sponsorNumber = -1;
	private String visaIssuePlace = null;
	private String sponsorName = null;
	private String sponsorAddress = null;
	private String passportIssuePlace = null;
	private String passportIssueDate = null;
	private String passportExpiryDate = null;
	
	public int getVisaIssueDate() {
		return visaIssueDate;
	}
	
	public void setVisaIssueDate(int visaIssueDate) {
		this.visaIssueDate = visaIssueDate;
	}
	
	public String getVisaIssuePlace() {
		return visaIssuePlace;
	}
	
	public void setVisaIssuePlace(String visaIssuePlace) {
		this.visaIssuePlace = visaIssuePlace;
	}
	
	public long getVisaNumber() {
		return visaNumber;
	}
	
	public void setVisaNumber(long visaNumber) {
		this.visaNumber = visaNumber;
	}

	public int getDaysVisaExtended() {
		return daysVisaExtended;
	}

	public void setDaysVisaExtended(int daysVisaExtended) {
		this.daysVisaExtended = daysVisaExtended;
	}

	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}

	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}

	public String getPassportIssueDate() {
		return passportIssueDate;
	}

	public void setPassportIssueDate(String passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}

	public String getPassportIssuePlace() {
		return passportIssuePlace;
	}

	public void setPassportIssuePlace(String passportIssuePlace) {
		this.passportIssuePlace = passportIssuePlace;
	}

	public String getSponsorAddress() {
		return sponsorAddress;
	}

	public void setSponsorAddress(String sponsorAddress) {
		this.sponsorAddress = sponsorAddress;
	}


	public String getSponsorName() {
		return sponsorName;
	}

	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}

	public int getVisaExpiryDate() {
		return visaExpiryDate;
	}

	public void setVisaExpiryDate(int visaExpiryDate) {
		this.visaExpiryDate = visaExpiryDate;
	}
	
	public long getSponsorNumber() {
		return sponsorNumber;
	}

	public void setSponsorNumber(long sponsorNumber) {
		this.sponsorNumber = sponsorNumber;
	}
}
