/*
 * $Author$
 * $Source$
 * $Revision$
 * $Date$
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA 
 * Phone: +1.310.615.0311 
 * 
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 */
package nic.common.datamodel;

/**
 * EnrollmentInfo
 *
 */
public class EnrollmentInfo {
	// Status Constants
	public static final int STATUS_ENROLLED =  1;
	public static final int STATUS_PROCESSING = 2;
	public static final int STATUS_PROCESS_LATER = 22;
	public static final int STATUS_HIT =  3;
	public static final int STATUS_ERROR =  4;
	public static final int STATUS_VISUAL = 5;
	public static final int STATUS_DELETED =  6;
	public static final int STATUS_ENROLLED_HIT =  7;
	public static final int STATUS_UPDATE =  8;
	public static final int STATUS_INQUIRY_NOHIT =  91;
	public static final int STATUS_INQUIRY_HIT =  93;

	// Type Constants
	public static final int TYPE_ENROLL_CITIZEN = 1;	
	public static final int TYPE_ENROLL_RESIDENT = 2;
	public static final int TYPE_ENROLL_VISITOR = 3;
	public static final int TYPE_ENROLL_ILLEGAL = 4;
	public static final int TYPE_ENROLL_DEPORTEE = 5;		
	public static final int TYPE_ENROLL_PILGRIM = 6;	
	public static final int TYPE_DELETE_CITIZEN = 11;	
	public static final int TYPE_DELETE_RESIDENT = 12;
	public static final int TYPE_DELETE_VISITOR = 13;
	public static final int TYPE_DELETE_ILLEGAL = 14;	
	public static final int TYPE_DELETE_DEPORTEE = 15;
	public static final int TYPE_DELETE_PILGRIM = 16;	
	public static final int TYPE_INQUIRY = 9;
	public static final int TYPE_ENROLL_MISCREANT = 7;	
	public static final int TYPE_DELETE_MISCREANT = 17;	
	// Subsystem Constants
	public static final int SUBSYSTEM_NONE = 0;	
	public static final int SUBSYSTEM_AFIS = 1;
	public static final int SUBSYSTEM_SIRIS = 2;
	public static final int SUBSYSTEM_ABIS = 3;
	public static final int SUBSYSTEM_AFIS_SIRIS = 12;		
	public static final int SUBSYSTEM_AFIS_ABIS = 13;	
	public static final int SUBSYSTEM_SIRIS_ABIS = 23;	
	public static final int SUBSYSTEM_AFIS_SIRIS_ABIS = 123;
	public static final int SUBSYSTEM_NOT_SYNC_SIRIS = -2;
	public static final int SUBSYSTEM_NOT_SYNC_ABIS = -3;
	public static final int SUBSYSTEM_NOT_SYNC_AFIS_SIRIS = -12;		
	public static final int SUBSYSTEM_NOT_SYNC_AFIS_ABIS = -13;	
	public static final int SUBSYSTEM_NOT_SYNC_SIRIS_ABIS = -23;	
	public static final int SUBSYSTEM_NOT_SYNC_AFIS_SIRIS_ABIS = -123;
	
	// Class Attributes
	private long  tcn = 0;
	private long enrollee = 0;
    private long biometricId = 0;	
    private long operator = 0;
    private long supervisor = 0;
    private int	location = 0;
    private int type = 0;
    private long captureTime = 0;
    private int status = 0;
    private int subsystem = 0;    
    private long xCandidate = 0;
    private String tliCandidate = null;

    public EnrollmentInfo() {
    	this(0, 0, 0, 0, 0, 0, 0, 0, 0);
    }
    
	public EnrollmentInfo(long tcn, long enrollee, long operator, long supervisor, int location, int type, int status) {
		this(tcn, enrollee, 0, operator, supervisor, location, type, status, 0);
	}
	
	public EnrollmentInfo(long tcn, long enrollee, long biometricId, long operator, long supervisor, int location, int type, int status, int subsystem) {
		this.tcn = tcn;
		this.enrollee = enrollee;
		this.biometricId = biometricId;		
		this.operator = operator;
		this.supervisor = supervisor;
		this.location = location;
		this.type = type;
		this.status = status;
		this.subsystem = subsystem;
	}

	public long getCaptureTime() {
		return captureTime;
	}
	
	public void setCaptureTime(long captureTime) {
		this.captureTime = captureTime;
	}
	
	public long getEnrollee() {
		return enrollee;
	}
	
	public void setEnrollee(long enrollee) {
		this.enrollee = enrollee;
	}
	
	public long getBiometricId() {
		return biometricId;
	}
	
	public void setBiometricId(long biometricId) {
		this.biometricId = biometricId;
	}
	
	public int getLocation() {
		return location;
	}
	
	public void setLocation(int location) {
		this.location = location;
	}
	
	public long getOperator() {
		return operator;
	}
	
	public void setOperator(long operator) {
		this.operator = operator;
	}
	
	public long getSupervisor() {
		return supervisor;
	}
	
	public void setSupervisor(long supervisor) {
		this.supervisor = supervisor;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getSubsystem() {
		return subsystem;
	}
	
	public void setSubsystem(int subsystem) {
		this.subsystem = subsystem;
	}
	
	public void setSubsystemInSync(boolean isInSync) {
		int absoluteSubsystem = Math.abs(subsystem);
		
		this.subsystem = isInSync ? absoluteSubsystem : -1 * absoluteSubsystem;
	}
		
	public long getTCN() {
		return tcn;
	}
	
	public void setTCN(long tcn) {
		this.tcn = tcn;
	}
		
	public int getType() {
		return type;
	}
	
	public void setType(int type) {
		this.type = type;
	}
	
	public long getXCandidate() {
		return xCandidate;
	}
	
	public void setXCandidate(long xCandidate) {
		this.xCandidate = xCandidate;
	}
	
	public String getTLICandidate() {
		return tliCandidate;
	}
	
	public void setTLICandidate(String tliCandidate) {
		this.tliCandidate = tliCandidate;
	}

	public boolean isStatusPending() {
		return status == STATUS_PROCESSING  || status == STATUS_VISUAL || status == STATUS_PROCESS_LATER;
	}
	
	public boolean isStatusEnrolled() {
		return status == STATUS_ENROLLED || status == STATUS_ENROLLED_HIT || 
		       status == STATUS_UPDATE;
	}
	
	public boolean isStatusFinishEnroll() {
		return status == STATUS_ENROLLED || status == STATUS_HIT || 
		       status == STATUS_ENROLLED_HIT || status == STATUS_UPDATE;
	}
	
	public boolean isStatusSubmitEnroll() {
		return isStatusFinishEnroll() || (isTypeEnroll() && isStatusPending());
	}
	
	public boolean isStatusFinishInquiry() {
		return status == STATUS_INQUIRY_HIT || status == STATUS_INQUIRY_NOHIT;
	}
	
	public boolean isTypeCitizen() {
		return type == TYPE_ENROLL_CITIZEN  || type == TYPE_DELETE_CITIZEN;
	}
	
	public boolean isTypeResident() {
		return type == TYPE_ENROLL_RESIDENT  || type == TYPE_DELETE_RESIDENT;
	}
	
	public boolean isTypeVisitor() {
		return type == TYPE_ENROLL_VISITOR  || type == TYPE_DELETE_VISITOR;
	}

	public boolean isTypeIllegal() {
		return type == TYPE_ENROLL_ILLEGAL  || type == TYPE_DELETE_ILLEGAL;
	}
	
	public boolean isTypeDeportee() {
		return type == TYPE_ENROLL_DEPORTEE  || type == TYPE_DELETE_DEPORTEE;
	}
	
	public boolean isTypePilgrim() {
		return type == TYPE_ENROLL_PILGRIM  || type == TYPE_DELETE_PILGRIM;
	}
	
	
	public boolean isTypeMiscreant() {
		return type == TYPE_ENROLL_MISCREANT  || type == TYPE_DELETE_MISCREANT;
	}
	
	public boolean isTypeEnroll() {
		return type == TYPE_ENROLL_CITIZEN || type == TYPE_ENROLL_RESIDENT ||
			   type == TYPE_ENROLL_VISITOR || type == TYPE_ENROLL_ILLEGAL ||
			   type == TYPE_ENROLL_DEPORTEE || type == TYPE_ENROLL_PILGRIM ||type == TYPE_ENROLL_MISCREANT ;
	}

	public boolean isTypeDelete() {
		return type == TYPE_DELETE_CITIZEN || type == TYPE_DELETE_RESIDENT ||
			   type == TYPE_DELETE_VISITOR || type == TYPE_DELETE_ILLEGAL || 
			   type == TYPE_DELETE_DEPORTEE || type == TYPE_DELETE_PILGRIM ||type == TYPE_DELETE_MISCREANT ;
	}
	
	public boolean isTypeInquiry() {
		return type == TYPE_INQUIRY;
	}
	
	public boolean isSubsystemAFIS() {
		int absoluteSubsystem = Math.abs(subsystem);
		
		return absoluteSubsystem == SUBSYSTEM_AFIS || absoluteSubsystem == SUBSYSTEM_AFIS_SIRIS ||
			   absoluteSubsystem == SUBSYSTEM_AFIS_ABIS || absoluteSubsystem == SUBSYSTEM_AFIS_SIRIS_ABIS;
	}
	
	public boolean isSubsystemSIRIS() {
		int absoluteSubsystem = Math.abs(subsystem);
		
		return absoluteSubsystem == SUBSYSTEM_SIRIS || absoluteSubsystem == SUBSYSTEM_AFIS_SIRIS ||
			   absoluteSubsystem == SUBSYSTEM_SIRIS_ABIS || absoluteSubsystem == SUBSYSTEM_AFIS_SIRIS_ABIS;
	}
	
	public boolean isSubsystemABIS() {
		int absoluteSubsystem = Math.abs(subsystem);
		
		return absoluteSubsystem == SUBSYSTEM_ABIS || absoluteSubsystem == SUBSYSTEM_AFIS_ABIS ||
			   absoluteSubsystem == SUBSYSTEM_SIRIS_ABIS || absoluteSubsystem == SUBSYSTEM_AFIS_SIRIS_ABIS;
	}
	
	public boolean isSubsystemInSync() {
		return subsystem >= 0;
	}
		
	public static int getSubsystemValue(boolean isAFIS, boolean isSIRIS, boolean isABIS) {
		return getSubsystemValue(isAFIS, isSIRIS, isABIS, true);
	}
	
	public static int getSubsystemValue(boolean isAFIS, boolean isSIRIS, boolean isABIS, boolean inSync) {
		if (isAFIS && isSIRIS && isABIS) {
			return inSync ? SUBSYSTEM_AFIS_SIRIS_ABIS : SUBSYSTEM_NOT_SYNC_AFIS_SIRIS_ABIS;
		} else if (isSIRIS && isABIS) {
			return inSync ? SUBSYSTEM_SIRIS_ABIS : SUBSYSTEM_NOT_SYNC_SIRIS_ABIS;
		} else if (isAFIS && isABIS) {
			return inSync ? SUBSYSTEM_AFIS_ABIS : SUBSYSTEM_NOT_SYNC_AFIS_ABIS;
		} else if (isAFIS && isSIRIS) {
			return inSync ? SUBSYSTEM_AFIS_SIRIS : SUBSYSTEM_NOT_SYNC_AFIS_SIRIS;
		} else if (isABIS) {
			return inSync ? SUBSYSTEM_ABIS : SUBSYSTEM_NOT_SYNC_ABIS;
		} else if (isSIRIS) {
			return inSync ? SUBSYSTEM_SIRIS : SUBSYSTEM_NOT_SYNC_SIRIS;
		} else if (isAFIS) {
			return SUBSYSTEM_AFIS;
		} else {
			return SUBSYSTEM_NONE;
		}
	}
}
