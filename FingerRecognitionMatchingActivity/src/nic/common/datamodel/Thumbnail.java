/*
 * $Author$
 * $Source$
 * $Revision$
 * $Date$
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA 
 * Phone: +1.310.615.0311 
 * 
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 * $Log$
 * Revision 1.4  2007/01/03 11:33:11  htahhan
 * Thumbnail(byte[] thumbnail) added
 *
 * Revision 1.3  2006/06/28 07:52:39  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.1  2006/06/28 06:49:31  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.1  2006/01/10 05:31:16  htahhan
 * Thumbnail added
 *
 */
package nic.common.datamodel;

/**
 * @author htahhan
 *
 */
public class Thumbnail {

	private byte[] thumbnail;
	
	private int width;
	
	private int height;

	public Thumbnail() {
	}
	
	public Thumbnail(byte[] thumbnail) {
		this.thumbnail = thumbnail;
	}

	public byte[] getThumbnail() {
		return thumbnail;
	}
	
	public void setThumbnail(byte[] thumbnail) {
		this.thumbnail = thumbnail;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
}
