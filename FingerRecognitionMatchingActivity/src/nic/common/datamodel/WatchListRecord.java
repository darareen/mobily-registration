package nic.common.datamodel;

public class WatchListRecord {
	
	// Source Constants
		public static final int SOURCE_WANTED 						= 1;
		public static final int SOURCE_MISCREANT 						= 2;
		public static final int SOURCE_DEPORTATION 						= 3;
		public static final int SOURCE_AFIS		 						= 4;	
		
		// Action Wanted Constants
		public static final int ACTION_WANTED_ARREST 					= 101;
		public static final int ACTION_WANTED_PAY_BAIL 					= 102;
		public static final int ACTION_WANTED_NOTIFY_TO_APPEAR 			= 103;
		public static final int ACTION_WANTED_DENY_PRISONER_TRAVEL 		= 104;
		
		// Action Miscreant Constants
		public static final int ACTION_MISCREANT_CAPTURE_ENTRY 			= 211;
		public static final int ACTION_MISCREANT_DENY_ENTRY 			= 212;
		public static final int ACTION_MISCREANT_ALLOW_ONE_ENTRY 		= 213;
		public static final int ACTION_MISCREANT_MONITOR_ENTRY 			= 214;
		public static final int ACTION_MISCREANT_CAPTURE_EXIT 			= 221;
		public static final int ACTION_MISCREANT_DENY_EXIT 				= 222;
		public static final int ACTION_MISCREANT_ALLOW_ONE_EXIT 		= 223;
		public static final int ACTION_MISCREANT_MONITOR_EXIT 			= 224;
		
		// Action Deportation Constants
		public static final int ACTION_DEPORTATION_PUNISHMENT_ISSUED 	= 301;
		public static final int ACTION_DEPORTATION_DEPORTED 			= 302;
		public static final int ACTION_DEPORTATION_DEPORTED_FOR_PERIOD 	= 303;
		public static final int ACTION_DEPORTATION_ORDER_ISSUED 		= 304;
		
		// Action AFIS Constants
		public static final int ACTION_AFIS_EX_CRIMINAL					= 401;
		public static final int ACTION_AFIS_LATENT			 			= 402;
		
		private int actionCode;

	    private String actionMessage;

	    private long samisId;

	    private int sourceCode;

	    private long sourceId;

	    private String sourceMessage;

	    public WatchListRecord() {
	    }

	    public WatchListRecord(
	           int actionCode,
	           String actionMessage,
	           long samisId,
	           int sourceCode,
	           long sourceId,
	           String sourceMessage) {
	           this.actionCode = actionCode;
	           this.actionMessage = actionMessage;
	           this.samisId = samisId;
	           this.sourceCode = sourceCode;
	           this.sourceId = sourceId;
	           this.sourceMessage = sourceMessage;
	    }


	    /**
	     * Gets the actionCode value for this WatchListRecord.
	     * 
	     * @return actionCode
	     */
	    public int getActionCode() {
	        return actionCode;
	    }


	    /**
	     * Sets the actionCode value for this WatchListRecord.
	     * 
	     * @param actionCode
	     */
	    public void setActionCode(int actionCode) {
	        this.actionCode = actionCode;
	    }


	    /**
	     * Gets the actionMessage value for this WatchListRecord.
	     * 
	     * @return actionMessage
	     */
	    public String getActionMessage() {
	        return actionMessage;
	    }


	    /**
	     * Sets the actionMessage value for this WatchListRecord.
	     * 
	     * @param actionMessage
	     */
	    public void setActionMessage(String actionMessage) {
	        this.actionMessage = actionMessage;
	    }


	    /**
	     * Gets the samisId value for this WatchListRecord.
	     * 
	     * @return samisId
	     */
	    public long getSamisId() {
	        return samisId;
	    }


	    /**
	     * Sets the samisId value for this WatchListRecord.
	     * 
	     * @param samisId
	     */
	    public void setSamisId(long samisId) {
	        this.samisId = samisId;
	    }


	    /**
	     * Gets the sourceCode value for this WatchListRecord.
	     * 
	     * @return sourceCode
	     */
	    public int getSourceCode() {
	        return sourceCode;
	    }


	    /**
	     * Sets the sourceCode value for this WatchListRecord.
	     * 
	     * @param sourceCode
	     */
	    public void setSourceCode(int sourceCode) {
	        this.sourceCode = sourceCode;
	    }


	    /**
	     * Gets the sourceId value for this WatchListRecord.
	     * 
	     * @return sourceId
	     */
	    public long getSourceId() {
	        return sourceId;
	    }


	    /**
	     * Sets the sourceId value for this WatchListRecord.
	     * 
	     * @param sourceId
	     */
	    public void setSourceId(long sourceId) {
	        this.sourceId = sourceId;
	    }


	    /**
	     * Gets the sourceMessage value for this WatchListRecord.
	     * 
	     * @return sourceMessage
	     */
	    public String getSourceMessage() {
	        return sourceMessage;
	    }


	    /**
	     * Sets the sourceMessage value for this WatchListRecord.
	     * 
	     * @param sourceMessage
	     */
	    public void setSourceMessage(String sourceMessage) {
	        this.sourceMessage = sourceMessage;
	    }


}
