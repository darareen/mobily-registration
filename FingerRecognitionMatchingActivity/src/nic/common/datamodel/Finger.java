package nic.common.datamodel;

import org.kobjects.base64.Base64;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;


/**
 * This class contains a single fingerprint impression.
 *
 * @author abdrabbou
 * @author nroe
 */
public class Finger implements KvmSerializable {




	/**
     * Finger type code.
     */
    private int type;
    
    
    /**
     * WSQ compressed image.
     */
    private byte[] image;

    /**
     * Finger presence code.
     */
    private int presence;

    public void setPresence(int pres){
        this.presence = pres;
    }
    
    /**
     * Returns the finger presence code, indicating whether an image is available.
     *
     * @return the finger presence code.
     */
    public int getPresence() {
        return presence;
    }

    /**
     * Call this method to indicate that no fingerprint image because a the finger
     * has been amputated.
     * <p>
     * This method sets the finger presence code to {@link sa.gov.nic.common.datamodel.Finger#AMPUTATED AMPUTATED} and the image to null.
     */
    public void setAmputated() {
        image = null;
        this.presence = FingerPresence.AMPUTATED;
    }

    /**
     * Call this method to indicate that no fingerprint image because a print cannot be
     * taken (bandaged, print worn off, etc.)
     * <p>
     * This method sets the finger presence code to {@link sa.gov.nic.common.datamodel.Finger#NO_PRINT NO_PRINT} and the image to null.
     */
    public void setNoPrint() {
        image = null;
        this.presence = FingerPresence.NO_PRINT;
    }

    /**
     * Get the WSQ compressed image of this fingerprint impression.
     *
     * @return Returns the image.
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Set the impression image.  This is expected to be a 500 DPI final scan, compressed with WSQ.
     * This method sets the finger's presence code to {@link sa.gov.nic.common.datamodel.Finger#PRESENT PRESENT}.
     *
     * @param image The image to set.
     */
    public void setImage(byte[] image) {
        this.image = image;
        presence = FingerPresence.PRESENT;
    }

    /**
     * Get the type of fingerprint impression.
     *
     * @return Returns the type.
     * @see FingerType
     */
    public int getType() {
        return type;
    }

    /**
     * Create a new finger of the designated type.  Finger presence is initially {@link sa.gov.nic.common.datamodel.Finger#UNDEFINED UNDEFINED}
     * until {@link #setAmputated()}, {@link #setNoPrint()}, or {@link #setImage(byte [])} is called.
     *
     * @param type The type of finger.
     *
     * @throws IllegalArgumentException if the finger type code is out of range (less than {@link FingerType#FIRST_FINGER_CODE} or greater than {@link FingerType#LAST_FINGER_CODE}.)
     * @see FingerType
     */
    public Finger(int type) {
		if (type >= FingerType.FIRST_FINGER_CODE && type <= FingerType.LAST_FINGER_CODE) {
			this.type = type;
			presence = FingerPresence.UNDEFINED;
		} else {
			throw new IllegalArgumentException("Finger type code " + type + " is out of range.");
		}
    }
    
    /**
     * Create a new finger of the designated type.
     *
     * @param type The type of finger.
     * @param image The image to set. 
     *
     * @throws IllegalArgumentException if the finger type code is out of range (less than {@link FingerType#FIRST_FINGER_CODE} or greater than {@link FingerType#LAST_FINGER_CODE}.)
     * @see FingerType
     */
    public Finger(int type, byte[] image) {
		if (type >= FingerType.FIRST_FINGER_CODE && type <= FingerType.LAST_FINGER_CODE) {
			this.type = type;
			this.image = image;
			
			if (image != null) {
				presence = FingerPresence.PRESENT;
			} else {
				presence = FingerPresence.AMPUTATED;
			}
		} else {
			throw new IllegalArgumentException("Finger type code " + type + " is out of range.");
		}
    }
    
    /**
     * Create a new finger of the designated type.
     *
     * @param type The type of finger.
     * @param presence The presence to set. 
     *
     * @throws IllegalArgumentException if the finger type code is out of range (less than {@link FingerType#FIRST_FINGER_CODE} or greater than {@link FingerType#LAST_FINGER_CODE}.)
     * @see FingerType
     */
    public Finger(int type, int presence) {
		if (type >= FingerType.FIRST_FINGER_CODE && type <= FingerType.LAST_FINGER_CODE) {
			this.type = type;
			this.presence = presence;
		} else {
			throw new IllegalArgumentException("Finger type code " + type + " is out of range.");
		}
    }
    
    public Finger() {
    	
    }
    public void setType(int type) {
        this.type = type;
    }

    @Override
	public Object getProperty(int arg0) {
		switch (arg0) {
		case 0:
			if (image == null)
			{
				return null;
			}
			else
			{
				return getImage();
			}
		case 1:
			return getPresence();
		case 2:
			return getType();
		default:
			break;
		}
		return null;

	}
	@Override
	public int getPropertyCount() {
		// TODO Auto-generated method stub
		return 3;
	}
	@Override
	public void getPropertyInfo(int index, Hashtable arg1, PropertyInfo info) {
		switch (index) {
		case 0:
			info.type = MarshalBase64.BYTE_ARRAY_CLASS;
			info.name = "image";
			break;
		case 1:
			info.type = PropertyInfo.INTEGER_CLASS;
			info.name = "presence";
			break;
		case 2:
			info.type = PropertyInfo.INTEGER_CLASS;
			info.name = "type";
		default:
			break;
		}
		
	}
	@Override
	public void setProperty(int index, Object value) {
		if (null == value)
			value = "";
		switch (index) {
		case 0:
			image = Base64.decode(value.toString());
			break;
		case 1:
			presence =  Integer.parseInt(value.toString());
			break;
		case 2:
			type =  Integer.parseInt(value.toString());
			break;
	
		}
		
	}

	
}
