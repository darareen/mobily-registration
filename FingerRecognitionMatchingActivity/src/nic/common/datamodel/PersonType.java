/*
 * $Author$
 * $Source$
 * $Revision$
 * $Date$
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA 
 * Phone: +1.310.615.0311 
 * 
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 */
package nic.common.datamodel;

/**
 * PersonType.
 *
 */
public class PersonType {
	public static final int UNKNOWN = 0;	
	public static final int CITIZEN = 1;	
	public static final int RESIDENT = 2;	
	public static final int VISITOR = 3;
	public static final int ILLEGAL = 4;
	public static final int DEPORTEE = 5;
	public static final int PILGRIM = 6;
	public static final int MISCREANT = 7;
}