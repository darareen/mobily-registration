/*
 * Author: $
 * Source: $
 * Revision: $
 * Date: $
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 */
package nic.common.datamodel;

/**
 * IrisData
 *
 */
public class IrisData {
	public static final String IMAGER_LG_4000 = "LG4000";
	public static final String IMAGER_L1_HIIDE = "HIIDE";
	
	private byte[] rightCode;
	private byte[] rightImage;
	private byte[] leftCode;
	private byte[] leftImage;
	private String imager;
	
	public IrisData() {
		imager = IMAGER_LG_4000;
	}
	
	/**
	 * @return the rightCode
	 */
	public byte[] getRightCode() {
		return rightCode;
	}
	
	/**
	 * @param rightCode the rightCode to set
	 */
	public void setRightCode(byte[] rightCode) {
		this.rightCode = rightCode;
	}
	
	/**
	 * @return the rightImage
	 */
	public byte[] getRightImage() {
		return rightImage;
	}
	
	/**
	 * @param rightImage the rightImage to set
	 */
	public void setRightImage(byte[] rightImage) {
		this.rightImage = rightImage;
	}
	
	/**
	 * @return the leftCode
	 */
	public byte[] getLeftCode() {
		return leftCode;
	}
	
	/**
	 * @param leftCode the leftCode to set
	 */
	public void setLeftCode(byte[] leftCode) {
		this.leftCode = leftCode;
	}
	
	/**
	 * @return the leftImage
	 */
	public byte[] getLeftImage() {
		return leftImage;
	}
	
	/**
	 * @param leftImage the leftImage to set
	 */
	public void setLeftImage(byte[] leftImage) {
		this.leftImage = leftImage;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getImager() {
		if (imager == null || imager.trim().length() == 0) {
			return IMAGER_LG_4000;
		}
		
		return imager;
	}

	/**
	 * 
	 * @param imager
	 */
	public void setImager(String imager) {
		this.imager = imager;
	}
	
	/**
	 * 
	 * @return
	 */
	public static int getIrisCount(IrisData irisData) {
		int irisCount = 0;
		
		if (irisData != null && irisData.getRightImage() != null && irisData.getRightImage().length > 0) {
			irisCount++;
		}
		
		if (irisData != null && irisData.getLeftImage() != null && irisData.getLeftImage().length > 0) {
			irisCount++;
		}
		
		return irisCount;
	}
}
