package nic.common.datamodel;


public class Minutiae {

	private byte[] minutiaeValue;
	private int fingerId;

	public static final int UNKNOWN = 0x00;
	public static final int RIGHT_THUMB = 0x01;
	public static final int RIGHT_INDEX = 0x02;
	public static final int RIGHT_MIDDLE = 0x03;
	public static final int RIGHT_RING = 0x04;
	public static final int RIGHT_PINKY = 0x05;
	public static final int LEFT_THUMB = 0x06;
	public static final int LEFT_INDEX = 0x07;
	public static final int LEFT_MIDDLE = 0x08;
	public static final int LEFT_RING = 0x09;
	public static final int LEFT_PINKY = 0x0A;

	/**
	 * Convert finger types defined in FingerType to finger Id
	 * used for Minutiae objects. 
	 * @param fingerType finger type
	 * @return finger id
	 */
	public static int fingerTypeToId(int fingerType) {
		int fingerId = -1;
		switch (fingerType) {
		case FingerType.RIGHT_THUMB_ROLL:
			fingerId = RIGHT_THUMB;
			break;
		case FingerType.RIGHT_INDEX_ROLL:
			fingerId = RIGHT_INDEX;
			break;
		case FingerType.RIGHT_MIDDLE_ROLL:
			fingerId = RIGHT_MIDDLE;
			break;
		case FingerType.RIGHT_RING_ROLL:
			fingerId = RIGHT_RING;
			break;
		case FingerType.RIGHT_PINKY_ROLL:
			fingerId = RIGHT_PINKY;
			break;
		case FingerType.LEFT_THUMB_ROLL:
			fingerId = LEFT_THUMB;
			break;
		case FingerType.LEFT_INDEX_ROLL:
			fingerId = LEFT_INDEX;
			break;
		case FingerType.LEFT_MIDDLE_ROLL:
			fingerId = LEFT_MIDDLE;
			break;
		case FingerType.LEFT_RING_ROLL:
			fingerId = LEFT_RING;
			break;
		case FingerType.LEFT_PINKY_ROLL:
			fingerId = LEFT_PINKY;
			break;
		}
		return fingerId;
	}
	
	/**
	 * Convert finger id in Minutiae objects to finger types
	 * defined in FingerType. 
	 * @param fingerId finger id
	 * @return finger type
	 */
	public static int fingerIdToType(int fingerId) {
		int fingerType = -1;
		switch (fingerId) {
		case RIGHT_THUMB:
			fingerType = FingerType.RIGHT_THUMB_ROLL;
			break;
		case RIGHT_INDEX:
			fingerType = FingerType.RIGHT_INDEX_ROLL;
			break;
		case RIGHT_MIDDLE:
			fingerType = FingerType.RIGHT_MIDDLE_ROLL;
			break;
		case RIGHT_RING:
			fingerType = FingerType.RIGHT_RING_ROLL;
			break;
		case RIGHT_PINKY:
			fingerType = FingerType.RIGHT_PINKY_ROLL;
			break;
		case LEFT_THUMB:
			fingerType = FingerType.LEFT_THUMB_ROLL;
			break;
		case LEFT_INDEX:
			fingerType = FingerType.LEFT_INDEX_ROLL;
			break;
		case LEFT_MIDDLE:
			fingerType = FingerType.LEFT_MIDDLE_ROLL;
			break;
		case LEFT_RING:
			fingerType = FingerType.LEFT_RING_ROLL;
			break;
		case LEFT_PINKY:
			fingerType = FingerType.LEFT_PINKY_ROLL;
			break;
		}
		return fingerType;
	}
	
	/**
	 * @return Returns the fingerId.
	 */
	public int getFingerId() {
		return fingerId;
	}
	/**
	 * @param fingerId The fingerId to set.
	 */
	public void setFingerId(int fingerId) {
		this.fingerId = fingerId;
	}
	/**
	 * @return Returns the minutiaeValue.
	 */
	public byte[] getMinutiaeValue() {
		return minutiaeValue;
	}
	/**
	 * @param minutiaeValue The minutiaeValue to set.
	 */
	public void setMinutiaeValue(byte[] minutiaeValue) {
		this.minutiaeValue = minutiaeValue;
	}
}
