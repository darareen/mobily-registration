package nic.common.datamodel;


import nic.common.datamodel.types.biometrics.Side;


public interface MinutiaeEnrollmentData {

	// All the getters
@SuppressWarnings("unchecked")
	public Minutiae	getFirstTemplete();
@SuppressWarnings("unchecked")
	public Minutiae	getSecondTemplete();
@SuppressWarnings("unchecked")
	public FingerType	getFirstFingerType();
@SuppressWarnings("unchecked")
	public FingerType	getSecondFingerType();
@SuppressWarnings("unchecked")
	public Side	getFirstFingerSide();
@SuppressWarnings("unchecked")
	public Side	getSecondFingerSide();
@SuppressWarnings("unchecked")
	public BiometricPresence	getFirstPresence();
@SuppressWarnings("unchecked")
	public BiometricPresence	getSecondPresence();

	// All the setters
@SuppressWarnings("unchecked")
	public void setFirstTemplete(Minutiae newValue);
@SuppressWarnings("unchecked")
	public void setSecondTemplete(Minutiae newValue);
@SuppressWarnings("unchecked")
	public void setFirstFingerType(FingerType newValue);
@SuppressWarnings("unchecked")
	public void setSecondFingerType(FingerType newValue);
@SuppressWarnings("unchecked")
	public void setFirstFingerSide(Side newValue);
@SuppressWarnings("unchecked")
	public void setSecondFingerSide(Side newValue);
@SuppressWarnings("unchecked")
	public void setFirstPresence(BiometricPresence newValue);
@SuppressWarnings("unchecked")
	public void setSecondPresence(BiometricPresence newValue);

}
