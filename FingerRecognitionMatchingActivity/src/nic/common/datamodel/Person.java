/*
 * Author: Amer Abdrabbou, Nate Roe
 * Source:
 * Revision:
 * Date:
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 */
package nic.common.datamodel;

import nic.common.datamodel.exception.FingerAlreadyDefinedException;
import nic.common.datamodel.exception.FingerPresenceUndefinedException;
import nic.common.datamodel.exception.FingerUndefinedException;

/**
 * This class is a data container for any person within TE Station
 *
 * @author abdrabbou
 * @author nroe
 */
public class Person {
	private Finger [] fingers;
	//private String facialPhoto;
	private Thumbnail thumbnail;
	private MinutiaeData minutiaeData;
	private int rightFignerID;
	private byte [] rightFingerMinutiae;
	private int leftFingerID;
	private byte [] leftFingerMinutiae;
	private IrisData irisData;
	private String idExpDate;
    private FacialPhoto facialPhoto;
	private String birthGDate;
    private int    birthHDate;
    
    private String occupation;
    private String birthPlace;
	private String nationality = null;
	private String passportNumber = null;
	
    private long idNumber;
    private int idIssueDate;
	private int idExpiryDate;
    private long biometricId;
    private int gender;
    private String firstName = null;
    private String familyName = null;
    private String fatherName = null;
    private String grandFatherName = null;
    private String tribeName = null;

    private boolean isFinished;
    private boolean isPending;
    private boolean isReadyForPrinting;
    private boolean isExit;
    private boolean hasIris;
    
    private String status = null;
    
    private int enrollmentStatus;
    
    private int fingerVerified;
    public static final int FINGER_NOT_VERIFY=0;
    public static final int FINGER_VERIFIED=1;
    public static final int FINGER_SKIPPED=2;

    /**
     * PersonType 
     */
    private int personType;
    
    /**
     * Create a new, empty Person.
     *
     */
    public Person() {
        this(0);
    }
    
    /**
     * Create Person.
     *
     */
    public Person(long idNumber) {
    	this(idNumber, 0);
    }
    
    /**
     * Create Person.
     *
     */
    public Person(long idNumber, long biometricId) {
    	this.idNumber = idNumber;
    	this.biometricId = biometricId;
    	
        fingers = new Finger[23];
        birthHDate = 0;
        birthGDate = null;
        gender = GenderType.UNKNOWN;
        fingerVerified = FINGER_NOT_VERIFY;
        isExit = false;
    }

    /**
     * Returns full name.
     * @return
     */
    public String getFullName() {
		String fullName = "";
		
		if (firstName != null && firstName.length() > 0) {				
			fullName += firstName + " ";
		}
		
		if (fatherName != null && fatherName.length() > 0) {
	    	fullName += fatherName + " ";			
		}
		
		if (grandFatherName != null && grandFatherName.length() > 0) {			
			fullName += grandFatherName + " ";
		}
		
		if (tribeName != null && tribeName.length() > 0) {
			fullName += tribeName + " ";
		}
		
		if (familyName != null && familyName.length() > 0) {
			fullName += familyName + " ";
		}
	
		return fullName.trim(); 
	}
    
    /**
     * Get the date of birth.
     *
     * @return Returns the birthDate.
     */
    public int getBirthHDate() {
        return birthHDate;
    }

    public String getBirthGDate() {
        return birthGDate;
    }

    /**
     * Set the date of birth.
     *
     * @param birthDate The birthDate to set.
     */
    public void setBirthHDate(int birthDate) {
        this.birthHDate = birthDate;
    }

    public void setBirthGDate(String birthDate) {
        this.birthGDate = birthDate;
    }
    
	/**
	 * Get birth place.
	 * 
	 * @return
	 */
	public String getBirthPlace() {
		return birthPlace;
	}

	/**
	 * Set birth place.
	 * 
	 * @param birthPlace
	 */
	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}
	 /**
	 * Get Occupation.
	 * 
	 * @return
	 */
	public String getOccupation() {
		return occupation;
	}
	
	/**
	 * Set occupation.
	 * 
	 * @param birthPlace
	 */
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	/**
	 * 
	 * @return idIssueDate
	 */
	public int getIdIssueDate() {
		return idIssueDate;
	}
	
	/**
	 * 
	 * @param idIssueDate
	 */
	public void setIdIssueDate(int idIssueDate) {
		this.idIssueDate = idIssueDate;
	}
	/**
	 * 
	 * @return idExpiryDate
	 */
	public int getIdExpiryDate() {
		return idExpiryDate;
	}
	
	/**
	 * 
	 * @param idExpiryDate
	 */
	public void setIdExpiryDate(int idExpiryDate) {
		this.idExpiryDate = idExpiryDate;
	}
	
	protected int fingerTypeToIndex(int fingerType) {
		int returnVal = -1;

		switch (fingerType) {
			case FingerType.RIGHT_THUMB_ROLL:
				returnVal = 4;
				break;
			case FingerType.RIGHT_INDEX_ROLL:
				returnVal = 5;
				break;
			case FingerType.RIGHT_MIDDLE_ROLL:
				returnVal = 6;
				break;
			case FingerType.RIGHT_RING_ROLL:
				returnVal = 7;
				break;
			case FingerType.RIGHT_PINKY_ROLL:
				returnVal = 8;
				break;
    			case FingerType.LEFT_THUMB_ROLL:
				returnVal = 9;
				break;
			case FingerType.LEFT_INDEX_ROLL:
				returnVal = 10;
				break;
			case FingerType.LEFT_MIDDLE_ROLL:
				returnVal = 11;
				break;
			case FingerType.LEFT_RING_ROLL:
				returnVal = 12;
				break;
			case FingerType.LEFT_PINKY_ROLL:
				returnVal = 13;
				break;
    			case FingerType.RIGHT_THUMB_SLAP:
				returnVal = 2;
				break;
			case FingerType.LEFT_THUMB_SLAP:
				returnVal = 3;
				break;
			case FingerType.RIGHT_FOUR_SLAP:
				returnVal = 0;
				break;
			case FingerType.LEFT_FOUR_SLAP:
				returnVal = 1;
				break;
			case FingerType.TWO_THUMB_SLAP:
				returnVal = 22;
				break;
			case FingerType.RIGHT_INDEX_SLAP:
				returnVal = 14;
				break;
			case FingerType.RIGHT_MIDDLE_SLAP:
				returnVal = 15;
				break;
			case FingerType.RIGHT_RING_SLAP:
				returnVal = 16;
				break;
			case FingerType.RIGHT_PINKY_SLAP:
				returnVal = 17;
				break;
			case FingerType.LEFT_INDEX_SLAP:
				returnVal = 18;
				break;
			case FingerType.LEFT_MIDDLE_SLAP:
				returnVal = 19;
				break;
			case FingerType.LEFT_RING_SLAP:
				returnVal = 20;
				break;
			case FingerType.LEFT_PINKY_SLAP:
				returnVal = 21;
				break;

	
		}
		return returnVal;
	}


    /**
     * Get a finger of the given type.
     *
     * @param type The finger type code indicating which finger to retrieve.
     * @return Returns the designated finger or null if it is not set.
     * @throws IllegalArgumentException if the finger type code is out of range (less than {@link sa.gov.nic.common.datamodel.FingerType#FIRST_FINGER_CODE} or greater than {@link sa.gov.nic.common.datamodel.FingerType#LAST_FINGER_CODE}.)
     */
    public Finger getFinger(int type) {
	Finger returnVal = null;
	int index = fingerTypeToIndex(type);
    	if (index >= 0 && index < fingers.length) {
    		returnVal = fingers[index];
    	} else {
    		throw new IllegalArgumentException("Finger type " + type + " out of range.");
    	}

        return returnVal;
    }

    /**
     * Add a finger to the record.  Throws an exception
     * if there is an existing finger of the same type already in the record.
     * <p>
     * The {@link #addFinger(sa.gov.nic.common.datamodel.Finger)} and {@link #replaceFinger(sa.gov.nic.common.datamodel.Finger)} methods throw if used to replace
     * or add, respectively.  This is intended to ensure that objects which build Records do not
     * unintentionally replace a given finger.
     *
     * @param finger The finger to set.
     * @throws sa.gov.nic.common.datamodel.exception.FingerAlreadyDefinedException
     * @throws sa.gov.nic.common.datamodel.exception.FingerPresenceUndefinedException
     *
     * @see #replaceFinger(sa.gov.nic.common.datamodel.Finger)
     */
    public void addFinger(Finger finger) throws FingerAlreadyDefinedException, FingerPresenceUndefinedException {
        if (finger == null) {
            throw new NullPointerException("Finger is null.");
        }

        if (finger.getPresence() == FingerPresence.UNDEFINED) {
            throw new FingerPresenceUndefinedException("Finger presence is undefined.");
        }
/*
        if (fingers[fingerTypeToIndex(finger.getType())] != null) {
            throw new FingerAlreadyDefinedException("Finger of type " + finger.getType() + " already defined.");
        }
*/        

        fingers[fingerTypeToIndex(finger.getType())] = finger;
    }

    /**
     * Replace an existing finger of a given type with a new finger of the same type.  Throws an exception
     * if there is no existing finger to replace.
     * <p>
     * The {@link #addFinger(sa.gov.nic.common.datamodel.Finger)} and {@link #replaceFinger(sa.gov.nic.common.datamodel.Finger)} methods throw if used to replace
     * or add, respectively.  This is intended to ensure that objects which build Records do not
     * unintentionally replace a given finger.
     *
     * @param finger The finger to set.
     * @throws sa.gov.nic.common.datamodel.exception.FingerPresenceUndefinedException if the finger's presence code has not been set.
     * @throws sa.gov.nic.common.datamodel.exception.FingerUndefinedException if there is not extant finger of the given finger's type to replace.
     *
     * @see #addFinger(sa.gov.nic.common.datamodel.Finger)
     */
    public void replaceFinger(Finger finger) throws FingerPresenceUndefinedException, FingerUndefinedException {
        if (finger == null) {
            throw new NullPointerException("Finger is null.");
        }

        if (finger.getPresence() == FingerPresence.UNDEFINED) {
            throw new FingerPresenceUndefinedException("Finger presence is undefined.");
        }

        if (fingers[fingerTypeToIndex(finger.getType())] == null) {
            throw new FingerUndefinedException("Finger of type " + finger.getType() + " already defined.");
        }

        fingers[fingerTypeToIndex(finger.getType())] = finger;
    }

    /**
     * Get the gender.
     *
     * @return Returns the gender.
     */
    public int getGender() {
        return gender;
    }

    /**
     * Set the sex of the enrollee.
     *
     * @param gender The sex to set.
     * @throws IllegalArgumentException if the sex is out of range ({@link sa.gov.nic.common.datamodel.GenderType#FIRST_GENDER_TYPE} to {@link sa.gov.nic.common.datamodel.GenderType#LAST_GENDER_TYPE}.)
     * @see sa.gov.nic.common.datamodel.GenderType
     */
    public void setGender(int gender) {
		this.gender = gender;
    }

    /**
     * @return Returns the familyName.
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * @param familyName The familyName to set.
     */
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    /**
     * @return Returns the fatherName.
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * @param fatherName The fatherName to set.
     */
    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    /**
     * @return Returns the firstName.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The firstName to set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return Returns the grandFatherName.
     */
    public String getGrandFatherName() {
        return grandFatherName;
    }

    /**
     * @param grandFatherName The grandFatherName to set.
     */
    public void setGrandFatherName(String grandFatherName) {
        this.grandFatherName = grandFatherName;
    }

    /**
     * @return Returns the idNumber.
     */
    public long getIdNumber() {
        return idNumber;
    }

    /**
     * @param idNumber The idNumber to set.
     */
    public void setIdNumber(long personId) {
        this.idNumber = personId;
    }
    
    /**
     * @return Returns the biometricId.
     */
    public long getBiometricId() {
        return biometricId;
    }

    /**
     * @param biometricId The biometricId to set.
     */
    public void setBiometricId(long biometricId) {
        this.biometricId = biometricId;
    }

    /**
     * @return Returns the firstMinutiae.
     */
    public MinutiaeData getMinutiaeData() {
        return minutiaeData;
    }

    /**
     * @param firstMinutiae The firstMinutiae to set.
     */
    public void setMinutiaeData(MinutiaeData minutiaeData) {
        this.minutiaeData = minutiaeData;
    }
    
    /**
     * @return Returns the irisData.
     */
    public IrisData getIrisData() {
        return irisData;
    }

    /**
     * @param irisData The irisData to set.
     */
    public void setIrisData(IrisData irisData) {
        this.irisData = irisData;
    }    

    /**
     * @return Returns the facialPhoto.
     */
    public FacialPhoto getFacialPhoto() {
        return facialPhoto;
    }

    /**
     * @param facialPhoto The facialPhoto to set.
     */
    public void setFacialPhoto(FacialPhoto facialPhoto) {
        this.facialPhoto = facialPhoto;
    }
    
    /**
     * @return Returns the thumbnail.
     */
    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    /**
     * @param thumbnail The thumbnail to set.
     */
    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    /**
     * This attribute will determine whether the enrollee is a new enrollee or not.
     * @return: whether the enrollee is a new enrollee or not
     */


    public boolean getIsFinished() {
        return isFinished;
    }

    /**
     * This attribute will determine whether the enrollee is a new enrollee or not.
     * @param isFinished
     */
    public void setIsFinished(boolean isFinished) {
        this.isFinished = isFinished;
    }
    
    /**
     * This attribute will determine whether the enrollee is under processing or not.
     * @return: whether the enrollee is under processing or not
     */
    public boolean getIsPending() {
        return isPending;
    }

    /**
     * This attribute will determine whether the enrollee is under processing or not.
     * @param isPending
     */
    public void setIsPending(boolean isPending) {
        this.isPending = isPending;
    }
    
	/**
	 * @return Returns the fingers.
	 */
	public Finger[] getFingers() {
		return fingers;
	}
	/**
	 * @param fingers The fingers to set.
	 */
	public void setFingers(Finger[] fingers) {
		this.fingers = fingers;
	}    
  
	public String toString(){
		String out = "Person [";
		out+= "idNumber : "+idNumber+";";
		out+= "biometricId : "+biometricId+";";		
		out+= "firstName : "+firstName+";";
		out+= "familyName : "+familyName+";";
		out+= "fatherName : "+fatherName+";";
		out+= "grandFatherName : "+grandFatherName+";";
		out+= "tribeName : "+tribeName+";";
		out+= "gender : "+gender+";";
		out+= "birthGDate : "+birthGDate+";";
		out+= "birthHDate : "+birthHDate+";";
		out+= "isFinished : "+isFinished+";\n";
		out+= "isPending : "+isPending+";\n";		
		out+= "isReadyForPrinting : "+isReadyForPrinting+";\n";
		out+= minutiaeData+"\n";		
		out+= irisData+"\n";		
		for (int i = 0; i < fingers.length; i++){
			out += fingers[i]+"\n";
		}
		return out;		
	}

	public String getTribeName() {
		return tribeName;
	}

	public void setTribeName(String tribeName) {
		this.tribeName = tribeName;
	}

	public boolean isReadyForPrinting() {
		return isReadyForPrinting;
	}

	public void setReadyForPrinting(boolean isReadyForPrinting) {
		this.isReadyForPrinting = isReadyForPrinting;
	}

	/**
	 * @return the fingerVerified
	 */
	public int getFingerVerified() {
		return fingerVerified;
	}

	/**
	 * @param fingerVerified the fingerVerified to set
	 */
	public void setFingerVerified(int fingerVerified) {
		this.fingerVerified = fingerVerified;
	}

	/**
	 * @return the personType
	 */
	public int getPersonType() {
		return personType;
	}

	/**
	 * @param personType the personType to set
	 */
	public void setPersonType(int personType) {
		this.personType = personType;
	}

	/**
	 * @return the isExit
	 */
	public boolean isExit() {
		return isExit;
	}

	/**
	 * @param isExit the isExit to set
	 */
	public void setExit(boolean isExit) {
		this.isExit = isExit;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getNationality() {
		return nationality;
	}
	
	/**
	 * 
	 * @param nationality
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPassportNumber() {
		return passportNumber;
	}

	/**
	 * 
	 * @param passortNumber
	 */
	public void setPassportNumber(String passortNumber) {
		this.passportNumber = passortNumber;
	}

	/**
	 * @return the hasIris
	 */
	public boolean isHasIris() {
		return hasIris;
	}

	/**
	 * @param hasIris the hasIris to set
	 */
	public void setHasIris(boolean hasIris) {
		this.hasIris = hasIris;
	}

	/**
	 * 
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the enrollmentStatus
	 */
	public int getEnrollmentStatus() {
		return enrollmentStatus;
	}

	/**
	 * @param enrollmentStatus the enrollmentStatus to set
	 */
	public void setEnrollmentStatus(int enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public int getRightFignerID() {
		return rightFignerID;
	}

	public void setRightFignerID(int rightFignerID) {
		this.rightFignerID = rightFignerID;
	}

	public byte[] getRightFingerMinutiae() {
		return rightFingerMinutiae;
	}

	public void setRightFingerMinutiae(byte[] rightFingerMinutiae) {
		this.rightFingerMinutiae = rightFingerMinutiae;
	}
	
	public int getLeftFingerID() {
		return leftFingerID;
	}

	public void setLeftFingerID(int leftFingerID) {
		this.leftFingerID = leftFingerID;
	}

	public byte[] getLeftFingerMinutiae() {
		return leftFingerMinutiae;
	}

	public void setLeftFingerMinutiae(byte[] leftFingerMinutiae) {
		this.leftFingerMinutiae = leftFingerMinutiae;
	}

	public String getIdExpDate() {
		return idExpDate;
	}

	public void setIdExpDate(String idExpDate) {
		this.idExpDate = idExpDate;
	}

}
