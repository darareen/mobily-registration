/*
 * Author: Nate roe, Amer Abdrabbou
 * Source:
 * Revision:
 * Date:
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 * $Log: FingerType.java,v $
 * Revision 1.14  2006/06/28 07:52:39  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.1  2006/06/28 06:49:31  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.12  2006/04/05 13:20:04  kwu
 * add getCorrespondingSlap and getCorrespondingRoll method
 *
 * Revision 1.11  2006/03/22 15:22:10  akayali
 * added the strings for LEFT_THUMB_SEG_SLAP & RIGHT_THUMB_SEG_SLAP in the fingerTypeToString()
 *
 * Revision 1.10  2006/01/10 14:27:48  nroe
 * Fix segmented thumb finger types
 *
 * Revision 1.9  2005/12/27 11:44:00  akatebah
 * Updated file from Airport Pilot
 *
 * Revision 1.8.2.4  2005/12/14 06:41:54  kwu
 * *** empty log message ***
 *
 * Revision 1.8.2.3  2005/11/29 06:08:34  kwu
 * *** empty log message ***
 *
 * Revision 1.8.2.2  2005/11/21 08:15:20  yspoon
 * *** empty log message ***
 *
 * Revision 1.8.2.1  2005/11/21 07:10:11  kwu
 * *** empty log message ***
 *
 * Revision 1.8  2005/08/28 17:46:18  akayali
 * The indexes of the the finger types is corrected to start with 0 instead of 1
 *
 * Revision 1.7  2005/08/17 07:43:40  nroe
 * Add single-finger slap types again.
 *
 * Revision 1.6  2005/08/15 11:22:12  nroe
 * Roll back to java 1.4
 *
 * Revision 1.5  2005/08/15 06:49:30  nroe
 * Add various integer interpretations of these enumerated types
 *
 * Revision 1.4  2005/08/13 07:00:32  nroe
 * change package name to correspond with repository changes
 *
 * Revision 1.3  2005/08/13 05:56:04  nroe
 * add segmented slap finger types for use with jsequence
 *
 * Revision 1.2  2005/08/09 06:48:41  nroe
 * Change enumerations to the new enum type, modify other classes to work with these new enumerations
 *
 * Revision 1.1  2005/08/07 12:32:57  aabdrabbou
 * Take out dbbeans in a separate project
 *
 *
 */
package nic.common.datamodel;


/**
 * An enumeration of the types of finger print images.
 * <p>
 * <table>
 *   <tr>
 *     <th>Value</th>
 *     <th>Description</th>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_THUMB_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_INDEX_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_MIDDLE_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_RING_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_PINKY_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_THUMB_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_INDEX_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_MIDDLE_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_RING_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_PINKY_ROLL</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_THUMB_SLAP</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_FOUR_SLAP</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_THUMB_SLAP</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_FOUR_SLAP</td>
 *     <td></td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_INDEX_SLAP</td>
 *     <td>Indicates the right index finger segmented in or from a four finger slap</td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_MIDDLE_SLAP</td>
 *     <td>Indicates the right middle finger segmented in or from a four finger slap</td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_RING_SLAP</td>
 *     <td>Indicates the right ring finger segmented in or from a four finger slap</td>
 *   </tr>
 *   <tr>
 *     <td>RIGHT_PINKY_SLAP</td>
 *     <td>Indicates the right pinky finger segmented in or from a four finger slap</td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_INDEX_SLAP</td>
 *     <td>Indicates the left index finger segmented in or from a four finger slap</td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_MIDDLE_SLAP</td>
 *     <td>Indicates the left middle finger segmented in or from a four finger slap</td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_RING_SLAP</td>
 *     <td>Indicates the left ring finger segmented in or from a four finger slap</td>
 *   </tr>
 *   <tr>
 *     <td>LEFT_PINKY_SLAP</td>
 *     <td>Indicates the left pinky finger segmented in or from a four finger slap</td>
 *   </tr>
 *   <tr>
 *     <td>TWO_THUMB_SLAP</td>
 *     <td>Indicates the two thumb slap image</td>
 *   </tr>
 * </table>
 *
 * @author nroe
 * @author abdrabbou
 */
public class FingerType {

	/**
	 * Finger type code: Rolled print of the right thumb.
	 */
	public static final int RIGHT_THUMB_ROLL  = 4;
	/**
	 * Finger type code: Rolled print of the right index finger.
	 */
	public static final int RIGHT_INDEX_ROLL  = 5;
	/**
	 * Finger type code: Rolled print of the right middle finger.
	 */
	public static final int RIGHT_MIDDLE_ROLL = 6;
	/**
	 * Finger type code: Rolled print of the right ring finger.
	 */
	public static final int RIGHT_RING_ROLL   = 7;
	/**
	 * Finger type code: Rolled print of the right pinky finger.
	 */
	public static final int RIGHT_PINKY_ROLL  = 8;

	/**
	 * Finger type code: Rolled print of the left thumb.
	 */
	public static final int LEFT_THUMB_ROLL  = 9;
	/**
	 * Finger type code: Rolled print of the left index finger.
	 */
	public static final int LEFT_INDEX_ROLL  = 10;
	/**
	 * Finger type code: Rolled print of the left middle finger.
	 */
	public static final int LEFT_MIDDLE_ROLL = 11;
	/**
	 * Finger type code: Rolled print of the left ring finger.
	 */
	public static final int LEFT_RING_ROLL   = 12;
	/**
	 * Finger type code: Rolled print of the left pinky finger.
	 */
	public static final int LEFT_PINKY_ROLL  = 13;


	public static final int TWO_THUMB_SLAP =22;
	
	/**
	 * Finger type code: Slap print of the rightt thumb.
	 */
	public static final int RIGHT_THUMB_SLAP = 2;
	/**
	 * Finger type code: Slap print of the right four fingers.
	 */
	public static final int RIGHT_FOUR_SLAP  = 0;
	/**
	 * Finger type code: Slap print of the left thumb.
	 */
	public static final int LEFT_THUMB_SLAP  = 3;
	/**
	 * Finger type code: Slap print of the left four fingers.
	 */
	public static final int LEFT_FOUR_SLAP   = 1;

	public static final int RIGHT_INDEX_SLAP  = 14;
	public static final int RIGHT_MIDDLE_SLAP = 15;
	public static final int RIGHT_RING_SLAP   = 16;
	public static final int RIGHT_PINKY_SLAP  = 17;

	public static final int LEFT_INDEX_SLAP  = 18;
	public static final int LEFT_MIDDLE_SLAP = 19;
	public static final int LEFT_RING_SLAP   = 20;
	public static final int LEFT_PINKY_SLAP  = 21;
	
	/**
	 * Finger type code: segmented thumb slap
	 */
	public static final int LEFT_THUMB_SEG_SLAP  = 33;

	/**
	 * Finger type code: segmented thumb slap
	 */
	public static final int RIGHT_THUMB_SEG_SLAP  = 32;


	/**
	 * Indicates the first finger type code.  Finger codes are consecutive from
	 * FIRST_FINGER_CODE to LAST_FINGER_CODE.  There are FINGER_CODE_COUNT finger
	 * codes.
	 */
	public static final int FIRST_FINGER_CODE = RIGHT_FOUR_SLAP;

	/**
	 * Indicates the last finger type code.  Finger codes are consecutive from
	 * FIRST_FINGER_CODE to LAST_FINGER_CODE.  There are FINGER_CODE_COUNT finger
	 * codes.
	 */
	public static final int LAST_FINGER_CODE  = TWO_THUMB_SLAP;

	/**
	 * Indicates the total number of finger type codes.  Finger codes are consecutive from
	 * FIRST_FINGER_CODE to LAST_FINGER_CODE.  There are FINGER_CODE_COUNT finger
	 * codes.
	 */
	public static final int FINGER_CODE_COUNT  = LAST_FINGER_CODE - FIRST_FINGER_CODE + 1;

	/**
	 * Private constructor prevents instanciation: no instance of this class should ever be created
	 * as its members are all static.
	 *
	 */
	private FingerType() {
	}

	/**
	 * Return the String representation of the name of the given finger type code, or "Undefined" if none is found.
	 *
	 * @param fingerType the finger type code
	 * @return the String representation of the name of the given finger type code, or "Undefined" if none is found.
	 */
	public static String fingerTypeToString(int fingerType) {
		String returnVal = "Undefined";

		switch (fingerType) {
			case RIGHT_THUMB_ROLL:
				returnVal = "RIGHT_THUMB_ROLL";
				break;
			case RIGHT_INDEX_ROLL:
				returnVal = "RIGHT_INDEX_ROLL";
				break;
			case RIGHT_MIDDLE_ROLL:
				returnVal = "RIGHT_MIDDLE_ROLL";
				break;
			case RIGHT_RING_ROLL:
				returnVal = "RIGHT_RING_ROLL";
				break;
			case RIGHT_PINKY_ROLL:
				returnVal = "RIGHT_PINKY_ROLL";
				break;

			case LEFT_THUMB_ROLL:
				returnVal = "LEFT_THUMB_ROLL";
				break;
			case LEFT_INDEX_ROLL:
				returnVal = "LEFT_INDEX_ROLL";
				break;
			case LEFT_MIDDLE_ROLL:
				returnVal = "LEFT_MIDDLE_ROLL";
				break;
			case LEFT_RING_ROLL:
				returnVal = "LEFT_RING_ROLL";
				break;
			case LEFT_PINKY_ROLL:
				returnVal = "LEFT_PINKY_ROLL";
				break;

			case RIGHT_THUMB_SLAP:
				returnVal = "RIGHT_THUMB_SLAP";
				break;
			case RIGHT_THUMB_SEG_SLAP:
				returnVal = "RIGHT_THUMB_SEG_SLAP";
				break;
			case RIGHT_FOUR_SLAP:
				returnVal = "RIGHT_FOUR_SLAP";
				break;
			case LEFT_THUMB_SLAP:
				returnVal = "LEFT_THUMB_SLAP";
				break;
			case LEFT_THUMB_SEG_SLAP:
				returnVal = "LEFT_THUMB_SEG_SLAP";
				break;
			case LEFT_FOUR_SLAP:
				returnVal = "LEFT_FOUR_SLAP";
				break;
			case TWO_THUMB_SLAP:
				returnVal = "TWO_THUMB_SLAP";
				break;
			case RIGHT_INDEX_SLAP :
				returnVal = "RIGHT_INDEX_SLAP";
				break;
			case RIGHT_MIDDLE_SLAP :
				returnVal = "RIGHT_MIDDLE_SLAP";
				break;
			case RIGHT_RING_SLAP :
				returnVal = "RIGHT_RING_SLAP";
				break;
			case RIGHT_PINKY_SLAP :
				returnVal = "RIGHT_PINKY_SLAP";
				break;
			case LEFT_INDEX_SLAP :
				returnVal = "LEFT_INDEX_SLAP";
				break;
			case LEFT_MIDDLE_SLAP :
				returnVal = "LEFT_MIDDLE_SLAP";
				break;
			case LEFT_RING_SLAP :
				returnVal = "LEFT_RING_SLAP";
				break;
			case LEFT_PINKY_SLAP :
				returnVal = "LEFT_PINKY_SLAP";
				break;
		}

		return returnVal;
	}
	
	static public int getNumOfFinger(int fingerType){
		int returnVal = 1;
		
		switch (fingerType) {
		case RIGHT_FOUR_SLAP:
			returnVal = 4;
			break;
		case LEFT_FOUR_SLAP:
			returnVal = 4;
			break;
		case TWO_THUMB_SLAP:
			returnVal = 2;
			break;
		default:
			returnVal = 1;
		}
		return returnVal;	
	}
		
	
	static public boolean isValidValue(int fingerType){
		if (fingerType < FIRST_FINGER_CODE || fingerType > LAST_FINGER_CODE){
			return false;
		} else {
			return true;
		}
	}
	
	static public int getCorrespondingSlap(int fingerType){
		int returnVal = fingerType;
		
		switch (fingerType) {
		case RIGHT_THUMB_ROLL:
			returnVal = RIGHT_THUMB_SLAP;
			break;
		case RIGHT_INDEX_ROLL:
			returnVal = RIGHT_INDEX_SLAP;
			break;
		case RIGHT_MIDDLE_ROLL:
			returnVal = RIGHT_MIDDLE_SLAP;
			break;
		case RIGHT_RING_ROLL:
			returnVal = RIGHT_RING_SLAP;
			break;
		case RIGHT_PINKY_ROLL:
			returnVal = RIGHT_PINKY_SLAP;
			break;
		case LEFT_THUMB_ROLL:
			returnVal = LEFT_THUMB_SLAP;
			break;
		case LEFT_INDEX_ROLL:
			returnVal = LEFT_INDEX_SLAP;
			break;
		case LEFT_MIDDLE_ROLL:
			returnVal = LEFT_MIDDLE_SLAP;
			break;
		case LEFT_RING_ROLL:
			returnVal = LEFT_RING_SLAP;
			break;
		case LEFT_PINKY_ROLL:
			returnVal = LEFT_PINKY_SLAP;
			break;		
		}
		return returnVal;	
	}
	
	static public int getCorrespondingRoll(int fingerType){
		int returnVal = fingerType;
		
		switch (fingerType) {
		case RIGHT_THUMB_SLAP:
			returnVal = RIGHT_THUMB_ROLL;
			break;
		case RIGHT_INDEX_SLAP:
			returnVal = RIGHT_INDEX_ROLL;
			break;
		case RIGHT_MIDDLE_SLAP:
			returnVal = RIGHT_MIDDLE_ROLL;
			break;
		case RIGHT_RING_SLAP:
			returnVal = RIGHT_RING_ROLL;
			break;
		case RIGHT_PINKY_SLAP:
			returnVal = RIGHT_PINKY_ROLL;
			break;
		case LEFT_THUMB_SLAP:
			returnVal = LEFT_THUMB_ROLL;
			break;
		case LEFT_INDEX_SLAP:
			returnVal = LEFT_INDEX_ROLL;
			break;
		case LEFT_MIDDLE_SLAP:
			returnVal = LEFT_MIDDLE_ROLL;
			break;
		case LEFT_RING_SLAP:
			returnVal = LEFT_RING_ROLL;
			break;
		case LEFT_PINKY_SLAP:
			returnVal = LEFT_PINKY_ROLL;
			break;		
		}
		return returnVal;	
	}
}

