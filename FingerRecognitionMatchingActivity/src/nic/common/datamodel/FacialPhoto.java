/*
 * Author: Amer Abdrabbou
 * Source:
 * Revision:
 * Date:
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 * Log:
 *
 */
package nic.common.datamodel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * @author abdrabbou
 *
 */
public class FacialPhoto {
	public static long ONE_SECOND 	= 1000L;
	public static long ONE_HOUR 	= 60 * 60 * ONE_SECOND;
	public static long ONE_DAY 		= 24 * ONE_HOUR;
	public static long ONE_YEAR 	= (long) 365.25 * ONE_DAY;
	public static long FIVE_YEARS 	= 5 * ONE_YEAR;
	public static long TEN_YEARS  	= 10 * ONE_YEAR;
	public static long THIRTY_YEARS = 30 * ONE_YEAR;
	public static long SIXTY_YEARS  = 60 * ONE_YEAR;
	public static long OPEN         = 32503582800000l;
	
	public static long REPLACEMENT_MARGIN = ONE_YEAR;
		
	private int width = -1;
	private int height = -1;
	private byte[] facialPhoto;
	private long creationTime = -1;
	private long expirationTime = -1;
	private boolean newPhoto = true; 
	
	/**
	 * @return the oldPhoto
	 */
	public boolean isNewPhoto() {
		return newPhoto;
	}

	/**
	 * @param oldPhoto the oldPhoto to set
	 */
	public void setNewPhoto(boolean oldPhoto) {
		this.newPhoto = oldPhoto;
	}

	public byte[] getFacialPhoto() {
		return facialPhoto;
	}
	
	public void setFacialPhoto(byte[] facialPhoto) {
		this.facialPhoto = facialPhoto;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public long getCreationTime() {
		return creationTime;
	}
	
	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}
	
	public long getExpirationTime() {
		return expirationTime;
	}
	
	public void setExpirationTime(long expirationTime) {
		this.expirationTime = expirationTime;
	}
	
	public static Date computeExpirationDate(Date dateOfBirth, long creationTS){
		Calendar expiration = Calendar.getInstance();
		long age = creationTS - dateOfBirth.getTime(); 
		
		if (age > SIXTY_YEARS) {
			expiration.setTimeInMillis(OPEN);
		} else if (age >= THIRTY_YEARS) {
			expiration.setTimeInMillis(creationTS);
			expiration.add(Calendar.YEAR, 10);
		} else {
			expiration.setTimeInMillis(creationTS);
			expiration.add(Calendar.YEAR, 5);
		}
		
		return expiration.getTime();	
	}
	
	public static long computeExpirationTime(String dateOfBirth, long creationTS) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("MMddyyyy");
		Date dob = format.parse(dateOfBirth);
		
		return computeExpirationDate(dob, creationTS).getTime();
	}
	
	public boolean goodForReplacement()
	{
		return getExpirationTime() - System.currentTimeMillis()<=REPLACEMENT_MARGIN;
	}
}
