/*
 * Author: Amer Abdrabbou
 * Source:
 * Revision:
 * Date:
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 * Log:
 *
 */
package nic.common.datamodel;

/**
 * @author abdrabbou
 *
 */
public class MinutiaeData {

	private String creationTS;
	private Minutiae firstMinutiae;
	private Minutiae secondMinutiae;

	public String getCreationTS() {
		return creationTS;
	}
	public void setCreationTS(String creationTS) {
		this.creationTS = creationTS;
	}
	public Minutiae getFirstMinutiae() {
		return firstMinutiae;
	}
	public void setFirstMinutiae(Minutiae firstMinutiae) {
		this.firstMinutiae = firstMinutiae;
	}
	public Minutiae getSecondMinutiae() {
		return secondMinutiae;
	}
	public void setSecondMinutiae(Minutiae secondMinutiae) {
		this.secondMinutiae = secondMinutiae;
	}
}
