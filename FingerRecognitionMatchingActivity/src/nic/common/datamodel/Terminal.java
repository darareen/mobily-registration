/*
 * Author: Ayman Katebah
 * Source:
 * Revision:
 * Date:
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 */

package nic.common.datamodel;

/**
 * This class is a data container for Terminal information
 *
 * @author akatebah
 */
public class Terminal {
	
	private String ip;
	private String locationId;
	private String locationName;	

	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	
}
