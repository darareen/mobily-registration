/*
 * Author: nroe
 * Source:
 * Revision:
 * Date:
 *
 * Copyright (c) 2005 Computer Sciences Corporation, Inc. (CSC)
 * Corporate Headquarters
 * 2100 East Grand Avenue
 * El Segundo, CA 90245 USA
 * Phone: +1.310.615.0311
 *
 * All Rights Reserved
 *
 * This software is the confidential and propietary information of
 * CSC, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with CSC.
 *
 *
 * $Log: GenderType.java,v $
 * Revision 1.11  2006/09/05 06:24:26  akayali
 * changed the values to match codes of the genders in the database for (male and female)
 *
 * Revision 1.10  2006/06/28 07:52:39  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.1  2006/06/28 06:49:32  aabdrabbou
 * *** empty log message ***
 *
 * Revision 1.8  2006/04/04 06:54:05  kwu
 * some sex string code(M,F,X) in gender type
 *
 * Revision 1.7  2006/04/03 11:30:42  kwu
 * complete description about gender type
 *
 * Revision 1.6  2006/01/22 15:21:14  yspoon
 * Remarked unuse gender type
 *
 * Revision 1.5  2006/01/22 15:20:26  yspoon
 * *** empty log message ***
 *
 * Revision 1.4  2005/08/15 11:22:12  nroe
 * Roll back to java 1.4
 *
 * Revision 1.3  2005/08/13 07:00:32  nroe
 * change package name to correspond with repository changes
 *
 * Revision 1.2  2005/08/09 06:48:41  nroe
 * Change enumerations to the new enum type, modify other classes to work with these new enumerations
 *
 * Revision 1.1  2005/08/07 12:32:57  aabdrabbou
 * Take out dbbeans in a separate project
 *
 *
 */
package nic.common.datamodel;


/**
 * This interface presents the sex types allowed by EFTS 7.0
 * <p>
 * <table>
 *   <tr>
 *     <th>Value</th>
 *     <th>Description</th>
 *   </tr>
 *   <tr>
 *     <td>FEMALE</td>
 *     <td>This person is female</td>
 *   </tr>
 *   <tr>
 *     <td>MALE</td>
 *     <td>This person is male</td>
 *   </tr>
 *   <tr>
 *     <td>FEMALE_IMPERSONATOR</td>
 *     <td>This person is a male immitating a female</td>
 *   </tr>
 *   <tr>
 *     <td>MALE_IMPERSONATOR</td>
 *     <td>This person is a femal immitating a male</td>
 *   </tr>
 *   <tr>
 *     <td>MALE_NAME</td>
 *     <td>Sex unknown; name is male</td>
 *   </tr>
 *   <tr>
 *     <td>FEMALE_NAME</td>
 *     <td>Sex unknown; name is female</td>
 *   </tr>
 *   <tr>
 *     <td>UNKNOWN</td>
 *     <td>Sex unknown</td>
 *   </tr>
 * </table>
 *
 *
 * XXX EFTS 7.0 is _not_ our governing spec.  Therefore these types must be checked against the NEC documents to ensure that the values here enumerated match those specified.
 *
 * @author nroe
 */
public class GenderType {
	/**
	 * Gender unknown.
	 */
	public static final int UNKNOWN             = 0;
	/**
	 * Male
	 */
	public static final int MALE                = 1;
	/**
	 * Female
	 */
	public static final int FEMALE              = 2;
	/**
	 * Male impersonating a female.
	 */
	//public static final int FEMALE_IMPERSONATOR = 2;
	/**
	 * Female impersonating a male
	 */
	//public static final int MALE_IMPERSONATOR   = 3;
	/**
	 * Male name, no gender given.
	 */
	//public static final int MALE_NAME           = 4;
	/**
	 * Female name, no gender given.
	 */
	//public static final int FEMALE_NAME         = 5;


	/**
	 * The value of the first valid sex type code.
	 * <p>
	 * Sex type codes are consecutive, so valid codes are greater than or equal to
	 * FIRST_GENDER_TYPE and less than or equal to LAST_GENDER_TYPE.
	 */
	public static final int FIRST_GENDER_TYPE = FEMALE;

	/**
	 * The value of the last valid sex type code.
	 * <p>
	 * Sex type codes are consecutive, so valid codes are greater than or equal to
	 * FIRST_GENDER_TYPE and less than or equal to LAST_GENDER_TYPE.
	 */
	public static final int LAST_GENDER_TYPE  = UNKNOWN;

	/**
	 * Return the associated EFTS code for the given sex type.
	 */
	public static String genderTypeToStringCode(int genderType) {
		String returnVal = "Undefined";
		switch (genderType) {
			case FEMALE:
				returnVal = "F";
				break;
			case MALE:
				returnVal = "M";
				break;
			/*
			case FEMALE_IMPERSONATOR:
				returnVal = "N";
				break;
			case MALE_IMPERSONATOR:
				returnVal = "G";
				break;
			case MALE_NAME:
				returnVal = "Y";
				break;
			case FEMALE_NAME:
				returnVal = "Z";
				break;
			*/
			
			case UNKNOWN:
				returnVal = "X";
				break;
		}

		return returnVal;
	}
}
