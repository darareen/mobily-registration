package nic.common.datamodel.types.biometrics;
public enum BiometricPresence {
	PRESENT,
	AMPUTATED,
	UNAVAILABLE,
	ABSENT_BY_POLICY;

public static String stringValue(BiometricPresence value) {
switch(value) {
	case PRESENT: return "present";
	case AMPUTATED: return "amputated";
	case UNAVAILABLE: return "unavailable";
	case ABSENT_BY_POLICY: return "absent-by-policy";
default: return "unknown value for type BiometricPresence";
}
}
public static BiometricPresence value(String value) {
	if(value.equals("present"))  return PRESENT;
	if(value.equals("amputated"))  return AMPUTATED;
	if(value.equals("unavailable"))  return UNAVAILABLE;
	if(value.equals("absent-by-policy"))  return ABSENT_BY_POLICY;
return null;}
public static int intValue(BiometricPresence value) {
switch(value) {
	case PRESENT: return 0;
	case AMPUTATED: return 1;
	case UNAVAILABLE: return 2;
	case ABSENT_BY_POLICY: return 3;
default: return -1;
}
}
public static BiometricPresence value(int value) {
switch(value) {
	case 0: return PRESENT;
	case 1: return AMPUTATED;
	case 2: return UNAVAILABLE;
	case 3: return ABSENT_BY_POLICY;
default: return null;
}
}
}
