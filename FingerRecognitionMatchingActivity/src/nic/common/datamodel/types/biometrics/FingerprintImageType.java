package nic.common.datamodel.types.biometrics;
public enum FingerprintImageType {
	RIGHT_THUMB_ROLL,
	RIGHT_INDEX_ROLL,
	RIGHT_MIDDLE_ROLL,
	RIGHT_RING_ROLL,
	RIGHT_PINKY_ROLL,
	LEFT_THUMB_ROLL,
	LEFT_INDEX_ROLL,
	LEFT_MIDDLE_ROLL,
	LEFT_RING_ROLL,
	LEFT_PINKY_ROLL,
	RIGHT_THUMB_SLAP,
	LEFT_THUMB_SLAP,
	RIGHT_FOUR_SLAP,
	LEFT_FOUR_SLAP,
	RIGHT_INDEX_SLAP,
	RIGHT_MIDDLE_SLAP,
	RIGHT_RING_SLAP,
	RIGHT_PINKY_SLAP,
	LEFT_INDEX_SLAP,
	LEFT_MIDDLE_SLAP,
	LEFT_RING_SLAP,
	LEFT_PINKY_SLAP,
	TWO_THUMB_SLAP,
	RIGHT_THUMB_SEG_SLAP,
	LEFT_THUMB_SEG_SLAP;

public static String stringValue(FingerprintImageType value) {
switch(value) {
	case RIGHT_THUMB_ROLL: return "right-thumb-roll";
	case RIGHT_INDEX_ROLL: return "right-index-roll";
	case RIGHT_MIDDLE_ROLL: return "right-middle-roll";
	case RIGHT_RING_ROLL: return "right-ring-roll";
	case RIGHT_PINKY_ROLL: return "right-pinky-roll";
	case LEFT_THUMB_ROLL: return "left-thumb-roll";
	case LEFT_INDEX_ROLL: return "left-index-roll";
	case LEFT_MIDDLE_ROLL: return "left-middle-roll";
	case LEFT_RING_ROLL: return "left-ring-roll";
	case LEFT_PINKY_ROLL: return "left-pinky-roll";
	case RIGHT_THUMB_SLAP: return "right-thumb-slap";
	case LEFT_THUMB_SLAP: return "left-thumb-slap";
	case RIGHT_FOUR_SLAP: return "right-four-slap";
	case LEFT_FOUR_SLAP: return "left-four-slap";
	case RIGHT_INDEX_SLAP: return "right-index-slap";
	case RIGHT_MIDDLE_SLAP: return "right-middle-slap";
	case RIGHT_RING_SLAP: return "right-ring-slap";
	case RIGHT_PINKY_SLAP: return "right-pinky-slap";
	case LEFT_INDEX_SLAP: return "left-index-slap";
	case LEFT_MIDDLE_SLAP: return "left-middle-slap";
	case LEFT_RING_SLAP: return "left-ring-slap";
	case LEFT_PINKY_SLAP: return "left-pinky-slap";
	case TWO_THUMB_SLAP: return "two-thumb-slap";
	case RIGHT_THUMB_SEG_SLAP: return "right-thumb-seg-slap";
	case LEFT_THUMB_SEG_SLAP: return "left-thumb-seg-slap";
default: return "unknown value for type FingerprintImageType";
}
}
public static FingerprintImageType value(String value) {
	if(value.equals("right-thumb-roll"))  return RIGHT_THUMB_ROLL;
	if(value.equals("right-index-roll"))  return RIGHT_INDEX_ROLL;
	if(value.equals("right-middle-roll"))  return RIGHT_MIDDLE_ROLL;
	if(value.equals("right-ring-roll"))  return RIGHT_RING_ROLL;
	if(value.equals("right-pinky-roll"))  return RIGHT_PINKY_ROLL;
	if(value.equals("left-thumb-roll"))  return LEFT_THUMB_ROLL;
	if(value.equals("left-index-roll"))  return LEFT_INDEX_ROLL;
	if(value.equals("left-middle-roll"))  return LEFT_MIDDLE_ROLL;
	if(value.equals("left-ring-roll"))  return LEFT_RING_ROLL;
	if(value.equals("left-pinky-roll"))  return LEFT_PINKY_ROLL;
	if(value.equals("right-thumb-slap"))  return RIGHT_THUMB_SLAP;
	if(value.equals("left-thumb-slap"))  return LEFT_THUMB_SLAP;
	if(value.equals("right-four-slap"))  return RIGHT_FOUR_SLAP;
	if(value.equals("left-four-slap"))  return LEFT_FOUR_SLAP;
	if(value.equals("right-index-slap"))  return RIGHT_INDEX_SLAP;
	if(value.equals("right-middle-slap"))  return RIGHT_MIDDLE_SLAP;
	if(value.equals("right-ring-slap"))  return RIGHT_RING_SLAP;
	if(value.equals("right-pinky-slap"))  return RIGHT_PINKY_SLAP;
	if(value.equals("left-index-slap"))  return LEFT_INDEX_SLAP;
	if(value.equals("left-middle-slap"))  return LEFT_MIDDLE_SLAP;
	if(value.equals("left-ring-slap"))  return LEFT_RING_SLAP;
	if(value.equals("left-pinky-slap"))  return LEFT_PINKY_SLAP;
	if(value.equals("two-thumb-slap"))  return TWO_THUMB_SLAP;
	if(value.equals("right-thumb-seg-slap"))  return RIGHT_THUMB_SEG_SLAP;
	if(value.equals("left-thumb-seg-slap"))  return LEFT_THUMB_SEG_SLAP;
return null;}
public static int intValue(FingerprintImageType value) {
switch(value) {
	case RIGHT_THUMB_ROLL: return 1;
	case RIGHT_INDEX_ROLL: return 2;
	case RIGHT_MIDDLE_ROLL: return 3;
	case RIGHT_RING_ROLL: return 4;
	case RIGHT_PINKY_ROLL: return 5;
	case LEFT_THUMB_ROLL: return 6;
	case LEFT_INDEX_ROLL: return 7;
	case LEFT_MIDDLE_ROLL: return 8;
	case LEFT_RING_ROLL: return 9;
	case LEFT_PINKY_ROLL: return 10;
	case RIGHT_THUMB_SLAP: return 11;
	case LEFT_THUMB_SLAP: return 12;
	case RIGHT_FOUR_SLAP: return 13;
	case LEFT_FOUR_SLAP: return 14;
	case RIGHT_INDEX_SLAP: return 15;
	case RIGHT_MIDDLE_SLAP: return 16;
	case RIGHT_RING_SLAP: return 17;
	case RIGHT_PINKY_SLAP: return 18;
	case LEFT_INDEX_SLAP: return 19;
	case LEFT_MIDDLE_SLAP: return 20;
	case LEFT_RING_SLAP: return 21;
	case LEFT_PINKY_SLAP: return 22;
	case TWO_THUMB_SLAP: return 31;
	case RIGHT_THUMB_SEG_SLAP: return 32;
	case LEFT_THUMB_SEG_SLAP: return 33;
default: return -1;
}
}
public static FingerprintImageType value(int value) {
switch(value) {
	case 1: return RIGHT_THUMB_ROLL;
	case 2: return RIGHT_INDEX_ROLL;
	case 3: return RIGHT_MIDDLE_ROLL;
	case 4: return RIGHT_RING_ROLL;
	case 5: return RIGHT_PINKY_ROLL;
	case 6: return LEFT_THUMB_ROLL;
	case 7: return LEFT_INDEX_ROLL;
	case 8: return LEFT_MIDDLE_ROLL;
	case 9: return LEFT_RING_ROLL;
	case 10: return LEFT_PINKY_ROLL;
	case 11: return RIGHT_THUMB_SLAP;
	case 12: return LEFT_THUMB_SLAP;
	case 13: return RIGHT_FOUR_SLAP;
	case 14: return LEFT_FOUR_SLAP;
	case 15: return RIGHT_INDEX_SLAP;
	case 16: return RIGHT_MIDDLE_SLAP;
	case 17: return RIGHT_RING_SLAP;
	case 18: return RIGHT_PINKY_SLAP;
	case 19: return LEFT_INDEX_SLAP;
	case 20: return LEFT_MIDDLE_SLAP;
	case 21: return LEFT_RING_SLAP;
	case 22: return LEFT_PINKY_SLAP;
	case 31: return TWO_THUMB_SLAP;
	case 32: return RIGHT_THUMB_SEG_SLAP;
	case 33: return LEFT_THUMB_SEG_SLAP;
default: return null;
}
}
}
