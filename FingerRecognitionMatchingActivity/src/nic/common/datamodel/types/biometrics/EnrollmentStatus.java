package nic.common.datamodel.types.biometrics;
public enum EnrollmentStatus {
	NOT_ENROLLED,
	ENROLLED,
	PROCESSING,
	PROCESSINGLATER,
	HIT,
	ERROR,
	VISUAL,
	DELETED,
	ENROLLED_HIT,
	SAMIS_TYPE_UPDATE,
	INQUIRY_NOHIT,
	INQUIRY_HIT,
	ENROLL_CRIM_LATENT,
	ENROLL_CRIM,
	ENROLL_LATENT,
	ENROLL_HIT_CRIM,
	CANCELLED,
	INQUIRY_NOHIT_CRIM,
	UNAVAILABLE,
	SAMIS_TYPE_UPDATE_FINGER,
	SAMIS_TYPE_UPDATE_IRIS,
	SAMIS_TYPE_UPDATE_FACE,
	ARCHIVE_DB_OFF;

public static String stringValue(EnrollmentStatus value) {
switch(value) {
	case NOT_ENROLLED: return "EnrollmentStatus.not-enrolled";
	case ENROLLED: return "EnrollmentStatus.enrolled";
	case PROCESSING: return "EnrollmentStatus.processing";
	case PROCESSINGLATER: return "EnrollmentStatus.processingLater";
	case HIT: return "EnrollmentStatus.hit";
	case ERROR: return "EnrollmentStatus.error";
	case VISUAL: return "EnrollmentStatus.visual";
	case DELETED: return "EnrollmentStatus.deleted";
	case ENROLLED_HIT: return "EnrollmentStatus.enrolled-hit";
	case SAMIS_TYPE_UPDATE: return "EnrollmentStatus.samis-type-update";
	case INQUIRY_NOHIT: return "EnrollmentStatus.inquiry-nohit";
	case INQUIRY_HIT: return "EnrollmentStatus.inquiry-hit";
	case ENROLL_CRIM_LATENT: return "EnrollmentStatus.enroll-crim-latent";
	case ENROLL_CRIM: return "EnrollmentStatus.enroll-crim";
	case ENROLL_LATENT: return "EnrollmentStatus.enroll-latent";
	case ENROLL_HIT_CRIM: return "EnrollmentStatus.enroll-hit-crim";
	case CANCELLED: return "EnrollmentStatus.cancelled";
	case INQUIRY_NOHIT_CRIM: return "EnrollmentStatus.inquiry-nohit-crim";
	case UNAVAILABLE: return "EnrollmentStatus.unavailable";
	case SAMIS_TYPE_UPDATE_FINGER: return "EnrollmentStatus.samis-type-finger-update";
	case SAMIS_TYPE_UPDATE_IRIS: return "EnrollmentStatus.samis-type-iris-update";
	case SAMIS_TYPE_UPDATE_FACE: return "EnrollmentStatus.samis-type-face-update";
	case ARCHIVE_DB_OFF: return "EnrollmentStatus.unavailable";
default: return "unknown value for type EnrollmentStatus";
}
}
public static EnrollmentStatus value(String value) {
	if(value.equals("EnrollmentStatus.not-enrolled"))  return NOT_ENROLLED;
	if(value.equals("EnrollmentStatus.enrolled"))  return ENROLLED;
	if(value.equals("EnrollmentStatus.processing"))  return PROCESSING;
	if(value.equals("EnrollmentStatus.processingLater"))  return PROCESSINGLATER;
	if(value.equals("EnrollmentStatus.hit"))  return HIT;
	if(value.equals("EnrollmentStatus.error"))  return ERROR;
	if(value.equals("EnrollmentStatus.visual"))  return VISUAL;
	if(value.equals("EnrollmentStatus.deleted"))  return DELETED;
	if(value.equals("EnrollmentStatus.enrolled-hit"))  return ENROLLED_HIT;
	if(value.equals("EnrollmentStatus.samis-type-update"))  return SAMIS_TYPE_UPDATE;
	if(value.equals("EnrollmentStatus.inquiry-nohit"))  return INQUIRY_NOHIT;
	if(value.equals("EnrollmentStatus.inquiry-hit"))  return INQUIRY_HIT;
	if(value.equals("EnrollmentStatus.enroll-crim-latent"))  return ENROLL_CRIM_LATENT;
	if(value.equals("EnrollmentStatus.enroll-crim"))  return ENROLL_CRIM;
	if(value.equals("EnrollmentStatus.enroll-latent"))  return ENROLL_LATENT;
	if(value.equals("EnrollmentStatus.enroll-hit-crim"))  return ENROLL_HIT_CRIM;
	if(value.equals("EnrollmentStatus.cancelled"))  return CANCELLED;
	if(value.equals("EnrollmentStatus.inquiry-nohit-crim"))  return INQUIRY_NOHIT_CRIM;
	if(value.equals("EnrollmentStatus.unavailable"))  return UNAVAILABLE;
	if(value.equals("EnrollmentStatus.samis-type-finger-update"))  return SAMIS_TYPE_UPDATE_FINGER;
	if(value.equals("EnrollmentStatus.samis-type-iris-update"))  return SAMIS_TYPE_UPDATE_IRIS;
	if(value.equals("EnrollmentStatus.samis-type-face-update"))  return SAMIS_TYPE_UPDATE_FACE;
	if(value.equals("EnrollmentStatus.unavailable"))  return ARCHIVE_DB_OFF;
return null;}
public static int intValue(EnrollmentStatus value) {
switch(value) {
	case NOT_ENROLLED: return 0;
	case ENROLLED: return 1;
	case PROCESSING: return 2;
	case PROCESSINGLATER: return 22;
	case HIT: return 3;
	case ERROR: return 4;
	case VISUAL: return 5;
	case DELETED: return 6;
	case ENROLLED_HIT: return 7;
	case SAMIS_TYPE_UPDATE: return 8;
	case INQUIRY_NOHIT: return 91;
	case INQUIRY_HIT: return 93;
	case ENROLL_CRIM_LATENT: return 50;
	case ENROLL_CRIM: return 51;
	case ENROLL_LATENT: return 52;
	case ENROLL_HIT_CRIM: return 53;
	case CANCELLED: return 54;
	case INQUIRY_NOHIT_CRIM: return 55;
	case UNAVAILABLE: return 100;
	case SAMIS_TYPE_UPDATE_FINGER: return 81;
	case SAMIS_TYPE_UPDATE_IRIS: return 82;
	case SAMIS_TYPE_UPDATE_FACE: return 83;
	case ARCHIVE_DB_OFF: return 101;
default: return -1;
}
}
public static EnrollmentStatus value(int value) {
switch(value) {
	case 0: return NOT_ENROLLED;
	case 1: return ENROLLED;
	case 2: return PROCESSING;
	case 22: return PROCESSINGLATER;
	case 3: return HIT;
	case 4: return ERROR;
	case 5: return VISUAL;
	case 6: return DELETED;
	case 7: return ENROLLED_HIT;
	case 8: return SAMIS_TYPE_UPDATE;
	case 91: return INQUIRY_NOHIT;
	case 93: return INQUIRY_HIT;
	case 50: return ENROLL_CRIM_LATENT;
	case 51: return ENROLL_CRIM;
	case 52: return ENROLL_LATENT;
	case 53: return ENROLL_HIT_CRIM;
	case 54: return CANCELLED;
	case 55: return INQUIRY_NOHIT_CRIM;
	case 100: return UNAVAILABLE;
	case 81: return SAMIS_TYPE_UPDATE_FINGER;
	case 82: return SAMIS_TYPE_UPDATE_IRIS;
	case 83: return SAMIS_TYPE_UPDATE_FACE;
	case 101: return ARCHIVE_DB_OFF;
default: return null;
}
}
}
