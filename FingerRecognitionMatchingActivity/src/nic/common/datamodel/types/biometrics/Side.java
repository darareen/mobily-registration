package nic.common.datamodel.types.biometrics;
public enum Side {
	LEFT,
	RIGHT,
	BOTH,
	LEFT_HR,
	RIGHT_HR,
	BOTH_HR,
	FACE_ONLY_HR;

public static String stringValue(Side value) {
switch(value) {
	case LEFT: return "left";
	case RIGHT: return "right";
	case BOTH: return "both";
	case LEFT_HR: return "left-hr";
	case RIGHT_HR: return "right_hr";
	case BOTH_HR: return "both_hr";
	case FACE_ONLY_HR: return "face_only_hr";
default: return "unknown value for type Side";
}
}
public static Side value(String value) {
	if(value.equals("left"))  return LEFT;
	if(value.equals("right"))  return RIGHT;
	if(value.equals("both"))  return BOTH;
	if(value.equals("left-hr"))  return LEFT_HR;
	if(value.equals("right_hr"))  return RIGHT_HR;
	if(value.equals("both_hr"))  return BOTH_HR;
	if(value.equals("face_only_hr"))  return FACE_ONLY_HR;
return null;}
public static int intValue(Side value) {
switch(value) {
	case LEFT: return 0;
	case RIGHT: return 1;
	case BOTH: return 2;
	case LEFT_HR: return 3;
	case RIGHT_HR: return 4;
	case BOTH_HR: return 5;
	case FACE_ONLY_HR: return 6;
default: return -1;
}
}
public static Side value(int value) {
switch(value) {
	case 0: return LEFT;
	case 1: return RIGHT;
	case 2: return BOTH;
	case 3: return LEFT_HR;
	case 4: return RIGHT_HR;
	case 5: return BOTH_HR;
	case 6: return FACE_ONLY_HR;
default: return null;
}
}
}
