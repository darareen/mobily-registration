package nic.common.datamodel.types.biometrics;
public enum BiometricType {
	FINGERPRINT,
	IRIS,
	PORTRAIT;

public static String stringValue(BiometricType value) {
switch(value) {
	case FINGERPRINT: return "fingerprint";
	case IRIS: return "iris";
	case PORTRAIT: return "portrait";
default: return "unknown value for type BiometricType";
}
}
public static BiometricType value(String value) {
	if(value.equals("fingerprint"))  return FINGERPRINT;
	if(value.equals("iris"))  return IRIS;
	if(value.equals("portrait"))  return PORTRAIT;
return null;}
public static int intValue(BiometricType value) {
switch(value) {
	case FINGERPRINT: return 0;
	case IRIS: return 1;
	case PORTRAIT: return 2;
default: return -1;
}
}
public static BiometricType value(int value) {
switch(value) {
	case 0: return FINGERPRINT;
	case 1: return IRIS;
	case 2: return PORTRAIT;
default: return null;
}
}
}
