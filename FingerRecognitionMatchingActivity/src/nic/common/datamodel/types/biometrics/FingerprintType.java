package nic.common.datamodel.types.biometrics;
public enum FingerprintType {
	ROLL,
	SLAP;

public static String stringValue(FingerprintType value) {
switch(value) {
	case ROLL: return "roll";
	case SLAP: return "slap";
default: return "unknown value for type FingerprintType";
}
}
public static FingerprintType value(String value) {
	if(value.equals("roll"))  return ROLL;
	if(value.equals("slap"))  return SLAP;
return null;}
public static int intValue(FingerprintType value) {
switch(value) {
	case ROLL: return 0;
	case SLAP: return 1;
default: return -1;
}
}
public static FingerprintType value(int value) {
switch(value) {
	case 0: return ROLL;
	case 1: return SLAP;
default: return null;
}
}
}
