package nic.common.datamodel.types.biometrics;
public enum FingerType {
	LITTLE,
	RING,
	MIDDLE,
	INDEX,
	THUMB;

public static String stringValue(FingerType value) {
switch(value) {
	case LITTLE: return "little";
	case RING: return "ring";
	case MIDDLE: return "middle";
	case INDEX: return "index";
	case THUMB: return "thumb";
default: return "unknown value for type FingerType";
}
}
public static FingerType value(String value) {
	if(value.equals("little"))  return LITTLE;
	if(value.equals("ring"))  return RING;
	if(value.equals("middle"))  return MIDDLE;
	if(value.equals("index"))  return INDEX;
	if(value.equals("thumb"))  return THUMB;
return null;}
public static int intValue(FingerType value) {
switch(value) {
	case LITTLE: return 0;
	case RING: return 1;
	case MIDDLE: return 2;
	case INDEX: return 3;
	case THUMB: return 4;
default: return -1;
}
}
public static FingerType value(int value) {
switch(value) {
	case 0: return LITTLE;
	case 1: return RING;
	case 2: return MIDDLE;
	case 3: return INDEX;
	case 4: return THUMB;
default: return null;
}
}
}
