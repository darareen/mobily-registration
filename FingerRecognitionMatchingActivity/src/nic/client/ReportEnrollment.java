package nic.client;

//import android.os.StrictMode;
import nic.common.datamodel.EnrollmentInfo;
import nic.common.datamodel.types.biometrics.EnrollmentStatus;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;


/* Wael
This method checks if the search in NIC backend was completed. It uses the inquiry ID to check if the result was ready.
*/


public class ReportEnrollment {

	private static final String SOAP_ACTION = "";

	private static final String OPERATION_NAME = "reportEnrolmentByEnrollee";

	private static final String WSDL_TARGET_NAMESPACE = "http://datamodel.common.nic.gov.sa";

	private static final String SOAP_ADDRESS = "http://10.0.21.2/CAFISServer/services/EnrollmentReport?wsdl";
	private static final int retryTimeout = 1; //minute
	private static final int retryPeriod = 15; //second


	public EnrollmentInfo[] retrieveEnrollmentResult(long InquiryNo)
    {
//		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//		StrictMode.setThreadPolicy(policy);

		EnrollmentInfo [] EnrollmentInfoResult = null;
        EnrollmentInfo [] EnrollmentInfoResultFinal = null;

		boolean Processing = false;

		if (InquiryNo!=0)
		{
			int NumRetries = 5;
			int RetryCount = 0;

			EnrollmentInfoResult = reportEnrolmentByEnrollee(InquiryNo);
			if (EnrollmentInfoResult[0].getStatus() ==  EnrollmentStatus.intValue(EnrollmentStatus.PROCESSING))
			{
				try
                {
                    System.out.println("======>>> Still PROCESSING");
                    RetryCount = RetryCount +1;
                    if (RetryCount >= NumRetries)
                    {
                        System.out.println("Number of Retries Exceeded....");
                        Processing = true;
                        return null;
                    }

					Thread.sleep(retryPeriod*1000);
				}
                catch (InterruptedException e)
                {
					e.printStackTrace();
				}
			}
		}

        EnrollmentInfoResultFinal = new  EnrollmentInfo [1];
		if (! Processing)
		{
			for (int i=0; i< EnrollmentInfoResult.length;i++)

			{
				if (EnrollmentInfoResult[i].getStatus() == 1 )  // I think we need only reords with status 1
				{
					EnrollmentInfoResultFinal[0] = EnrollmentInfoResult[i];
				}
			}
		}
		return EnrollmentInfoResultFinal;
	}


	 public EnrollmentInfo[] reportEnrolmentByEnrollee(long InquiryNo)
	 {

//			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//			StrictMode.setThreadPolicy(policy);

		 EnrollmentInfo [] EnrollmentInfoRecordResult = null;


		 try{
		 	SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);
		 	request.addProperty( "sessionId","");
		 	request.addProperty( "enrolleeId", InquiryNo); //enrolleeId
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = false;
			envelope.setOutputSoapObject(request);
			HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);   //    SOAP_ADDRESS_12
			List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();

			headerList.add(new HeaderProperty("Authorization", "Basic "+ org.kobjects.base64.Base64.encode("teuser:123456".getBytes())));
			httpTransport.call(SOAP_ACTION, envelope,headerList);
			SoapObject soap = (SoapObject) envelope.bodyIn;
            EnrollmentInfoRecordResult = new EnrollmentInfo[soap.getPropertyCount()];
		    for (int i = 0; i < EnrollmentInfoRecordResult.length; i++) {
		        SoapObject pii = (SoapObject)soap.getProperty(i);
		        EnrollmentInfo record = new EnrollmentInfo();
		        record.setTCN(Long.parseLong(pii.getProperty("TCN").toString()));
		        record.setBiometricId(Long.parseLong(pii.getProperty("biometricId").toString()));
		        record.setEnrollee(Long.parseLong(pii.getProperty("enrollee").toString()));
		        record.setOperator(Long.parseLong(pii.getProperty("operator").toString()));
		        record.setSupervisor(Long.parseLong(pii.getProperty("supervisor").toString()));
		        record.setLocation(Integer.parseInt(pii.getProperty("location").toString()));
		        record.setType(Integer.parseInt(pii.getProperty("type").toString()));
		        record.setStatus(Integer.parseInt(pii.getProperty("status").toString()));
		        record.setSubsystem(Integer.parseInt(pii.getProperty("subsystem").toString()));
		        record.setXCandidate(Long.parseLong(pii.getProperty("XCandidate").toString()));
		        record.setCaptureTime(Long.parseLong(pii.getProperty("captureTime").toString()));

		        EnrollmentInfoRecordResult[i] = record;
		    }

			}catch(Exception ex ){
				ex.printStackTrace();
			}catch (Throwable t) {
				t.printStackTrace();
			}

		 return EnrollmentInfoRecordResult;
	 }
}
