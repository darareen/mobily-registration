package nic.client;

//import android.os.StrictMode;

//import com.nic.FingerArraySerializer;

import com.neurotec.samples.multibiometric.fingers.app.FingerArraySerializer;
import nic.common.datamodel.*;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class IdentificationProcess {
	private static final String SOAP_ACTION = "";

	private static final String OPERATION_NAME = "searchFingers";

	private static final String WSDL_TARGET_NAMESPACE = "http://datamodel.common.nic.gov.sa";

	private static final String SOAP_ADDRESS = "http://10.0.21.2/CAFISServer/services/MBISExtension?wsdl";
//	private static final String SOAP_ADDRESS_12 = "http://10.0.224.12:8000/CAFISServer/services/MBISExtension?wsdl";

	PropertyInfo fingerVectorPropertyInfo;
	PropertyInfo fingersPropertyInfo;
	Fingerprints FingerprintsVector;


	public long searchFingers(Finger[] fingers,long operatorId, int locationId)
    {

//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//		StrictMode.setThreadPolicy(policy);

        long inquiryId = 0;

		Finger[] finger = new Finger[23];


        finger[FingerType.RIGHT_THUMB_SLAP] = new Finger(FingerType.RIGHT_THUMB_SLAP, null);

        finger[FingerType.LEFT_THUMB_SLAP] = new Finger(FingerType.LEFT_THUMB_SLAP, null);

        finger[FingerType.RIGHT_FOUR_SLAP]   = new Finger(FingerType.RIGHT_FOUR_SLAP, null);

        finger[FingerType.LEFT_FOUR_SLAP]     = new Finger(FingerType.LEFT_FOUR_SLAP, null);

        finger[FingerType.RIGHT_THUMB_ROLL]    = fingers[6];

        finger[FingerType.RIGHT_INDEX_ROLL]   = fingers[7];

        finger[FingerType.RIGHT_MIDDLE_ROLL]    = fingers[8];

        finger[FingerType.RIGHT_RING_ROLL]   = fingers[9];

        finger[FingerType.RIGHT_PINKY_ROLL]   = fingers[0];

        finger[FingerType.LEFT_THUMB_ROLL]    = fingers[5];

        finger[FingerType.LEFT_INDEX_ROLL]    = fingers[4];

        finger[FingerType.LEFT_MIDDLE_ROLL]   = fingers[3];

        finger[FingerType.LEFT_RING_ROLL]   = fingers[2];

        finger[FingerType.LEFT_PINKY_ROLL]   = fingers[1];


        try
        {
            SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);

           FingerArraySerializer stringArray = new FingerArraySerializer();

            for (int i=0;i < finger.length ;i++)
			{
				stringArray.add(finger[i]);
			}

            PropertyInfo objekt = new PropertyInfo();
            objekt.setName("finger");
           objekt.setValue(stringArray);
            objekt.setType(stringArray.getClass());


            request.addProperty(objekt);
			request.addProperty("operatorId", operatorId);
			request.addProperty("locationId", locationId);

			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = false;

            envelope.setOutputSoapObject(request);

			envelope.addMapping("http://datamodel.common.nic.gov.sa", "Finger", nic.common.datamodel.Finger[].class);

//            envelope.addMapping("http://datamodel.common.nic.gov.sa", "FingerArraySerializer", new com.nic.FingerArraySerializer().getClass());


			//new MarshalBase64File().register(envelope);
			new MarshalBase64().register(envelope);
			

			HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
			httpTransport.debug = true;
			List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
			headerList.add(new HeaderProperty("Authorization", "Basic "+ org.kobjects.base64.Base64.encode("teuser:123456".getBytes())));

            envelope.bodyOut = request;

            httpTransport.call(SOAP_ACTION, envelope, headerList);

			SoapObject soap = (SoapObject) envelope.bodyIn;

            System.out.println("----->>>" + soap.toString());

			//SoapObject pii = (SoapObject)soap.getProperty(0);
			inquiryId = Long.parseLong(soap.getProperty(0).toString());
		}

        catch (IOException ex) {
			// Logger.getLogger(SoapWebServices.class.getName()).log(Level.SEVERE,
			// image, ex);
			// return image;
			ex.printStackTrace();
		} catch (XmlPullParserException ex) {
			// Logger.getLogger(SoapWebServices.class.getName()).log(Level.SEVERE,
			// image, ex);
			ex.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();

		}

		return inquiryId;
	}

	private Finger[] getFingerprintImages() {

		Finger[] fingers = new Finger[23];
		Finger noPrint = new Finger();
		noPrint.setNoPrint();
		Finger print = new Finger();
		print.setPresence(FingerPresence.PRESENT);
		print.setType(FingerType.RIGHT_RING_ROLL);
		byte[] image = null;
		try {
			File inputFile = new File("F:/WSQ_Image_Samples/finger.wsq");
			image = new byte[(int) (inputFile.length())];
			FileInputStream inputStream = new FileInputStream(inputFile);
			inputStream.read(image);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		print.setImage(image);
		fingers[FingerType.LEFT_FOUR_SLAP] = noPrint;
		fingers[FingerType.RIGHT_FOUR_SLAP] = noPrint;
		fingers[FingerType.LEFT_THUMB_SLAP] = noPrint;
		fingers[FingerType.RIGHT_THUMB_SLAP] = noPrint;
		fingers[FingerType.LEFT_THUMB_ROLL] = noPrint;
		fingers[FingerType.LEFT_INDEX_ROLL] = noPrint;
		fingers[FingerType.LEFT_MIDDLE_ROLL] = noPrint;
		fingers[FingerType.LEFT_RING_ROLL] = noPrint;
		fingers[FingerType.LEFT_PINKY_ROLL] = noPrint;
		fingers[FingerType.RIGHT_THUMB_ROLL] = noPrint;
		fingers[FingerType.RIGHT_INDEX_ROLL] = noPrint;
		fingers[FingerType.RIGHT_MIDDLE_ROLL] = noPrint;
		fingers[FingerType.RIGHT_RING_ROLL] = noPrint;
		fingers[FingerType.RIGHT_PINKY_ROLL] = print;
		MinutiaeData minutiaeData = new MinutiaeData();
		Minutiae firstMinutiae = new Minutiae();
		firstMinutiae.setMinutiaeValue(new byte[] { 0x00 });
		Minutiae secondMinutiae = new Minutiae();
		secondMinutiae.setMinutiaeValue(new byte[] { 0x00 });
		minutiaeData.setFirstMinutiae(firstMinutiae);
		minutiaeData.setSecondMinutiae(secondMinutiae);

		return fingers;
	}

	private Finger getFingerprintSingleImage() {

		Finger[] fingers = new Finger[23];

		Finger noPrint = new Finger();
		noPrint.setNoPrint();
		Finger print = new Finger();
		print.setPresence(FingerPresence.PRESENT);
		print.setType(FingerType.RIGHT_RING_ROLL);
		byte[] image = null;
		try {
			File inputFile = new File("F:/WSQ_Image_Samples/finger.wsq");
			image = new byte[(int) (inputFile.length())];
			FileInputStream inputStream = new FileInputStream(inputFile);
			inputStream.read(image);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		print.setImage(image);
		fingers[FingerType.RIGHT_PINKY_ROLL] = print;
		MinutiaeData minutiaeData = new MinutiaeData();
		Minutiae firstMinutiae = new Minutiae();
		firstMinutiae.setMinutiaeValue(new byte[] { 0x00 });
		Minutiae secondMinutiae = new Minutiae();
		secondMinutiae.setMinutiaeValue(new byte[] { 0x00 });
		minutiaeData.setFirstMinutiae(firstMinutiae);
		minutiaeData.setSecondMinutiae(secondMinutiae);

		return print;
	}

	byte[] getImageWSQ(){
		byte[] image = null;
		/*try {
			File inputFile = new File("F:/WSQ_Image_Samples/a.wsq");
			image = new byte[(int) (inputFile.length())];
			FileInputStream inputStream = new FileInputStream(inputFile);
			inputStream.read(image);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/



        //CHECKKKKKKK HEREEEEEE

//        try {
//            File sdCard = Environment.getExternalStorageDirectory();
//            File dir = new File (sdCard.getAbsolutePath() + "/CredenceNIC/wsq/");
//            File file = new File(dir, "imgwsq1.wsq");
//            image = org.apache.commons.io.FileUtils.readFileToByteArray(file);
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
		return image;
	}
	
	public static byte[] convertWSQToByteArray() throws IOException {
		String sourcePath = "F:/WSQ_Image_Samples/a.wsq";
		File f = new File(sourcePath);
	      long l = f.length();
	      byte [] buf = new byte[(int) l];
	      ByteArrayOutputStream bos = new ByteArrayOutputStream();
	            try {
	                  InputStream fis = new FileInputStream(sourcePath);
	                  
	                  for (int readNum; (readNum = fis.read(buf)) != -1;) {
	                      bos.write(buf, 0, readNum);
	                     
	                  }
	           } catch (IOException e) {
	                System.out.println("IO Ex"+e);
	            }
	            byte[] bytes = bos.toByteArray();
				return bytes;
	      }

}
