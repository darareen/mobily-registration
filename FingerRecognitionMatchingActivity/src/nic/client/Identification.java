package nic.client;

import nic.common.datamodel.Person;
import nic.common.datamodel.PersonType;

public class Identification {
	
	private static final String OPERATION_CitizenData  = "getCitizenData";
	private static final String OPERATION_ResidentData = "getResidentData";
	private static final String OPERATION_VisitorData = "getVisitorData";
	private static final String OPERATION_IllegalData = "getIllegalData";
	private static final String OPERATION_DeporteeData = "getDeporteeData";
	private static final String OPERATION_PilgrimData = "getPilgrimData";

	
	private IdentificationSoapBinding ident;
	
	public Person getPersonData(String sessionId , long smaisID , int  personType){
		try {
			/*setLastActionTime();
			binding = (EnrollmentSoapBindingStub) enrollmentLocator.getEnrollment();
			binding.setTimeout(ENROLLMENT_TIMEOUT);
			setCookies(binding);*/
			
			ident = new IdentificationSoapBinding();
			
			if ( personType == PersonType.CITIZEN) {
				return getCitizenData(sessionId, smaisID);
			} else if (personType == PersonType.RESIDENT) {
				return getResidentData(sessionId, smaisID);
			} else if (personType == PersonType.VISITOR) {
				return getVisitorData(sessionId, smaisID);
			} else if (personType == PersonType.ILLEGAL) {
				return getIllegalData(sessionId, smaisID);
			} else if (personType == PersonType.DEPORTEE) {
				return getDeporteeData(sessionId, smaisID);				
			} else if (personType == PersonType.PILGRIM) {
				return getPilgrimData(sessionId, smaisID);
			} else {
				throw new IllegalArgumentException("Person ID type not recognized, personId = " +smaisID);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} catch (Throwable t) {
			t.printStackTrace();

		}
		return null;
	}

	private Person getPilgrimData(String sessionId, long smaisID) throws Exception {
		Person person = null;
		person = ident.getData(sessionId, smaisID, OPERATION_PilgrimData);
		return person;
	}

	private Person getDeporteeData(String sessionId, long smaisID) throws Exception {
		Person person = null;
		person = ident.getData(sessionId, smaisID, OPERATION_DeporteeData);
		return person;
	}

	private Person getIllegalData(String sessionId, long smaisID) throws Exception{
		Person person = null;
		person = ident.getData(sessionId, smaisID, OPERATION_IllegalData);
		return person;
	}

	private Person getVisitorData(String sessionId, long smaisID) throws Exception{
		Person person = null;
		person = ident.getData(sessionId, smaisID, OPERATION_VisitorData);
		return person;
	}

	private Person getResidentData(String sessionId, long smaisID)throws Exception {
		Person person = null;
		person = ident.getData(sessionId, smaisID, OPERATION_ResidentData);
		return person;
	}

	private Person getCitizenData(String sessionId, long smaisID)throws Exception {
		Person person = null;
		person = ident.getData(sessionId, smaisID, OPERATION_CitizenData);
	
		return person;
	}

}
