package nic.client;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.neurotec.samples.multibiometric.fingers.app.FingerRecognitionMatchingActivity;

import android.util.Base64;
import android.util.Log;
import nic.common.datamodel.FacialPhoto;
import nic.common.datamodel.Finger;
import nic.common.datamodel.Minutiae;
import nic.common.datamodel.MinutiaeData;
import nic.common.datamodel.Person;

import java.util.ArrayList;
import java.util.List;

public class IdentificationSoapBinding {
	private static final String TAG = IdentificationSoapBinding.class
			.getSimpleName();
	String SOAP_ACTION = "http://www.benchmarkatlarge.com/";
	String NAMESPACE = "http://www.benchmarkatlarge.com";

	String SOAP_ADDRESS = "http://www.benchmarkatlarge.com/Mobily/WebServices/mobilysync.asmx?wsdl";

	public Person getData(String sessionId, long personId,
			final String OPERATION_NAME) {
		Log.d("IdentificationSoapBinding", "inside getData --> Method Name -> "
				+ OPERATION_NAME);

		Person person = null;
		Finger[] fingers = null;
		Finger finger = null;
		try {
			SOAP_ACTION = SOAP_ACTION + OPERATION_NAME;
			SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
			request.addProperty("sessionid", personId);
			request.addProperty("personid", personId);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
			List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
			// Authorization Credentials here:
			headerList.add(new HeaderProperty("Authorization", "Basic "+ org.kobjects.base64.Base64.encode("teuser:123456".getBytes())));
			httpTransport.call(SOAP_ACTION, envelope, headerList);
			SoapObject soap = (SoapObject) envelope.bodyIn;

			Log.d(TAG, "inside loop");
			SoapObject pii = (SoapObject) soap.getProperty(0);
			person = new Person();
			person.setIdNumber(Long.parseLong(pii.getProperty("idNumber") == null ? "".toString() : pii.getProperty("idNumber").toString()));
			person.setBiometricId(Long.parseLong(pii.getProperty("biometricId") == null ? "".toString() : pii.getProperty("biometricId").toString()));
			person.setIdExpDate(pii.getProperty("idExpiryDate") == null ? "".toString() : pii.getProperty("idExpiryDate").toString());
			person.setEnrollmentStatus(Integer.parseInt(pii.getProperty("enrollmentStatus") == null ? "".toString() : pii.getProperty("enrollmentStatus").toString()));
			person.setFamilyName(pii.getProperty("familyName") == null ? "".toString() : pii.getProperty("familyName").toString());
			person.setFatherName(pii.getProperty("fatherName") == null ? "".toString() : pii.getProperty("fatherName").toString());
			person.setFirstName(pii.getProperty("firstName") == null ? "".toString() : pii.getProperty("firstName").toString());
			person.setGender(Integer.parseInt(pii.getProperty("gender") == null ? "".toString() : pii.getProperty("gender").toString()));

			SoapObject SoapFingers = (SoapObject) pii.getProperty("fingers");
			fingers = new Finger[SoapFingers.getPropertyCount()];
			for (int j = 0; j < fingers.length; j++) {
				finger = (Finger) SoapFingers.getProperty(j);
				fingers[j] = finger;
			}
			person.setFingers(fingers);

			person.setFingerVerified(Integer.parseInt(pii.getProperty("fingerVerified") == null ? "".toString()	: pii.getProperty("fingerVerified").toString()));

			FacialPhoto fp = new FacialPhoto();
			try {
				SoapObject FacialPhotoSoap = (SoapObject) pii.getProperty("facialPhoto");

				fp.setWidth(Integer.parseInt(FacialPhotoSoap.getPropertyAsString("width")));
				fp.setHeight(Integer.parseInt(FacialPhotoSoap.getPropertyAsString("height")));
				fp.setFacialPhoto(Base64.decode(FacialPhotoSoap.getPropertyAsString("facialphoto"),	Base64.DEFAULT));
				fp.setCreationTime(Long.parseLong(FacialPhotoSoap.getPropertyAsString("creationTime")));
				fp.setExpirationTime(Long.parseLong(FacialPhotoSoap.getPropertyAsString("expirationTime")));

				if (FacialPhotoSoap.getPropertyAsString("newPhoto").equals("true"))
					fp.setNewPhoto(true);
				else
					fp.setNewPhoto(false);

			} catch (Exception ex) {
				Log.d(TAG, "Exception FacialPhoto -> " + ex.getMessage());
				ex.printStackTrace();
			}

			person.setFacialPhoto(fp);

			MinutiaeData minutiaeData = new MinutiaeData();
			Minutiae fristMinutiae = new Minutiae();
			Minutiae secondMinutiae = new Minutiae();

			try {
				SoapObject MinutiaeSoap = (SoapObject) pii.getProperty("minutiaeData");
				// This is for Right Finger
				SoapObject rightMinutiaeSOAP = (SoapObject) MinutiaeSoap.getProperty(0);
				// This is for Right Finger
				SoapObject leftMinutiaeSOAP = (SoapObject) MinutiaeSoap.getProperty(1);

				fristMinutiae.setFingerId(Integer.parseInt(rightMinutiaeSOAP.getPropertyAsString("fingerId")));
				fristMinutiae.setMinutiaeValue(Base64.decode(rightMinutiaeSOAP.getPropertyAsString("minutiaeValue"),Base64.DEFAULT));

				secondMinutiae.setFingerId(Integer.parseInt(leftMinutiaeSOAP.getPropertyAsString("fingerId")));
				secondMinutiae.setMinutiaeValue(Base64.decode(leftMinutiaeSOAP.getPropertyAsString("minutiaeValue"),Base64.DEFAULT));

				
			} catch (Exception ex) {
				Log.d(TAG, "Exception  MinutiaeData -> " + ex.getMessage());
				ex.printStackTrace();
			}
			minutiaeData.setFirstMinutiae(fristMinutiae);
			minutiaeData.setSecondMinutiae(secondMinutiae);
			minutiaeData.setCreationTS("");
			person.setMinutiaeData(minutiaeData);

			Log.d(TAG, " Sucessfully Person retrived");
				} catch (SoapFault sf) {
			Log.d(TAG, "SOAPFault -> " + sf.getMessage());
			sf.printStackTrace();
			person = null;
		} catch (Exception ex) {
			Log.d(TAG, "Exception -> " + ex.getMessage());
			ex.printStackTrace();
			person = null;
		} catch (Throwable t) {
			// "CAFIS Failure", t
			t.printStackTrace();
			person = null;
		}
		return person;
	}

}
