package nic.client;



import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import nic.common.datamodel.WatchListRecord;

import java.util.ArrayList;
import java.util.List;

/* Wael
This class is used for checking if a person is on the watchlist.
*/


public class WSCheckCWL {
	
	private static final String SOAP_ACTION = "";

	private static final String OPERATION_NAME = "checkCWL";

	private static final String WSDL_TARGET_NAMESPACE = "http://datamodel.common.nic.gov.sa";

	private static final String SOAP_ADDRESS = "http://10.0.21.2/CAFISServer/services/MBISExtension?wsdl";
	
	private static final String SOAP_ADDRESS_12 = "http://10.0.224.12:8000/CAFISServer/services/MBISExtension?wsdl";
	
	 @SuppressWarnings("unchecked")
	public nic.common.datamodel.WatchListRecord[] checkCWL(long personId, int personType, String screenCode, long operatorId, int locationId)
	 {
		 
		 WatchListRecord [] watchListRecordResult = null;		


		 try{
		 	SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);
			request.addProperty( "personId", personId);
			request.addProperty( "personType", personType);
			request.addProperty( "screenCode", screenCode);
			request.addProperty( "operatorId", operatorId);
			request.addProperty( "locationId", locationId);
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			envelope.dotNet = false;
			envelope.setOutputSoapObject(request);
			HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS_12);
			List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
			//cafis_user
			//pass
	        //headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("teuser:123456".getBytes())));
			headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("User:123".getBytes())));
			httpTransport.call(SOAP_ACTION, envelope,headerList);

			SoapObject soap = (SoapObject) envelope.bodyIn;
			watchListRecordResult = new WatchListRecord[soap.getPropertyCount()];
		    for (int i = 0; i < watchListRecordResult.length; i++) {
		        SoapObject pii = (SoapObject)soap.getProperty(i);
		        WatchListRecord record = new WatchListRecord();
		        record.setActionCode( Integer.parseInt(pii.getProperty("actionCode").toString()));
		        record.setActionMessage( pii.getProperty("actionMessage").toString());
		        record.setSamisId(  Long.parseLong(pii.getProperty("samisId").toString()));
		        record.setSourceCode(Integer.parseInt(pii.getProperty("sourceCode").toString()));
		        watchListRecordResult[i] = record;
		    }             
			}catch(Exception ex ){
				ex.printStackTrace();
			}catch (Throwable t) {
				//"CAFIS Failure", t
				t.printStackTrace();
			}
		 	
		 return watchListRecordResult;
	 }
	 

}

