package nic.client;


import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import android.util.Base64;
//import android.os.StrictMode;
import android.util.Log;
import nic.common.datamodel.Finger;
import nic.common.datamodel.Minutiae;
import nic.common.datamodel.MinutiaeData;
import nic.common.datamodel.Person;

import java.util.ArrayList;
import java.util.List;

public class RetrievePhoto {

	private static final String OPERATION_NAME = "retrieveBiometric";
	private static final String SOAP_ACTION = "";
	 
	private static final String WSDL_TARGET_NAMESPACE = "http://datamodel.common.nic.gov.sa";

	private static final String SOAP_ADDRESS = "http://10.0.21.2/CAFISServer/services/DataRetrieval?wsdl";
//	private static final String SOAP_ADDRESS_12 = "http://10.0.224.12:8000/CAFISServer/services/Identification?wsdl";
	
	
	public static Person getData(long samisID, long HitTCN, int type) {
		Person[] personRecord = null;
		Person person = null;
//		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//
//		StrictMode.setThreadPolicy(policy); 
		Log.d("RetrievePhoto", "inside getData");
		
		try{
				SoapObject request = new SoapObject(WSDL_TARGET_NAMESPACE,OPERATION_NAME);
				Log.d("RetrievePhoto", "inside try block of getData");
			 	PropertyInfo idProp = new PropertyInfo();
			    idProp.setName("id");
			    idProp.setValue(samisID);
			    idProp.setType(long.class);
			    request.addProperty(idProp);

			 	PropertyInfo typeProp = new PropertyInfo();
			    typeProp.setName("type");
			    typeProp.setValue(type);
			    typeProp.setType(int.class);
			    request.addProperty(typeProp);

			 	PropertyInfo tcnProp = new PropertyInfo();
			    tcnProp.setName("hitTcn");
			    tcnProp.setValue(HitTCN);
			    tcnProp.setType(long.class);
			    request.addProperty(tcnProp);
			    
			 	
				SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			
				envelope.dotNet = false;
				envelope.setOutputSoapObject(request);
				HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
				List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();

		        //Authorization Credentials here:
                headerList.add(new HeaderProperty("Authorization", "Basic " + org.kobjects.base64.Base64.encode("teuser:123456".getBytes())));

                httpTransport.call(SOAP_ACTION, envelope,headerList);

				SoapObject soap = (SoapObject) envelope.bodyIn;
			
				personRecord = new Person[soap.getPropertyCount()];
			    for (int i = 0; i < personRecord.length; i++) {
			        SoapObject pii = (SoapObject)soap.getProperty(i);
			        person = new Person();
			        person.setIdNumber(Long.parseLong(pii.getProperty("idNumber") == null ?"".toString():pii.getProperty("idNumber").toString()));
			        person.setBiometricId(Long.parseLong(pii.getProperty("biometricId")== null ?"".toString():pii.getProperty("biometricId").toString()));
			        person.setBirthGDate(pii.getProperty("birthGDate") == null?"".toString():pii.getProperty("birthGDate").toString());
			        person.setBirthHDate(Integer.parseInt(pii.getProperty("birthHDate")==null ?"".toString():pii.getProperty("birthHDate").toString()));
			        person.setBirthPlace(pii.getProperty("birthPlace") == null ?"".toString():pii.getProperty("birthPlace").toString());
			        person.setEnrollmentStatus(Integer.parseInt(pii.getProperty("enrollmentStatus") == null?"".toString():pii.getProperty("enrollmentStatus").toString()));
			        person.setFamilyName(pii.getProperty("familyName") == null ?"".toString():pii.getProperty("familyName").toString());
			        person.setFatherName(pii.getProperty("fatherName") == null ? "".toString() : pii.getProperty("fatherName").toString());
			        person.setFirstName(pii.getProperty("firstName") == null ?"".toString():pii.getProperty("firstName").toString());
			        person.setGender(Integer.parseInt(pii.getProperty("gender") == null ?"".toString():pii.getProperty("gender").toString()));

			        
			        SoapObject FacialPhotoSoap = (SoapObject) pii.getProperty("facialPhoto");
			     
			        
                    String  Fphoto = FacialPhotoSoap.getPropertyAsString("facialPhoto");
                  //  person.setFacialPhoto(Fphoto);
 
                    SoapObject MinutiaeSoap = (SoapObject) pii.getProperty("minutiaeData");
                    //This is for Right Finger 
                    SoapObject rightMinutiaeSOAP = (SoapObject) MinutiaeSoap.getProperty(1);
                    //This is for Right Finger 
                    SoapObject leftMinutiaeSOAP = (SoapObject) MinutiaeSoap.getProperty(2);

                    SoapPrimitive rightFingerid = (SoapPrimitive) rightMinutiaeSOAP.getProperty(0);
                    SoapPrimitive rightMinutiae = (SoapPrimitive) rightMinutiaeSOAP.getProperty(1);
                   

                    SoapPrimitive leftFingerid = (SoapPrimitive) leftMinutiaeSOAP.getProperty(0);
                    SoapPrimitive leftMinutiae = (SoapPrimitive) leftMinutiaeSOAP.getProperty(1);
                   
                    person.setRightFignerID(Integer.parseInt(rightFingerid.toString()));
                    person.setRightFingerMinutiae(Base64.decode(rightMinutiae.toString(), Base64.DEFAULT));

                    person.setLeftFingerID(Integer.parseInt(leftFingerid.toString()));
                    person.setLeftFingerMinutiae(Base64.decode(leftMinutiae.toString(), Base64.DEFAULT));

                    
                    personRecord[i] = person;
			    }             
				}catch(Exception ex ){
					ex.printStackTrace();
				}catch (Throwable t) {
					//"CAFIS Failure", 
					t.printStackTrace();
				}
		return person;
	}

	
	

}
