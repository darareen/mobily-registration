package nic.ws.client;

public interface MBISExtension {
   // public sa.gov.nic.common.datamodel.Candidate[] searchFace(byte[] faceImage, boolean highAccuracyRequired, long operatorId, int locationId) ;
   // public sa.gov.nic.common.datamodel.Candidate searchIris(byte[] rightIrisImage, byte[] leftIrisImage, long operatorId, int locationId) ;
    public nic.common.datamodel.WatchListRecord[] checkCWL(long personId, int personType, String screenCode, long operatorId, int locationId) ;
    public boolean enrollPerson(long personId, int personType, byte[] faceImage, byte[] thumbnailImage, nic.common.datamodel.Finger[] fingers, nic.common.datamodel.Minutiae firstMinutiae, nic.common.datamodel.Minutiae secondMinutiae, byte[] rightIrisImage, byte[] leftIrisImage, java.util.Calendar birthDate, int sexCode, long operatorId, int locationId, long supervisorId) ;
    public long searchFingers(nic.common.datamodel.Finger[] fingers, long operatorId, int locationId) ;
    public void updateFace(long personId, byte[] faceImage, byte[] thumbnailImage, java.util.Calendar birthDate, long operatorId, int locationId, long supervisorId) ;
    public void updateIris(long personId, byte[] rightIrisImage, byte[] leftIrisImage, long operatorId, int locationId, long supervisorId) ;
}
