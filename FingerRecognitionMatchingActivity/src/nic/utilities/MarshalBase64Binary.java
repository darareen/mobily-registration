package nic.utilities;

import org.kobjects.base64.Base64;
import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

public class MarshalBase64Binary implements Marshal 
{

	@Override
	public Object readInstance(XmlPullParser arg0, String arg1, String arg2,
			PropertyInfo arg3) throws IOException, XmlPullParserException {
				
		return Base64.decode(arg0.nextText());
		
		

	}

	@Override
	public void register(SoapSerializationEnvelope arg0) {
		
		arg0.addMapping(arg0.xsd, "base64Binary", MarshalBase64.BYTE_ARRAY_CLASS, this);

	}

	@Override
	public void writeInstance(XmlSerializer writer, Object obj)
			throws IOException {
		 writer.text(Base64.encode((byte[]) obj));
		

		
	}

}
