package nic.utilities;

import org.kobjects.base64.Base64;
import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class MarshalBase64File implements Marshal {

	  public static Class FILE_CLASS = File.class;

	  public Object readInstance(XmlPullParser parser, String namespace, String name, PropertyInfo expected)
	      throws IOException, XmlPullParserException {
	    return Base64.decode(parser.nextText());
	  }

	  public void writeInstance(XmlSerializer writer, Object obj) throws IOException {
	    File file = (File)obj;
	    int total = (int)file.length();
	    FileInputStream in = new FileInputStream(file);
	    byte b[] = new byte[4096];
	    int pos = 0;
	    int num = b.length;
	    if ((pos + num) > total) {
	      num = total - pos;
	    }
	    int len = in.read(b, 0, num);
	    while ((len != -1) && ((pos + len) < total)) {
	      writer.text(Base64.encode(b, 0, len, null).toString());
	      pos += len;
	      if ((pos + num) > total) {
	        num = total - pos;
	      }
	      len = in.read(b, 0, num);
	    }
	    if (len != -1) {
	      writer.text(Base64.encode(b, 0, len, null).toString());
	    }
	  }

	  public void register(SoapSerializationEnvelope cm) {
	    cm.addMapping(cm.xsd, "base64Binary", MarshalBase64File.FILE_CLASS, this);
	  }
	}